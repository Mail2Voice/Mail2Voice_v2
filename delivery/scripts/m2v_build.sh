#!/bin/bash

# This is a simple script to build Mail2Voice executable inside a docker container built with Mail2Voice Dockerfile.
# Launch this script from Mail2Voice repository root directory.

mkdir -p build && cd build
pwd
cmake ../src/ -DCMAKE_PREFIX_PATH=/usr/lib/x86_64-linux-gnu/qt5/ -DBUILD_PROD=ON
make

import qbs

CppApplication
{
    files:
    [
        "core/propsTester.hpp",
        "core/sqlfiles/sqlfiles.qrc",
        "core/test_account.cpp",
        "core/test_account.hpp",
        "core/test_accountDatabaseConnector.cpp",
        "core/test_accountDatabaseConnector.hpp",
        "core/test_attachment.cpp",
        "core/test_attachment.hpp",
        "core/test_contact.cpp",
        "core/test_contact.hpp",
        "core/test_contactDatabaseConnector.cpp",
        "core/test_contactDatabaseConnector.hpp",
        "core/test_email.cpp",
        "core/test_email.hpp",
        "core/test_emailDatabaseConnector.cpp",
        "core/test_emailDatabaseConnector.hpp",
        "core/test_serverSettings.cpp",
        "core/test_serverSettings.hpp",
        "core/test_sqlfileparser.cpp",
        "core/test_sqlfileparser.hpp",
        "main.cpp",
    ]

    cpp.cxxLanguageVersion: "c++14"

    Depends
    {
        name: "Qt";
        submodules:
        [
            "core",
            "test",
            "sql"
        ]
    }

    Depends
    {
        name: "mail2voice-core"
    }

    Group
    {
        fileTagsFilter: product.type
        qbs.install: true
    }
}

#include <QTest>

#include "test_account.hpp"

using namespace m2v::core;

void compareServerSettings(const ServerSettings& settings1, const ServerSettings& settings2)
{
    QCOMPARE(settings1.type(), settings2.type());
    QCOMPARE(settings1.address(), settings2.address());
    QCOMPARE(settings1.port(), settings2.port());
    QCOMPARE(settings1.ssl(), settings2.ssl());
}

using A = const Account&;

TestAccount::TestAccount(QObject *parent) :
    QObject(parent)
{
    m_propsChecklist.append({"username",    [](A account) { QCOMPARE(account.username(), QString()); }});
    m_propsChecklist.append({"password",    [](A account) { QCOMPARE(account.password(), QString()); }});
    m_propsChecklist.append({"displayName", [](A account) { QCOMPARE(account.displayName(), QString()); }});
    m_propsChecklist.append({"email",       [](A account) { QCOMPARE(account.email(), QString()); }});
    m_propsChecklist.append({"storeSettings", [](A account) { QCOMPARE(account.storeSettings().type(), ServerType::Imap);
                                                              QCOMPARE(account.storeSettings().address(), QString());
                                                              QCOMPARE(account.storeSettings().port(), (quint16)0);
                                                              QCOMPARE(account.storeSettings().ssl(), true); }});
    m_propsChecklist.append({"transportSettings", [](A account) { QCOMPARE(account.storeSettings().type(), ServerType::Imap);
                                                                  QCOMPARE(account.transportSettings().address(), QString());
                                                                  QCOMPARE(account.transportSettings().port(), (quint16)0);
                                                                  QCOMPARE(account.transportSettings().ssl(), true); }});
}

void TestAccount::default_constructor()
{
    Account account;
    checkDefaultProperties(account, {});
}

void TestAccount::base_test_data()
{
    QTest::addColumn<QString>("username");
    QTest::addColumn<QString>("pass");
    QTest::addColumn<QString>("displayName");
    QTest::addColumn<QString>("email");

    QTest::newRow("empty properties") << QString() << QString() << QString() << QString();
    QTest::newRow("simple test") << QString("nobody") << QString("pass") << QString("Mr Nobody") << QString("mr.nobody@mail.com");
}

void TestAccount::base_test()
{
    QFETCH(QString, username);
    QFETCH(QString, pass);
    QFETCH(QString, displayName);
    QFETCH(QString, email);

    ServerSettings imapSettings(ServerType::Imap, "imap.server.com", 42, true);
    ServerSettings smtpSettings(ServerType::Smtp, "smtp.server.com", 0, true);

    Account account(username, pass, displayName, email, imapSettings, smtpSettings);
    Account accountCopyByConstructor(account);
    Account accountCopyByAssignment = account;

    QList<Account*> accountsToTest({&account, &accountCopyByConstructor, &accountCopyByAssignment});

    for(auto acc: accountsToTest)
    {
        QCOMPARE(acc->username(), username);
        QCOMPARE(acc->password(), pass);
        QCOMPARE(acc->displayName(), displayName);
        QCOMPARE(acc->email(), email);
        compareServerSettings(acc->storeSettings(), imapSettings);
        compareServerSettings(acc->transportSettings(), smtpSettings);
    }
}

void TestAccount::setUsername()
{
    Account account;
    account.setUsername("Nobody");

    QCOMPARE(account.username(), QString("Nobody"));

    checkDefaultProperties(account, {"username"});
}

void TestAccount::setDisplayName()
{
    Account account;
    account.setDisplayName("Nobody");

    QCOMPARE(account.displayName(), QString("Nobody"));

    checkDefaultProperties(account, {"displayName"});
}

void TestAccount::setEmail()
{
    Account account;
    account.setEmail("mail@example.com");

    QCOMPARE(account.email(), QString("mail@example.com"));

    checkDefaultProperties(account, {"email"});
}

void TestAccount::setStoreSettings()
{
    Account account;
    ServerSettings storeSettings(ServerType::Imap, "store.test.com", 42, true);
    account.setStoreSettings(storeSettings);

    compareServerSettings(account.storeSettings(), storeSettings);

    checkDefaultProperties(account, {"storeSettings"});
}

void TestAccount::setTransportSettings()
{
    Account account;
    ServerSettings transportSettings(ServerType::Imap, "transport.test.com", 1337, true);
    account.setTransportSettings(transportSettings);

    compareServerSettings(account.transportSettings(), transportSettings);

    checkDefaultProperties(account, {"transportSettings"});
}


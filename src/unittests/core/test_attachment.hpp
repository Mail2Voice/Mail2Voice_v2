#pragma once

#include "../../core/attachment.hpp"
#include "propsTester.hpp"

#include <QObject>

class TestAttachment : public QObject, public PropsTester<m2v::core::Attachment>
{
    Q_OBJECT

public:
    explicit TestAttachment(QObject *parent = nullptr);

private Q_SLOTS:
    void default_constructor();
    void base_test_data();
    void base_test();

    void setName();
    void setMimetype();
    void setContent();
};


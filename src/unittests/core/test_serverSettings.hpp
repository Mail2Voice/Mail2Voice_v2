#pragma once

#include "../../core/serverSettings.hpp"
#include "propsTester.hpp"

#include <QObject>

class TestServerSettings : public QObject, public PropsTester<m2v::core::ServerSettings>
{
    Q_OBJECT
public:
    explicit TestServerSettings(QObject *parent = nullptr);

private Q_SLOTS:
    void default_constructor();
    void base_test_data();
    void base_test();

    void setType();
    void setAddress();
    void setPort();
    void setSsl();
};

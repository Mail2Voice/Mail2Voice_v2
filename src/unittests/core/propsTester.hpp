#pragma once

#include <functional>
#include <QList>
#include <QPair>
#include <QString>
#include <QStringList>

template <class T>
class PropsTester
{
public:
    PropsTester() = default;

protected:

    void checkDefaultProperties(const T& objectToTest, const QStringList& excludedProperties)
    {
        for(auto pair: m_propsChecklist)
        {
            if(!excludedProperties.contains(pair.first))
            {
                pair.second(objectToTest);
            }
        }
    }

protected:

    QList<QPair<QString, std::function<void(const T&)>>> m_propsChecklist;
};

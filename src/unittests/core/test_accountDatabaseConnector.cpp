#include <QTest>
#include <QList>
#include <QSqlQuery>
#include <QtDebug>
#include "test_accountDatabaseConnector.hpp"
#include "../../core/account.hpp"

Q_DECLARE_METATYPE(m2v::core::Account)

/**
 * @brief TestAccountDatabaseConnector::TestAccountDatabaseConnector
 * @param db
 * @param dbConnectionName
 * @param parent
 */
TestAccountDatabaseConnector::TestAccountDatabaseConnector(const QSqlDatabase& db,
                                                           const QString &dbConnectionName,
                                                           QObject *parent)
            : QObject(parent)
            , m_db (db)
            , m_dbConnectionName(dbConnectionName)
{
}

/**
 * @brief TestAccountDatabaseConnector::addAccount_data
 */
void
TestAccountDatabaseConnector::addAccount_data()
{
    QTest::addColumn<m2v::core::Account>("account");
    QTest::addColumn<bool>("shouldRaiseException");

    m2v::core::ServerSettings imapSettings(m2v::core::ServerType::Imap, "imap.server.com", 42, true);
    m2v::core::ServerSettings smtpSettings(m2v::core::ServerType::Smtp, "smtp.server.com", 0, true);

    {
        m2v::core::Account account;
        bool shouldRaiseException = true;
        QTest::newRow("Add account with empty email") << account << shouldRaiseException;
    }
    {
        m2v::core::Account account("", "", "", "mr.nobody@mail.com", imapSettings, smtpSettings);
        bool shouldRaiseException = false;
        QTest::newRow("Add account with email") << account << shouldRaiseException;
    }
    {
        m2v::core::Account account("nobody", "pass", "Mr Nobody", "mr.nobody@mail.com", imapSettings, smtpSettings);
        bool shouldRaiseException = false;
        QTest::newRow("Add account with all infos") << account << shouldRaiseException;
    }
}

/**
 * @brief TestAccountDatabaseConnector::addAccount
 */
void TestAccountDatabaseConnector::addAccount()
{
    QFETCH(m2v::core::Account, account);
    QFETCH(bool, shouldRaiseException);

    AccountDatabaseConnector accountDbConnector(m_dbConnectionName);

    try
    {
        accountDbConnector.addAccount(account);
        m_testAccountList.push_back(account);
    }
    catch(const std::runtime_error& e)
    {
        Q_UNUSED(e)
        QVERIFY(shouldRaiseException);
        return;
    }

    QList<m2v::core::Account> accountDbList = accountDbConnector.getAccountList();
    for(auto acc: accountDbList)
    {
        if(acc.id() == account.id())
        {
            QCOMPARE(acc.username(), account.username());
            QCOMPARE(acc.password(), account.password());
            QCOMPARE(acc.displayName(), account.displayName());
            QCOMPARE(acc.email(), account.email());
        }
    }
}

/**
 * @brief TestAccountDatabaseConnector::deleteAccount
 */
void TestAccountDatabaseConnector::deleteAccount()
{
    QList<m2v::core::Account> accountList;
    m2v::core::Account removeAcc;
    AccountDatabaseConnector accountDbConnector(m_dbConnectionName);
    accountList = accountDbConnector.getAccountList();
    removeAcc = accountList.at(1);
    m_testAccountList.removeAt(1);

    try
    {
        accountDbConnector.deleteAccount(removeAcc);
    }
    catch(const std::runtime_error& e)
    {
        // TODO: Test error handling.
        QFAIL(e.what());
    }

    //test case 2: delete an account that doesnt exist in the db
    QString username3 {"cici"};
    QString password3 {"mot"};
    QString displayName3 {"cici"};
    QString email3 {"email@email.com"};
    m2v::core::ServerSettings imapSettings(m2v::core::ServerType::Imap, "imap.server.com", 42, true);
    m2v::core::ServerSettings smtpSettings(m2v::core::ServerType::Smtp, "smtp.server.com", 0, true);
    bool exceptionRaised = false;

    m2v::core::Account accountThree(username3, password3, displayName3, email3, imapSettings, smtpSettings);
    try
    {
         accountDbConnector.deleteAccount(accountThree);
    }
    catch(const std::runtime_error& e)
    {
        exceptionRaised = true;
    }

    QVERIFY(exceptionRaised);
}

/**
 * @brief TestAccountDatabaseConnector::getAccountList
 */
void TestAccountDatabaseConnector::getAccountList()
{
    QList<m2v::core::Account> accountList;
    AccountDatabaseConnector accountDbConnector(m_dbConnectionName);
    accountList = accountDbConnector.getAccountList();
    QList<m2v::core::Account>::iterator it, test_it;
    QCOMPARE(accountList.size(), m_testAccountList.size());

    for(it = accountList.begin(), test_it = m_testAccountList.begin();
        it != accountList.end(), test_it != m_testAccountList.end(); it++, test_it++)
    {
        QCOMPARE((*it).id(), (*test_it).id());
        QCOMPARE((*it).username(), (*test_it).username());
        QCOMPARE((*it).password(), (*test_it).password());
        QCOMPARE((*it).displayName(), (*test_it).displayName());
        QCOMPARE((*it).email(), (*test_it).email());
    }
}

#pragma once

#include "../../core/contact.hpp"
#include "propsTester.hpp"

#include <QObject>

class TestContact : public QObject, PropsTester<m2v::core::Contact>
{
    Q_OBJECT

public:
    explicit TestContact(QObject *parent = nullptr);

private Q_SLOTS:
    void default_constructor();
    void base_test_data();
    void base_test();

    void setEmail();
    void setName();
    void setPortrait();
    void setGroups();
};

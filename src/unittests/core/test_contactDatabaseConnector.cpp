#include <QTest>
#include <QList>
#include <QSqlQuery>
#include <QDebug>
#include "test_contactDatabaseConnector.hpp"
#include "../../core/contact.hpp"

/**
 * @brief TestContactDatabaseConnector::TestContactDatabaseConnector
 * @param db
 * @param dbConnectionName
 * @param parent
 */
TestContactDatabaseConnector::TestContactDatabaseConnector(const QSqlDatabase& db,
                                                           const QString &dbConnectionName,
                                                           QObject *parent)
        : QObject(parent)
        , m_db (db)
        , m_dbConnectionName (dbConnectionName)
{
}

/**
 * @brief TestContactDatabaseConnector::addContact_data
 */
void TestContactDatabaseConnector::addContact_data()
{
    QTest::addColumn<m2v::core::Contact>("contact");
    QTest::addColumn<bool>("shouldRaiseException");

    {
        m2v::core::Contact contact;
        bool shouldRaiseException = true;
        QTest::newRow("Add empty contact") << contact << shouldRaiseException;
    }
    {
        m2v::core::Contact contact("mr.nobody@mail.com");
        bool shouldRaiseException = false;
        QTest::newRow("Add contact with only email address") << contact << shouldRaiseException;
    }
    {
        m2v::core::Contact contact("mr.nobody@mail.com", "Jack Nobody", "jack.png");
        bool shouldRaiseException = false;
        QTest::newRow("Add contact with few infos") << contact << shouldRaiseException;
    }

    {
        m2v::core::Contact contact("mary@yahoo.com", "Jack Nobody", "jack.png", {"family", "friends"});
        bool shouldRaiseException = false;
        QTest::newRow("Add contact with groups") << contact << shouldRaiseException;
    }
}

/**
 * @brief TestContactDatabaseConnector::addContact
 */
void TestContactDatabaseConnector::addContact()
{
    QFETCH(m2v::core::Contact, contact);
    QFETCH(bool, shouldRaiseException);

    ContactDatabaseConnector contactDbConnector(m_dbConnectionName);

    try
    {
        contactDbConnector.addContact(contact);
        m_testContactList.push_back(contact);
    }
    catch (const std::runtime_error& e)
    {
        QVERIFY(shouldRaiseException);
        return;
    }


    QList<m2v::core::Contact> contactDbList = contactDbConnector.getContactList();
    for(auto contactItem: contactDbList)
    {
        if(contactItem.id() == contact.id())
        {
            QCOMPARE(contactItem.email(), contact.email());
            QCOMPARE(contactItem.name(), contact.name());
            QCOMPARE(contactItem.portrait(), contact.portrait());
            QCOMPARE(contactItem.groups(), contact.groups());
        }
    }
}

/**
 * @brief TestContactDatabaseConnector::deleteContact
 */
void TestContactDatabaseConnector::deleteContact()
{
    QList<m2v::core::Contact> contactList;
    m2v::core::Contact removeContact;
    ContactDatabaseConnector contactDbConnector(m_dbConnectionName);
    contactList = contactDbConnector.getContactList();
    removeContact = contactList.at(1);
    m_testContactList.removeAt(1);

    try
    {
        contactDbConnector.deleteContact(removeContact);
    }
    catch(const std::runtime_error& e)
    {
        // TODO: Test error handling.
        QFAIL(e.what());
    }

    //test case 2: delete an contact that doesnt exist in the db
    QString email {"tony@yahoo.com"};
    QString name {"Tony Big"};
    QString portrait {"tony.png"};
    QStringList groups {"family"};

    m2v::core::Contact contactNotExist(email, name, portrait, groups);
    bool exceptionRaised = false;

    try
    {
        contactDbConnector.deleteContact(contactNotExist);
    }
    catch(const std::runtime_error& e)
    {
        exceptionRaised = true;
    }

    QVERIFY(exceptionRaised);
}

/**
 * @brief TestContactDatabaseConnector::getContactList
 */
void TestContactDatabaseConnector::getContactList()
{
    QList<m2v::core::Contact> contactList;
    ContactDatabaseConnector contactDbConnector(m_dbConnectionName);
    contactList = contactDbConnector.getContactList();
    QList<m2v::core::Contact>::iterator it, test_it;
    QCOMPARE(contactList.size(), m_testContactList.size());

    for(it = contactList.begin(), test_it = m_testContactList.begin();
        it != contactList.end(), test_it != m_testContactList.end(); ++it, ++test_it)
    {
        QCOMPARE((*it).id(), (*test_it).id());
        QCOMPARE((*it).email(), (*test_it).email());
        QCOMPARE((*it).portrait(), (*test_it).portrait());
        QCOMPARE((*it).groups(), (*test_it).groups());
    }
}

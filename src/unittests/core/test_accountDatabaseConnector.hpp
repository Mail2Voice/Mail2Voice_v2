#pragma once

#include "../../core/database/accountDatabaseConnector.hpp"
#include "propsTester.hpp"

#include <QObject>
#include <QSqlDatabase>

class TestAccountDatabaseConnector : public QObject, public PropsTester<m2v::core::Account>
{
        Q_OBJECT
    public:
        explicit TestAccountDatabaseConnector(const QSqlDatabase& db,
                                              const QString& dbConnectionName,
                                              QObject *parent = nullptr);

    private Q_SLOTS:
        void addAccount_data();
        void addAccount();
        void deleteAccount();
        void getAccountList();

    private:
        QSqlDatabase m_db;
        QString m_dbConnectionName;
        QList<m2v::core::Account> m_testAccountList;
};

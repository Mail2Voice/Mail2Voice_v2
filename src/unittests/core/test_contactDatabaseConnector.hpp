#pragma once

#include "../../core/database/contactDatabaseConnector.hpp"
#include "propsTester.hpp"

#include <QObject>
#include <QSqlDatabase>

class TestContactDatabaseConnector : public QObject, public PropsTester<m2v::core::Contact>
{
        Q_OBJECT
    public:
        explicit TestContactDatabaseConnector(const QSqlDatabase& db,
                                              const QString& dbConnectionName,
                                              QObject *parent = nullptr);

    private Q_SLOTS:
        void addContact_data();
        void addContact();
        void deleteContact();
        void getContactList();

    private:
        QSqlDatabase m_db;
        QString m_dbConnectionName;
        QList<m2v::core::Contact> m_testContactList;
};

#pragma once

#include "../../core/database/emailDatabaseConnector.hpp"
#include "propsTester.hpp"

#include <QObject>
#include <QSqlDatabase>

class TestEmailDatabaseConnector : public QObject, public PropsTester<m2v::core::Email>
{
    Q_OBJECT

    public:
        explicit TestEmailDatabaseConnector(const QSqlDatabase& db,
                                            const QString& dbConnectionName,
                                            QObject *parent = nullptr);

    private Q_SLOTS:
        void addEmail_data();
        void addEmail();
        void deleteEmail();
        void getEmailList();

    private:
        QSqlDatabase m_db;
        QString m_dbConnectionName;
        QList<m2v::core::Email> m_testEmailList;
};

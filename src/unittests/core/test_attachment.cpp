#include <QTest>
#include "test_attachment.hpp"

using namespace m2v::core;

using A = const Attachment&;

TestAttachment::TestAttachment(QObject *parent) :
    QObject(parent)
{
    m_propsChecklist.append({"name", [](A attachment) { QCOMPARE(attachment.name(), QString()); }});
    m_propsChecklist.append({"mimetype", [](A attachment) { QCOMPARE(attachment.mimetype(), QString()); }});
    m_propsChecklist.append({"content", [](A attachment) { QCOMPARE(attachment.content(), QByteArray()); }});
}

void TestAttachment::default_constructor()
{
    Attachment attachment;
    checkDefaultProperties(attachment, {});
}

void TestAttachment::base_test_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("mimeType");
    QTest::addColumn<QByteArray>("content");

    QTest::newRow("empty properties") << QString() << QString() << QByteArray();
    QTest::newRow("basic test") << QString("data") << QString("plain/text") << QByteArray();
    QTest::newRow("basic test with content") << QString("data") << QString("plain/text") << QByteArray("data");
    QTest::newRow("empty name") << QString() << QString("plain/text") << QByteArray();
    QTest::newRow("content only") << QString() << QString() << QByteArray("data");
}

void TestAttachment::base_test()
{
    QFETCH(QString, name);
    QFETCH(QString, mimeType);
    QFETCH(QByteArray, content);

    Attachment attachment(name, mimeType, content);
    Attachment attachmentCopyByConstructor(attachment);
    Attachment attachmentCopyByAssignment = attachment;

    QList<Attachment*> attachmentsToTest({&attachment, &attachmentCopyByConstructor, &attachmentCopyByAssignment});

    for(auto att: attachmentsToTest)
    {
        QCOMPARE(att->name(), name);
        QCOMPARE(att->mimetype(), mimeType);
        QCOMPARE(att->content(), content);
    }
}

void TestAttachment::setName()
{
    Attachment attachment;
    attachment.setName("file.txt");

    QCOMPARE(attachment.name(), QString("file.txt"));

    checkDefaultProperties(attachment, {"name"});
}

void TestAttachment::setMimetype()
{
    Attachment attachment;
    attachment.setMimetype("plain/text");

    QCOMPARE(attachment.mimetype(), QString("plain/text"));

    checkDefaultProperties(attachment, {"mimetype"});
}

void TestAttachment::setContent()
{
    Attachment attachment;
    QByteArray content("data");
    attachment.setContent(content);

    QCOMPARE(attachment.content(), content);

    checkDefaultProperties(attachment, {"content"});
}

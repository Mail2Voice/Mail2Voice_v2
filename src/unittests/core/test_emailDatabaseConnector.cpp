#include <QTest>
#include <QList>
#include <QSqlQuery>
#include <QDebug>
#include "test_emailDatabaseConnector.hpp"
#include "../../core/email.hpp"
#include "../../core/attachment.hpp"

/**
 * @brief TestEmailDatabaseConnector::TestEmailDatabaseConnector
 * @param db
 * @param dbConnectionName
 * @param parent
 */
TestEmailDatabaseConnector::TestEmailDatabaseConnector( const QSqlDatabase& db,
                                                            const QString& dbConnectionName,
                                                            QObject *parent)
        : QObject(parent),
          m_db (db),
          m_dbConnectionName (dbConnectionName)
{
}

/**
 * @brief TestEmailDatabaseConnector::addEmail_data
 */
void TestEmailDatabaseConnector::addEmail_data()
{
    QTest::addColumn<m2v::core::Email> ("email");

    //email1
    QString messageId {"fDEDS"};
    QString subject { "Hello!" };
    QStringList senders {"Mary@yahoo.com"};
    QStringList from {"Mary@yahoo.com"};
    QStringList recipientsList {"John@mail.com", "Geo@mail.com"};
    QStringList carbonCopyList  {"Laura@yahoo.com", "Ana@mail.com"};
    QStringList blindCarbonCopyList {"Rico@mail.com"};
    QDateTime date {QDateTime::currentDateTime()};
    QString content  {"How are you?"};

    //attachement 1
    QString atchName {"n"};
    QString atchMimeType {"n"};
    QByteArray atchContent {"a"};
    m2v::core::Attachment a {atchName, atchMimeType, atchContent};
    std::shared_ptr<m2v::core::Attachment> ptrAtt (new m2v::core::Attachment);
    ptrAtt = std::make_shared<m2v::core::Attachment> (a);

    //attachement 2
    QString atchNameSecond {"name"};
    QString atchMimeSecond {"mime"};
    QByteArray atchContentSecond {"content"};
    m2v::core::Attachment aSecond {atchNameSecond, atchMimeSecond, atchContentSecond};
    std::shared_ptr<m2v::core::Attachment> ptrAttSecond (new m2v::core::Attachment);
    ptrAttSecond= std::make_shared<m2v::core::Attachment> (aSecond);

    QList<std::shared_ptr<m2v::core::Attachment>> attlist {ptrAtt, ptrAttSecond};
    m2v::core::AttachmentList attachmentList {attlist};
    QStringList extraHeadersList {"1", "2"};
    bool read {false};
    QString folder {"Perso"};

    m2v::core::Email email;
    email.setAccountId(1);
    email.setMessageId(messageId);
    email.setSubject(subject);
    email.setSenders(senders);
    email.setFrom(from);
    email.setRecipients(recipientsList);
    email.setCarbonCopy(carbonCopyList);
    email.setBlindCopy(blindCarbonCopyList);
    email.setDate(date);
    email.setContent(content);
    email.setAttachments(attachmentList);
    email.setExtraHeader(extraHeadersList);
    email.setRead(read);
    email.setFolder(folder);

    //email2
    QString messageId2 {"fDEDS"};
    QString subject2 { "File" };
    QStringList senders2 {"Newtown@yahoo.com"};
    QStringList from2 {"Newtown@yahoo.com"};
    QStringList listRecipients2 {"m.orly@mail.com"};
    QDateTime date2 {QDateTime::currentDateTime()};
    QString content2 {"Please sign this paper"};
    QString atchName2 {"n"};
    QString atchMimeType2 {"n"};
    QByteArray atchContent2 {"a"};
    m2v::core::Attachment a2 {atchName2, atchMimeType2, atchContent2};
    std::shared_ptr<m2v::core::Attachment> ptrAtt2 (new m2v::core::Attachment);
    ptrAtt2 = std::make_shared<m2v::core::Attachment> (a2);
    QList<std::shared_ptr<m2v::core::Attachment>> attlist2 {ptrAtt2};
    m2v::core::AttachmentList attachmentList2 {attlist2};
    QStringList extraHeadersList2 {"1", "2"};
    bool read2 {false};
    QString folder2 {"Work"};

    m2v::core::Email emailTwo;
    emailTwo.setAccountId(1);
    emailTwo.setMessageId(messageId2);
    emailTwo.setSubject(subject2);
    emailTwo.setSenders(senders2);
    emailTwo.setFrom(from2);
    emailTwo.setRecipients(listRecipients2);
    emailTwo.setDate(date2);
    emailTwo.setContent(content2);
    emailTwo.setAttachments(attachmentList2);
    emailTwo.setExtraHeader(extraHeadersList2);
    emailTwo.setRead(read2);
    emailTwo.setFolder(folder2);

    //email3
    QString messageId3 {"fDEDS"};
    QString subject3 { "Re: Re: Hello!" };
    QStringList senders3 {"Andi@yahoo.com"};
    QStringList from3 {"Andi@yahoo.com"};
    QStringList listRecipients3 {"John@mail.com"};
    QDateTime date3 {QDateTime::currentDateTime()};
    QString content3 {"Ce soir rdv a 19h00"};
    QString atchName3 {"n"};
    QString atchMimeType3 {"n"};
    QByteArray atchContent3 {"a"};
    m2v::core::Attachment a3 {atchName3, atchMimeType3, atchContent3};
    std::shared_ptr<m2v::core::Attachment> ptrAtt3 (new m2v::core::Attachment);
    ptrAtt3 = std::make_shared<m2v::core::Attachment> (a3);
    QList<std::shared_ptr<m2v::core::Attachment>> attlist3 {ptrAtt3};
    m2v::core::AttachmentList attachmentList3 {attlist3};
    QStringList extraHeadersList3 {"1", "2"};
    bool read3 = true;
    QString folder3 {"Amis"};

    m2v::core::Email emailThree;
    emailThree.setAccountId(3);
    emailThree.setMessageId(messageId3);
    emailThree.setSubject(subject3);
    emailThree.setSenders(senders3);
    emailThree.setFrom(from3);
    emailThree.setRecipients(listRecipients3);
    emailThree.setDate(date3);
    emailThree.setContent(content3);
    emailThree.setAttachments(attachmentList3);
    emailThree.setExtraHeader(extraHeadersList3);
    emailThree.setRead(read3);
    emailThree.setFolder(folder3);

    QTest::newRow("First email")  << email ;
    QTest::newRow("Second email") << emailTwo ;
    QTest::newRow("Third email")  << emailThree;
}

/**
 * @brief TestEmailDatabaseConnector::addEmail
 */
void TestEmailDatabaseConnector::addEmail()
{
    QFETCH(m2v::core::Email, email);
    EmailDatabaseConnector emailDbConnector(m_dbConnectionName);

    try
    {
        emailDbConnector.addEmail(email);
        m_testEmailList.push_back(email);
    }
    catch(const std::runtime_error& e)
    {
        // TODO: Test error handling properly.
        QFAIL(e.what());
    }

    QList<m2v::core::Email> emailDbList = emailDbConnector.getEmailList();
    for(auto emailItem: emailDbList)
    {
        if(emailItem.id() == email.id())
        {
            QCOMPARE(emailItem.accountId(), email.accountId());
            QCOMPARE(emailItem.messageId(), email.messageId());
            QCOMPARE(emailItem.subject(), email.subject());
            QCOMPARE(emailItem.senders(), email.senders());
            QCOMPARE(emailItem.from(), email.from());
            QCOMPARE(emailItem.recipients(), email.recipients());
            QCOMPARE(emailItem.carbonCopy(), email.carbonCopy());
            QCOMPARE(emailItem.blindCarbonCopy(), email.blindCarbonCopy());
            QCOMPARE(emailItem.date(), email.date());
            QCOMPARE(emailItem.content(), email.content());

            QCOMPARE(emailItem.attachments().size(), email.attachments().size());
            for(int i=0; i<emailItem.attachments().size(); ++i)
            {
                QCOMPARE(emailItem.attachments()[i]->name(), email.attachments()[i]->name());
                QCOMPARE(emailItem.attachments()[i]->mimetype(), email.attachments()[i]->mimetype());
                QCOMPARE(emailItem.attachments()[i]->content(), email.attachments()[i]->content());
            }

            QCOMPARE(emailItem.extraHeaders(), email.extraHeaders());
            QCOMPARE(emailItem.read(), email.read());
            QCOMPARE(emailItem.folder(), email.folder());
        }
    }
}

/**
 * @brief TestEmailDatabaseConnector::deleteEmail
 */
void TestEmailDatabaseConnector::deleteEmail()
{
    QList<m2v::core::Email> emailList;
    m2v::core::Email removeEmail;
    EmailDatabaseConnector emailDbConnector(m_dbConnectionName);
    emailList = emailDbConnector.getEmailList();
    removeEmail = emailList.at(1);
    m_testEmailList.removeAt(1);

    try
    {
        emailDbConnector.deleteEmail(removeEmail);
    }
    catch(const std::runtime_error& e)
    {
        // TODO: Test error handling.
        QFAIL(e.what());
    }

    //test case 2: delete an email that doesnt exist in the db
    m2v::core::Email emailNotExist;
    emailNotExist.setId(7);
    emailNotExist.setAccountId(4);

    bool exceptionRaised = false;

    try
    {
        emailDbConnector.deleteEmail(emailNotExist);
    }
    catch (const std::runtime_error& e)
    {
        exceptionRaised = true;
    }

    QVERIFY(exceptionRaised);
}

/**
 * @brief TestEmailDatabaseConnector::getEmailList
 */
void TestEmailDatabaseConnector::getEmailList()
{
    QList<m2v::core::Email> emailList;
    EmailDatabaseConnector emailDbConnector(m_dbConnectionName);
    emailList = emailDbConnector.getEmailList();
    QList<m2v::core::Email>::iterator it, test_it;
    QCOMPARE(emailList.size(), m_testEmailList.size());

    for(it = emailList.begin(), test_it = m_testEmailList.begin();
        it != emailList.end(), test_it != m_testEmailList.end(); it++, test_it++)
    {
        QCOMPARE((*it).id(), (*test_it).id());
        QCOMPARE((*it).accountId(), (*test_it).accountId());
        QCOMPARE((*it).messageId(), (*test_it).messageId());
        QCOMPARE((*it).subject(), (*test_it).subject());
        QCOMPARE((*it).senders(), (*test_it).senders());
        QCOMPARE((*it).from(), (*test_it).from());
        QCOMPARE((*it).recipients(), (*test_it).recipients());
        QCOMPARE((*it).carbonCopy(), (*test_it).carbonCopy());
        QCOMPARE((*it).blindCarbonCopy(), (*test_it).blindCarbonCopy());
        QCOMPARE((*it).date(), (*test_it).date());
        QCOMPARE((*it).content(), (*test_it).content());

        QCOMPARE((*it).attachments().size(), (*test_it).attachments().size());
        for(int i=0; i<(*it).attachments().size(); ++i)
        {
            QCOMPARE((*it).attachments()[i]->name(), (*test_it).attachments()[i]->name());
            QCOMPARE((*it).attachments()[i]->mimetype(), (*test_it).attachments()[i]->mimetype());
            QCOMPARE((*it).attachments()[i]->content(), (*test_it).attachments()[i]->content());
        }

        QCOMPARE((*it).extraHeaders(), (*test_it).extraHeaders());
        QCOMPARE((*it).read(), (*test_it).read());
        QCOMPARE((*it).folder(), (*test_it).folder());
    }
}

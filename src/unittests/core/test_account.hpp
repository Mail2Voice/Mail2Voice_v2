#pragma once

#include "../../core/account.hpp"
#include "propsTester.hpp"

#include <QObject>

class TestAccount : public QObject, public PropsTester<m2v::core::Account>
{
    Q_OBJECT

public:
    explicit TestAccount(QObject *parent = nullptr);

private Q_SLOTS:
    void default_constructor();
    void base_test_data();
    void base_test();

    void setUsername();
    void setDisplayName();
    void setEmail();
    void setStoreSettings();
    void setTransportSettings();
};

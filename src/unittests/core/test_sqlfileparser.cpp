#include "test_sqlfileparser.hpp"

TestSqlFileParser::TestSqlFileParser()
{
}

TestSqlFileParser::~TestSqlFileParser()
{
}

void TestSqlFileParser::test_empty_file_data()
{
  QTest::addColumn<QString>("filename");
  QTest::addColumn<QStringList>("expectedSqlRequests");
  QTest::addColumn<bool>("parseSuccess");

  {
    QString filename {":/sql/single_line_request.sql"};
    QStringList expectedSqlRequests {QStringLiteral("SELECT * FROM emails;")};
    bool parseSuccess {true};
    QTest::newRow("Single line request") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/bad_single_line_request.sql"};
    QStringList expectedSqlRequests {};
    bool parseSuccess {false};
    QTest::newRow("Bad single line request") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/multi_line_request.sql"};
    QStringList expectedSqlRequests {QStringLiteral("CREATE TABLE emails (INT 'id', TEXT 'email', TEXT 'name');")};
    bool parseSuccess {true};
    QTest::newRow("Multiple line request") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/multi_line_request_2.sql"};
    QStringList expectedSqlRequests {QStringLiteral("CREATE TABLE emails (INT 'id', TEXT 'email', TEXT 'name' );")};
    bool parseSuccess {true};
    QTest::newRow("Multiple line request 2") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/bad_multi_line_request.sql"};
    QStringList expectedSqlRequests {};
    bool parseSuccess {false};
    QTest::newRow("Bad multiple line request") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/multiple_requests.sql"};
    QStringList expectedSqlRequests {QStringLiteral("CREATE TABLE emails (INT 'id', TEXT 'email', TEXT 'name');"),
                                     QStringLiteral("INSERT INTO emails VALUES(0, 'bla', 'bla');"),
                                     QStringLiteral("SELECT * FROM emails;")};
    bool parseSuccess {true};
    QTest::newRow("Multiple requests") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/bad_multiple_requests.sql"};
    QStringList expectedSqlRequests {};
    bool parseSuccess {false};
    QTest::newRow("Bad multiple requests") << filename << expectedSqlRequests << parseSuccess;
  }
  {
    QString filename {":/sql/mix.sql"};
    QStringList expectedSqlRequests {QStringLiteral("CREATE TABLE emails (INT 'id', TEXT 'email', TEXT 'name');"),
                                     QStringLiteral("INSERT INTO emails VALUES(0, 'bla', 'bla');"),
                                     QStringLiteral("SELECT * FROM emails;")};
    bool parseSuccess {true};
    QTest::newRow("Mix") << filename << expectedSqlRequests << parseSuccess;
  }
}

void TestSqlFileParser::test_empty_file()
{
  QFETCH(QString, filename);
  QFETCH(QStringList, expectedSqlRequests);
  QFETCH(bool, parseSuccess);

  bool isParseSucceed {false};
  SqlFileParser fileParser;

  QStringList parsedSqlRequests = fileParser.parse(filename, &isParseSucceed);

  QCOMPARE(isParseSucceed, parseSuccess);
  QCOMPARE(parsedSqlRequests, expectedSqlRequests);
}

#include <QTest>
#include <QString>
#include "test_contact.hpp"

using namespace m2v::core;

using C = const Contact&;

TestContact::TestContact(QObject *parent) :
    QObject(parent)
{
    m_propsChecklist.append({"email",    [](C contact) { QCOMPARE(contact.email(), QString()); }});
    m_propsChecklist.append({"name",     [](C contact) { QCOMPARE(contact.name(), QString()); }});
    m_propsChecklist.append({"portrait", [](C contact) { QCOMPARE(contact.portrait(), QString()); }});
    m_propsChecklist.append({"groups",   [](C contact) { QCOMPARE(contact.groups(), QStringList()); }});
}

void TestContact::default_constructor()
{
    Contact contact;
    checkDefaultProperties(contact, {});
}

void TestContact::base_test_data()
{
    QTest::addColumn<QString>("email");
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("portrait");
    QTest::addColumn<QStringList>("groups");

    QTest::newRow("construct with email") << QString("mail@example.com") << QString()
                                          << QString() << QStringList();

    QTest::newRow("construct with email, name") << QString("mail@example.com") << QString("Nobody")
                                                << QString() << QStringList();

    QTest::newRow("construct with email, name, portrait") << QString("mail@example.com") << QString("Nobody")
                                                          << QString("/path/to/portrait") << QStringList();

    QTest::newRow("construct with email, name, portrait, groups") << QString("mail@example.com") << QString("Nobody")
                                                                  << QString("/path/to/portrait") << QStringList({"group1", "group2"});
}


void TestContact::base_test()
{
    QFETCH(QString, email);
    QFETCH(QString, name);
    QFETCH(QString, portrait);
    QFETCH(QStringList, groups);

    Contact contact(email, name, portrait, groups);
    Contact contactCopyByConstructor(contact);
    Contact contactCopyByAssignment = contact;

    QList<Contact*> contactsToTest({&contact, &contactCopyByConstructor, &contactCopyByAssignment});

    for(auto cont: contactsToTest)
    {
        QCOMPARE(cont->email(), email);
        QCOMPARE(cont->name(), name);
        QCOMPARE(cont->portrait(), portrait);
        QCOMPARE(cont->groups(), groups);
    }
}

void TestContact::setEmail()
{
    Contact contact;
    contact.setEmail("mail@example.com");

    QCOMPARE(contact.email(), QString("mail@example.com"));

    checkDefaultProperties(contact, {"email"});
}

void TestContact::setName()
{
    Contact contact;
    contact.setName("Nobody");

    QCOMPARE(contact.name(), QString("Nobody"));;

    checkDefaultProperties(contact, {"name"});
}

void TestContact::setPortrait()
{
    Contact contact;
    contact.setPortrait("/path/to/portrait");

    QCOMPARE(contact.portrait(), QString("/path/to/portrait"));

    checkDefaultProperties(contact, {"portrait"});
}

void TestContact::setGroups()
{
    QStringList groups{"group1", "group2"};
    Contact contact;
    contact.setGroups(groups);

    QCOMPARE(contact.groups(), groups);

    checkDefaultProperties(contact, {"groups"});
}

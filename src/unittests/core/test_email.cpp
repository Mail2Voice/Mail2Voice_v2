#include <QTest>

#include "test_email.hpp"
#include <QVector>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include <memory>

using namespace m2v::core;

using E = const Email&;

TestEmail::TestEmail(QObject *parent) :
    QObject(parent)
{
    m_propsChecklist.append({"subject",         [](E email) { QCOMPARE(email.subject(), QString()); }});
    m_propsChecklist.append({"senders",         [](E email) { QCOMPARE(email.senders(), QStringList()); }});
    m_propsChecklist.append({"from",            [](E email) { QCOMPARE(email.from(), QStringList()); }});
    m_propsChecklist.append({"recipients",      [](E email) { QCOMPARE(email.recipients(), QStringList()); }});
    m_propsChecklist.append({"carbonCopy",      [](E email) { QCOMPARE(email.carbonCopy(), QStringList()); }});
    m_propsChecklist.append({"blindCarbonCopy", [](E email) { QCOMPARE(email.blindCarbonCopy(), QStringList()); }});
    m_propsChecklist.append({"date",            [](E email) { QCOMPARE(email.date(), QDateTime()); }});
    m_propsChecklist.append({"content",         [](E email) { QCOMPARE(email.content(), QString()); }});
    m_propsChecklist.append({"attachments",     [](E email) { QCOMPARE(email.attachments(), AttachmentList()); }});
    m_propsChecklist.append({"extraHeaders",    [](E email) { QCOMPARE(email.extraHeaders(), QStringList()); }});
    m_propsChecklist.append({"read",            [](E email) { QCOMPARE(email.read(), false); }});
    m_propsChecklist.append({"folder",          [](E email) { QCOMPARE(email.folder(), QString()); }});
}

void TestEmail::default_constructor()
{
    Email email;
    checkDefaultProperties(email, {});
}

void TestEmail::base_test_data()
{
    QTest::addColumn<QString>("subject");
    QTest::addColumn<QStringList>("senders");
    QTest::addColumn<QStringList>("from");
    QTest::addColumn<QStringList>("recipients");
    QTest::addColumn<QStringList>("carbonCopy");
    QTest::addColumn<QStringList>("blindCopy");
    QTest::addColumn<QDateTime>("date");
    QTest::addColumn<QString>("content");
    QTest::addColumn<QStringList>("attachmentContents");
    QTest::addColumn<QStringList>("extraHeaders");
    QTest::addColumn<bool>("read");
    QTest::addColumn<QString>("folder");

    {
        QTest::newRow("Basic email") << QString("Test subject") << QString("sender@example.com")
                                << QStringList({"recipient1@mail.org"})
                                << QStringList()
                                << QStringList()
                                << QDateTime::currentDateTime() << QString("Test content")
                                << QStringList() << QStringList()
                                << false << QString("INBOX");
    }

    {
        QTest::newRow("Email with carbon and hidden copies") << QString("Test subject") << QString("sender@example.com")
                                << QStringList({"recipient1@mail.org", "recipient2@mail.org"})
                                << QStringList({"copyRecipient1@mail.org", "copyRecipient2@mail.org"})
                                << QStringList({"hiddenRecipient1@mail.org", "hiddenRecipient2@mail.org"})
                                << QDateTime::currentDateTime() << QString("Test content")
                                << QStringList() << QStringList()
                                << false << QString("INBOX");
    }

    {
        QTest::newRow("Email with attachments and extra headers") << QString("Test subject") << QString("sender@example.com")
                                << QStringList({"recipient1@mail.org", "recipient2@mail.org"})
                                << QStringList({"copyRecipient1@mail.org", "copyRecipient2@mail.org"})
                                << QStringList({"hiddenRecipient1@mail.org", "hiddenRecipient2@mail.org"})
                                << QDateTime::currentDateTime() << QString("Test content")
                                << QStringList({"Attachment content 1", "Attachment content 2"})
                                << QStringList({"Header1", "Header2"})
                                << true << QString("INBOX");
    }
}

void TestEmail::base_test()
{
    QFETCH(QString, subject);
    QFETCH(QStringList, senders);
    QFETCH(QStringList, from);
    QFETCH(QStringList, recipients);
    QFETCH(QStringList, carbonCopy);
    QFETCH(QStringList, blindCopy);
    QFETCH(QDateTime, date);
    QFETCH(QString, content);
    QFETCH(QStringList, attachmentContents);
    QFETCH(QStringList, extraHeaders);
    QFETCH(bool, read);
    QFETCH(QString, folder);

    AttachmentList attachments;

    for(QString attachmentContent: attachmentContents)
    {
        std::shared_ptr<Attachment> attachment = std::make_shared<Attachment>("attachment",
                                                                              "mime/test",
                                                                              QByteArray(attachmentContent.toStdString().c_str()));
        attachments.append(attachment);
    }

    Email email;
    email.setSubject(subject);
    email.setSenders(senders);
    email.setFrom(from);
    email.setRecipients(recipients);
    email.setCarbonCopy(carbonCopy);
    email.setBlindCopy(blindCopy);
    email.setDate(date);
    email.setContent(content);
    email.setAttachments(attachments);
    email.setExtraHeader(extraHeaders);
    email.setRead(read);
    email.setFolder(folder);

    Email emailCopyByConstructor(email);
    Email emailCopyByAssignment = email;

    QList<Email*> emailsToTest({&email, &emailCopyByConstructor, &emailCopyByAssignment});

    for(auto em: emailsToTest)
    {
        QCOMPARE(em->subject(), subject);
        QCOMPARE(em->senders(), senders);
        QCOMPARE(em->from(), from);
        QCOMPARE(em->recipients(), recipients);
        QCOMPARE(em->carbonCopy(), carbonCopy);
        QCOMPARE(em->blindCarbonCopy(), blindCopy);
        QCOMPARE(em->date(), date);
        QCOMPARE(em->content(), content);
        QCOMPARE(em->attachments(), attachments);
        QCOMPARE(em->extraHeaders(), extraHeaders);
        QCOMPARE(em->read(), read);
        QCOMPARE(em->folder(), folder);
    }
}

void TestEmail::setSubject()
{
    Email email;
    email.setSubject("Subject");
    QCOMPARE(email.subject(), QString("Subject"));

    checkDefaultProperties(email, {"subject"});
}

void TestEmail::setSenders()
{
    Email email;
    email.setSenders({"sender@mail.com", "sender2@mail.com"});
    QCOMPARE(email.senders(), QStringList({"sender@mail.com", "sender2@mail.com"}));

    checkDefaultProperties(email, {"senders"});
}

void TestEmail::setFrom()
{
    Email email;
    email.setFrom({"from@mail.com", "from2@mail.com"});
    QCOMPARE(email.from(), QStringList({"from@mail.com", "from2@mail.com"}));

    checkDefaultProperties(email, {"from"});
}

void TestEmail::setRecipients()
{
    Email email;
    QStringList recipients{"recipient1@mail.com", "recipient2@mail.com"};
    email.setRecipients(recipients);
    QCOMPARE(email.recipients(), recipients);

    checkDefaultProperties(email, {"recipients"});
}

void TestEmail::setCarbonCopy()
{
    Email email;
    QStringList carbonCopy{"recipient1@mail.com", "recipient2@mail.com"};
    email.setCarbonCopy(carbonCopy);
    QCOMPARE(email.carbonCopy(), carbonCopy);

    checkDefaultProperties(email, {"carbonCopy"});
}

void TestEmail::setBlindCarbonCopy()
{
    Email email;
    QStringList blindCarbonCopy{"recipient1@mail.com", "recipient2@mail.com"};
    email.setBlindCopy(blindCarbonCopy);
    QCOMPARE(email.blindCarbonCopy(), blindCarbonCopy);

    checkDefaultProperties(email, {"blindCarbonCopy"});
}

void TestEmail::setDate()
{
    Email email;
    QDateTime date = QDateTime::currentDateTime();
    email.setDate(date);
    QCOMPARE(email.date(), date);

    checkDefaultProperties(email, {"date"});
}

void TestEmail::setContent()
{
    Email email;
    email.setContent("Content");
    QCOMPARE(email.content(),  QString("Content"));

    checkDefaultProperties(email, {"content"});
}

void TestEmail::setAttachments()
{
    Email email;
    std::shared_ptr<Attachment> attachment1 = std::make_shared<Attachment>();
    std::shared_ptr<Attachment> attachment2 = std::make_shared<Attachment>();
    AttachmentList attachments{attachment1, attachment2};
    email.setAttachments(attachments);
    QCOMPARE(email.attachments(), attachments);

    checkDefaultProperties(email, {"attachments"});
}

void TestEmail::setExtraHeaders()
{
    Email email;
    QStringList extraHeaders{"header1", "header2"};
    email.setExtraHeader(extraHeaders);
    QCOMPARE(email.extraHeaders(), extraHeaders);

    checkDefaultProperties(email, {"extraHeaders"});
}

void TestEmail::setRead()
{
    Email email;
    email.setRead(true);
    QCOMPARE(email.read(), true);

    checkDefaultProperties(email, {"read"});
}

void TestEmail::setFolder()
{
    Email email;
    email.setFolder("INBOX");
    QCOMPARE(email.folder(), QString("INBOX"));

    checkDefaultProperties(email, {"folder"});
}

void TestEmail::appendAttachment_data()
{
    QTest::addColumn<int>("attachmentsCount");

    QTest::newRow("single attachment") << 1;
    QTest::newRow("multiple attachments") << 10;
    QTest::newRow("multiple attachments") << 100;
}


void TestEmail::appendAttachment()
{
    QFETCH(int, attachmentsCount);
    Email email;

    QList<std::shared_ptr<Attachment>> attachments;

    for(int i = 0; i < attachmentsCount; i++)
    {
        std::shared_ptr<Attachment> attachment = std::make_shared<Attachment>();
        attachments << attachment;
        email.appendAttachment(attachment);
    }

    QCOMPARE(email.attachments(), attachments);
}

void TestEmail::removeAttachmentByReferenceRight()
{
    Email email;
    std::shared_ptr<Attachment> attachment1 = std::make_shared<Attachment>();

    email.appendAttachment(attachment1);
    email.removeAttachment(attachment1);

    QCOMPARE(email.attachments().empty(), true);
}

void TestEmail::removeAttachmentByReferenceWrong()
{
    Email email;
    std::shared_ptr<Attachment> attachment1 = std::make_shared<Attachment>();
    std::shared_ptr<Attachment> attachment2 = std::make_shared<Attachment>();

    email.appendAttachment(attachment1);
    email.removeAttachment(attachment2);

    QCOMPARE(email.attachments().size(), 1);
    QCOMPARE(email.attachments().first(), attachment1);
}


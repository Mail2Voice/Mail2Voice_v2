#pragma once

#include "../../core/email.hpp"
#include "propsTester.hpp"

#include <QObject>

class TestEmail : public QObject, public PropsTester<m2v::core::Email>
{
    Q_OBJECT

public:
    explicit TestEmail(QObject *parent = nullptr);

private Q_SLOTS:
    void default_constructor();
    void base_test_data();
    void base_test();

    void setSubject();
    void setSenders();
    void setFrom();
    void setRecipients();
    void setCarbonCopy();
    void setBlindCarbonCopy();
    void setDate();
    void setContent();
    void setAttachments();
    void setExtraHeaders();
    void setRead();
    void setFolder();

    void appendAttachment_data();
    void appendAttachment();

    void removeAttachmentByReferenceRight();
    void removeAttachmentByReferenceWrong();
};

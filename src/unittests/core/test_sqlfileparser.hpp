#pragma once

#include <QtTest>

#include "../../core/database/upgrader/sqlfileparser.hpp"

class TestSqlFileParser : public QObject
{
  Q_OBJECT

public:
  TestSqlFileParser();
  ~TestSqlFileParser();

private slots:
  void test_empty_file_data();
  void test_empty_file();
};

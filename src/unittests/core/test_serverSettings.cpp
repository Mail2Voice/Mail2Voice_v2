#include <QTest>
#include "test_serverSettings.hpp"

using namespace m2v::core;

using S = const ServerSettings&;

TestServerSettings::TestServerSettings(QObject *parent) :
    QObject(parent)
{
    m_propsChecklist.append({"type", [](S serverSettings) { QCOMPARE(serverSettings.type(), ServerType::Imap); }});
    m_propsChecklist.append({"address", [](S serverSettings) { QCOMPARE(serverSettings.address(), QString()); }});
    m_propsChecklist.append({"port", [](S serverSettings) { QCOMPARE(serverSettings.port(), (quint16)0); }});
    m_propsChecklist.append({"ssl", [](S serverSettings) { QCOMPARE(serverSettings.ssl(), true); }});
}

void TestServerSettings::default_constructor()
{
    ServerSettings serverSettings;
    checkDefaultProperties(serverSettings, {});
}

void TestServerSettings::base_test_data()
{
    QTest::addColumn<int>("type");
    QTest::addColumn<QString>("address");
    QTest::addColumn<quint16>("port");
    QTest::addColumn<bool>("ssl");

    QTest::newRow("imap") << (int)ServerType::Imap << QString("imap.test.com") << (quint16)42 << true;
    QTest::newRow("smtp") << (int)ServerType::Smtp << QString("smtp.test.com") << (quint16)42 << true;
    QTest::newRow("pop3") << (int)ServerType::Pop3 << QString("pop3.test.com") << (quint16)42 << true;
    QTest::newRow("empty address") << (int) ServerType::Imap << QString() << (quint16)42 << true;
    QTest::newRow("port 0") << (int) ServerType::Imap << QString() << (quint16)0 << true;
    QTest::newRow("port 65535") << (int) ServerType::Imap << QString() << (quint16)65535 << true;
    QTest::newRow("unsecured") << (int)ServerType::Imap << QString("imap.test.com") << (quint16)42 << false;
}

void TestServerSettings::base_test()
{
    QFETCH(int, type);
    QFETCH(QString, address);
    QFETCH(quint16, port);
    QFETCH(bool, ssl);

    ServerSettings serverSettings((ServerType) type, address, port, ssl);
    ServerSettings serverSettingsCopyByConstructor(serverSettings);
    ServerSettings serverSettingsCopyByAssignment = serverSettings;

    QList<ServerSettings*> serverSettingsToTest({&serverSettings,
                                                 &serverSettingsCopyByConstructor,
                                                 &serverSettingsCopyByAssignment});

    for(auto servsets: serverSettingsToTest)
    {
        QCOMPARE(servsets->type(), (ServerType) type);
        QCOMPARE(servsets->address(), address);
        QCOMPARE(servsets->port(), port);
        QCOMPARE(servsets->ssl(), ssl);
    }
}

void TestServerSettings::setType()
{
    ServerSettings serverSettings;
    serverSettings.setType(ServerType::Smtp);
    QCOMPARE(serverSettings.type(), ServerType::Smtp);

    checkDefaultProperties(serverSettings, {"type"});
}

void TestServerSettings::setAddress()
{
    ServerSettings serverSettings;
    serverSettings.setAddress("server.test.com");
    QCOMPARE(serverSettings.address(), QString("server.test.com"));

    checkDefaultProperties(serverSettings, {"address"});
}

void TestServerSettings::setPort()
{
    ServerSettings serverSettings;
    serverSettings.setPort(42);
    QCOMPARE(serverSettings.port(), quint16(42));

    checkDefaultProperties(serverSettings, {"port"});
}

void TestServerSettings::setSsl()
{
    ServerSettings serverSettings;
    serverSettings.setSsl(true);
    QCOMPARE(serverSettings.ssl(), true);

    serverSettings.setSsl(false);
    QCOMPARE(serverSettings.ssl(), false);

    checkDefaultProperties(serverSettings, {"ssl"});
}

#include <QTest>

#include <QSqlQuery>

#include "core/test_account.hpp"
#include "core/test_accountDatabaseConnector.hpp"
#include "core/test_attachment.hpp"
#include "core/test_contact.hpp"
#include "core/test_contactDatabaseConnector.hpp"
#include "core/test_email.hpp"
#include "core/test_emailDatabaseConnector.hpp"
#include "core/test_serverSettings.hpp"
#include "core/test_sqlfileparser.hpp"

bool executeTests(int argc, char *argv[], QList<QObject*> tests)
{
    bool ret {true};

    for(auto test: tests)
    {
        ret &= (QTest::qExec(test, argc, argv) == 0);
    }

    return ret;
}

int main( int argc, char *argv[])
{
    QSqlDatabase db;
    QString dbConnectionName;

    dbConnectionName = "mail2voice1";
    db = QSqlDatabase::addDatabase("QSQLITE", dbConnectionName);
    db.setDatabaseName(":memory:");
    db.open();
    db.exec ("DROP TABLE account");
    db.exec ("DROP TABLE contact");
    db.exec ("DROP TABLE email");

    db.exec("CREATE TABLE account "
                "(account_id INTEGER PRIMARY KEY, "
                "username TEXT,"
                "password TEXT,"
                "displayName TEXT,"
                "email TEXT)");

    db.exec("CREATE TABLE contact "
               "(contact_id INTEGER PRIMARY KEY, "
               "name TEXT,"
               "email TEXT,"
               "portrait TEXT,"
               "groups TEXT)");

    db.exec("CREATE TABLE email "
               "(email_id INTEGER PRIMARY KEY, "
               "account_id INTEGER,"
               "message_id TEXT,"
               "subject TEXT, "
               "sender TEXT, "
               "recipients TEXT,"
               "carbonCopy TEXT,"
               "blindCarbonCopy TEXT,"
               "date INTEGER,"
               "content TEXT,"
               "attachments TEXT,"
               "extraHeaders TEXT,"
               "read INTEGER,"
               "folder TEXT,"
               "FOREIGN KEY(account_id) REFERENCES account(id))");

    TestContact tstContact;
    TestEmail tstEmail;
    TestAccount tstAccount;
    TestAttachment tstAttachment;
    TestServerSettings tstServerSettings;
    TestAccountDatabaseConnector tstAccountDbConnector(db, dbConnectionName);
    TestContactDatabaseConnector tstContactDbConnector(db, dbConnectionName);
    TestEmailDatabaseConnector tstEmailDbConnector(db, dbConnectionName);
    TestSqlFileParser tstSqlFileParser;

    QList<QObject*> tests;
    tests << &tstContact
          << &tstEmail
          << &tstAccount
          << &tstAttachment
          << &tstServerSettings
          << &tstAccountDbConnector
          << &tstContactDbConnector
          << &tstEmailDbConnector
          << &tstSqlFileParser;

    bool success = executeTests(argc, argv, tests);

    db.close();
    dbConnectionName.clear();
    QSqlDatabase::removeDatabase(dbConnectionName);

    if (success)
    {
        qInfo() << "All tests passed successfully !";
        return EXIT_SUCCESS;
    }
    else
    {
        qInfo() << "Some tests failed.";
        return EXIT_FAILURE;
    }
}

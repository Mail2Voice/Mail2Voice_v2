#include <QTest>
#include <QApplication>

#include "gui/test_pixmapContainer.hpp"

bool executeTests(int argc, char *argv[], QList<QObject*> tests)
{
    bool ret = true;

    for(auto test: tests)
    {
        ret &= QTest::qExec(test, argc, argv);
    }

    return ret;
}

int main( int argc, char *argv[])
{
    QApplication app(argc, argv);

    int ret = 0;

    TestPixmapContainer tstPixmapContainer;

    QList<QObject*> tests;
    tests << &tstPixmapContainer;

    ret = (int) executeTests(argc, argv, tests);

    return ret;
}

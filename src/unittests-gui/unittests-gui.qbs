import qbs

CppApplication
{
    files:
    [
        "../gui/common/containers/baseContainer.cpp",
        "../gui/common/containers/baseContainer.hpp",
        "../gui/common/containers/resourceContainer.hpp",
        "../gui/common/containers/pixmapContainer.cpp",
        "../gui/common/containers/pixmapContainer.hpp",
        "gui/test_pixmapContainer.cpp",
        "gui/test_pixmapContainer.hpp",
        "main.cpp",
        "main.qrc",
    ]

    cpp.cxxLanguageVersion: "c++14"

    Depends
    {
        name: "Qt";
        submodules:
        [
            "core",
            "gui",
            "widgets",
            "test"
        ]
    }

    Depends
    {
        name: "mail2voice-core"
    }

    Group
    {
        fileTagsFilter: product.type
        qbs.install: true
    }
}

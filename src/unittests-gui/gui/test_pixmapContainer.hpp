#pragma once

#include <QObject>
#include <QPixmap>

class TestPixmapContainer : public QObject
{
    Q_OBJECT

public:
    explicit TestPixmapContainer(QObject *parent = nullptr);

private slots:
    void base_test();

private:
    QPixmap m_testPixmap1{":/resources/icons/test1.png"};
    QPixmap m_testPixmap2{":/resources/icons/test2.png"};
};

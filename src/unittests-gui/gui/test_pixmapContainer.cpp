#include <QTest>
#include <QPixmap>

#include "test_pixmapContainer.hpp"
#include "../../gui/common/containers/pixmapContainer.hpp"

using namespace m2v::gui;

TestPixmapContainer::TestPixmapContainer(QObject *parent) :
    QObject(parent)
{
    QVERIFY(!m_testPixmap1.isNull());
    QVERIFY(!m_testPixmap2.isNull());
}

void TestPixmapContainer::base_test()
{
    PixmapContainer pixmapContainer{"testResource", m_testPixmap1};

    // Test retrieving an invalid resource id to verify if we get the default pixmap
    QCOMPARE(pixmapContainer.retrieve(424242).toImage(), m_testPixmap1.toImage());
    QVERIFY(!pixmapContainer.hasResource(424242));

    // Test storing a pixmap
    auto resourceId = pixmapContainer.store(m_testPixmap2);
    QVERIFY(resourceId != BaseContainer::InvalidResourceId);

    // Test that the resource is contained
    QVERIFY(pixmapContainer.hasResource(resourceId));

    // Test retrieving the pixmap
    QCOMPARE(pixmapContainer.retrieve(resourceId).toImage(), m_testPixmap2.toImage());
}


#pragma once

#include <memory>
#include <QDateTime>
#include <QString>
#include <QStringList>
#include <QMetaType>
#include <QVariant>

#include "attachment.hpp"

namespace m2v
{
namespace core
{
using AttachmentList = QList<std::shared_ptr<Attachment>>;

class Email final
{
public:
    Email() = default;

    Email(const Email& other) :
        m_id{other.m_id},
        m_accountId{other.m_accountId},
        m_messageId{other.m_messageId},
        m_subject{other.m_subject},
        m_senders{other.m_senders},
        m_from{other.m_from},
        m_recipients{other.m_recipients},
        m_carbonCopy{other.m_carbonCopy},
        m_blindCarbonCopy{other.m_blindCarbonCopy},
        m_date{other.m_date},
        m_content{other.m_content},
        m_attachments{other.m_attachments},
        m_extraHeaders{other.m_extraHeaders},
        m_read{other.m_read},
        m_folder{other.m_folder},
        m_bodystructure{other.m_bodystructure}
    {
    }

    ~Email() = default;

    Email &operator=(Email other)
    {
        std::swap(m_id, other.m_id);
        std::swap(m_accountId, other.m_accountId);
        std::swap(m_messageId, other. m_messageId);
        std::swap(m_subject, other.m_subject);
        std::swap(m_senders, other.m_senders);
        std::swap(m_from, other.m_from);
        std::swap(m_recipients, other.m_recipients);
        std::swap(m_carbonCopy, other.m_carbonCopy);
        std::swap(m_blindCarbonCopy, other.m_blindCarbonCopy);
        std::swap(m_date, other.m_date);
        std::swap(m_content, other.m_content);
        std::swap(m_attachments, other.m_attachments);
        std::swap(m_extraHeaders, other.m_extraHeaders);
        std::swap(m_read, other.m_read);
        std::swap(m_folder, other.m_folder);
        std::swap(m_bodystructure, other.m_bodystructure);

        return *this;
    }

    void appendAttachment(const std::shared_ptr<Attachment>& attachment);
    void removeAttachment(const std::shared_ptr<Attachment>& attachment);

    qint64 id()                             const { return m_id; }
    qint64 accountId()                      const { return m_accountId; }
    const QString& messageId()              const { return m_messageId; }
    const QString& subject()                const { return m_subject; }
    const QStringList& senders()            const { return m_senders; }
    const QStringList& from()               const { return m_from; }
    const QStringList& recipients()         const { return m_recipients; }
    const QStringList& carbonCopy()         const { return m_carbonCopy; }
    const QStringList& blindCarbonCopy()    const { return m_blindCarbonCopy; }
    const QDateTime& date()                 const { return m_date; }
    const QString& content()                const { return m_content; }
    const AttachmentList& attachments()     const { return m_attachments; }
    const QStringList& extraHeaders()       const { return m_extraHeaders; }
    bool read()                             const { return m_read; }
    const QString& folder()                 const { return m_folder; }
    const QVariantMap &bodystructure()      const { return m_bodystructure; }

    void setId(const qint64 id)                             { m_id = id; }
    void setAccountId(const qint64 id)                      { m_accountId = id; }
    void setMessageId(const QString& messageId)             { m_messageId = messageId; }
    void setSubject(const QString& subject)                 { m_subject = subject; }
    void setSenders(const QStringList& senders)             { m_senders = senders; }
    void setFrom(const QStringList& from)                   { m_from = from; }
    void setRecipients(const QStringList& recipients)       { m_recipients = recipients; }
    void setCarbonCopy(const QStringList& carbonCopy)       { m_carbonCopy = carbonCopy; }
    void setBlindCopy(const QStringList& blindCopy)         { m_blindCarbonCopy = blindCopy; }
    void setDate(const QDateTime& date)                     { m_date = date; }
    void setContent(const QString& content)                 { m_content = content; }
    void setAttachments(const AttachmentList& attachments)  { m_attachments = attachments; }
    void setExtraHeader(const QStringList& extraHeaders)    { m_extraHeaders = extraHeaders; }
    void setRead(bool read)                                 { m_read = read; }
    void setFolder(const QString& folder)                   { m_folder = folder; }
    void setBodystructure(const QVariantMap &bodystructure) { m_bodystructure = bodystructure; }

private:
    qint64  m_id{-1};
    qint64  m_accountId{-1};
    QString m_messageId;
    QString m_subject;
    QStringList m_senders;
    QStringList m_from;
    QStringList m_recipients;
    QStringList m_carbonCopy;
    QStringList m_blindCarbonCopy;
    QDateTime m_date;
    QString m_content;
    AttachmentList m_attachments;
    QStringList m_extraHeaders;
    bool m_read{false};
    QString m_folder;
    QVariantMap m_bodystructure;
};
}
}

Q_DECLARE_METATYPE(m2v::core::Email)
Q_DECLARE_METATYPE(std::shared_ptr<m2v::core::Email>)

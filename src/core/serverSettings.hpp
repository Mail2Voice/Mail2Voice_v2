#pragma once

#include <QString>

namespace m2v
{
namespace core
{
enum class ServerType
{
    Imap,
    Pop3,
    Smtp
};

class ServerSettings final
{
public:
    ServerSettings() = default;

    ServerSettings(ServerType type, const QString& address, quint16 port, bool ssl):
        m_type{type},
        m_address{address},
        m_port{port},
        m_ssl{ssl}
    {
    }

    ServerSettings(const ServerSettings& other):
        m_type{other.m_type},
        m_address{other.m_address},
        m_port{other.m_port},
        m_ssl{other.m_ssl}
    {
    }

    ServerSettings &operator=(ServerSettings other)
    {
        std::swap(m_type, other.m_type);
        std::swap(m_address, other.m_address);
        std::swap(m_port, other.m_port);
        std::swap(m_ssl, other.m_ssl);

        return *this;
    }

    ServerType type()           const { return m_type; }
    const QString& address()    const { return m_address; }
    quint16 port()              const { return m_port; }
    bool ssl()                  const { return m_ssl; }

    void setType(ServerType type)           { m_type = type; }
    void setAddress(const QString& address) { m_address = address; }
    void setPort(quint16 port)              { m_port = port; }
    void setSsl(bool ssl)                   { m_ssl = ssl; }

public:
    static constexpr quint16 defaultImapPort {993};
    static constexpr quint16 defaultSmtpPort {465};

private:
    ServerType m_type{ServerType::Imap};
    QString m_address;
    quint16 m_port{0};
    bool m_ssl{true};
};
}
}

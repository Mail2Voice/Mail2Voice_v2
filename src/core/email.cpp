#include "email.hpp"

namespace m2v
{
namespace core
{
void Email::appendAttachment(const std::shared_ptr<Attachment>& attachment)
{
    m_attachments.append(attachment);
}

void Email::removeAttachment(const std::shared_ptr<Attachment>& attachment)
{
    int attachmentIndex = m_attachments.indexOf(attachment);

    if(attachmentIndex != -1)
    {
        m_attachments.removeAt(attachmentIndex);
    }
}
}
}

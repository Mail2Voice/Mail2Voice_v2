#pragma once

#include <memory>
#include <QByteArray>
#include <QString>
#include <QMetaType>

namespace m2v
{
namespace core
{
class Attachment final
{
public:
    Attachment() = default;

    Attachment(const QString& name, const QString& mimetype, const QByteArray& content) :
        m_name{name},
        m_mimetype{mimetype},
        m_content{content}
    {
    }

    Attachment(const Attachment& other) :
        m_name{other.m_name},
        m_mimetype{other.m_mimetype},
        m_content{other.m_content}
    {
    }

    Attachment &operator=(Attachment other)
    {
        std::swap(m_name, other.m_name);
        std::swap(m_mimetype, other.m_mimetype);
        std::swap(m_content, other.m_content);

        return *this;
    }

    qint64 id()                 const { return m_id; }
    const QString& name()       const { return m_name; }
    const QString& mimetype()   const { return m_mimetype; }
    const QByteArray& content() const { return m_content; }

    void setId(const qint64 id)                 { m_id = id;}
    void setName(const QString& name)           { m_name = name; }
    void setMimetype(const QString& mimetype)   { m_mimetype = mimetype; }
    void setContent(const QByteArray& content)  { m_content = content; }

private:
    qint64 m_id{-1};
    QString m_name;
    QString m_mimetype;
    QByteArray m_content;
};
}
}

Q_DECLARE_METATYPE(m2v::core::Attachment)
Q_DECLARE_METATYPE(std::shared_ptr<m2v::core::Attachment>)

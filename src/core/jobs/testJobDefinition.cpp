#include "testJobDefinition.hpp"

namespace m2v
{
namespace core
{
TestJobDefinition::TestJobDefinition(QObject *parent):
    AbstractJobDefinition(parent)
{
}

TestJobDefinition::~TestJobDefinition() = default;

QString TestJobDefinition::name() const
{
    return QStringLiteral("Test");
}
}
}

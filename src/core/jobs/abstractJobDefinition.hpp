#pragma once

#include <QObject>

namespace m2v
{
namespace core
{
class AbstractJobDefinition : public QObject
{
    Q_OBJECT

public:
    AbstractJobDefinition(QObject *parent = nullptr):
        QObject(parent)
    {
    }
    ~AbstractJobDefinition() override = default;

    virtual QString name() const = 0;
};
}
}

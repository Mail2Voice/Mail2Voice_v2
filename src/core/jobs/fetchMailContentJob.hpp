#pragma once

#include "abstractJob.hpp"

#include <QTimer>

namespace m2v
{
namespace core
{
class EmailClientWrapper;

class FetchMailContentJob : public AbstractJob
{
    Q_OBJECT

public:
    FetchMailContentJob(const QString &mailFolder, qint64 id, EmailClientWrapper &emailClientWrapper, QObject *parent = nullptr);
    ~FetchMailContentJob() override = default;

    void run() override;
    void stop() override;

signals:
    void jobFinished(const QString &content);

private:
    void onFetchContentSucceeded(qint64 emailId, const QString &mailbox, const QString &content);
    void onFetchContentFailed(qint64 emailId, const QString &mailbox);
    void onTimeout();

private:
    QString m_mailFolder;
    qint64 m_id;
    EmailClientWrapper &m_emailClientWrapper;
    QTimer m_timeout;
    static constexpr int timeoutDelay {3000};
};
}
}

#pragma once
#include <QObject>

#include <memory>
#include <queue>

namespace m2v
{
namespace core
{

class AbstractJob;

class JobRunner : public QObject
{
    Q_OBJECT

public:
    static void init(QObject *parent)
    {
        jobRunnerInstance = new JobRunner(parent);
    }

    static JobRunner* getInstance()
    {
        return jobRunnerInstance;
    }

    void addJob(std::unique_ptr<AbstractJob> &&job);

    ~JobRunner();


private:
    explicit JobRunner(QObject *parent = nullptr);

    void runNextJob();

    void stop();

private:
    static JobRunner *jobRunnerInstance;

    std::queue<std::unique_ptr<AbstractJob>> jobQueue;

    bool isRunning {false};

private slots:
    void onJobError();

    void onJobFinished();
};

}  // namespace core
}  // namespace m2v

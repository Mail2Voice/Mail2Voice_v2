#include "jobRunner.hpp"

#include "abstractJob.hpp"
#include <QDebug>

namespace m2v
{
namespace core
{

JobRunner* JobRunner::jobRunnerInstance = nullptr;

JobRunner::JobRunner(QObject *parent) : QObject(parent)
{

}

JobRunner::~JobRunner()
{

}

void JobRunner::addJob(std::unique_ptr<AbstractJob> &&job)
{
    jobQueue.push(std::move(job));

    if(!isRunning) {
        runNextJob();
    }
}

void JobRunner::runNextJob()
{
    qDebug() << __FUNCTION__;
    if(jobQueue.empty())
    {
        qDebug() << "No more job to do.";
        stop();
        return;
    }

    isRunning = true;

    std::unique_ptr<AbstractJob> &nextJob = jobQueue.front();
    connect(nextJob.get(), &AbstractJob::error, this, &JobRunner::onJobError);
    connect(nextJob.get(), &AbstractJob::finished, this, &JobRunner::onJobFinished);

    nextJob->run();
}

void JobRunner::stop()
{
    isRunning = false;
}

void JobRunner::onJobError()
{
    qDebug() << "Job failed";
    jobQueue.pop();
    runNextJob();
}

void JobRunner::onJobFinished()
{
    std::unique_ptr<AbstractJob> &nextJob = jobQueue.front();
    disconnect(nextJob.get(), &AbstractJob::error, this, &JobRunner::onJobError);
    disconnect(nextJob.get(), &AbstractJob::finished, this, &JobRunner::onJobFinished);

    jobQueue.pop();
    runNextJob();
}

}  // namespace core
}  // namespace m2v

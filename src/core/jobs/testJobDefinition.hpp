#pragma once

#include "abstractJobDefinition.hpp"

namespace m2v
{
namespace core
{
class TestJobDefinition : public AbstractJobDefinition
{
    Q_OBJECT

public:
    TestJobDefinition(QObject *parent = nullptr);
    ~TestJobDefinition() override;

    QString name() const override;
};
}
}

#include "testJob.hpp"

#include <QDebug>
#include <QTimer>

namespace m2v
{
namespace core
{
TestJob::TestJob(const QString &param, QObject *parent):
    AbstractJob(parent),
    param(param)
{
    qDebug() << "Constructed with" << param;
}

void TestJob::run()
{
    qDebug() << "Run" << param;

    QTimer::singleShot(5000, [this]
    {
        qDebug() << "Finished" << param;

        emit finished();
    });
}

void TestJob::stop()
{
    qDebug() << "Stop";
}
}
}

#pragma once

#include "abstractJob.hpp"

namespace m2v
{
namespace core
{
class TestJob : public AbstractJob
{
    Q_OBJECT

public:
    TestJob(const QString &param, QObject *parent = nullptr);
    ~TestJob() override = default;

    void run() override;
    void stop() override;

private:
    QString param;
};
}
}

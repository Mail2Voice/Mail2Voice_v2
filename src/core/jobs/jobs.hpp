#pragma once

#include <unordered_map>
#include <memory>
#include <typeindex>

class AbstractJobDefinition;

namespace m2v
{
namespace core
{
extern std::unordered_map<std::type_index, std::unique_ptr<AbstractJobDefinition>> jobDefinitions;

void initialize();
}
}

#include "fetchMailContentJob.hpp"
#include "../emails/emailClientWrapper.hpp"

#include <QDebug>
#include <QString>

namespace m2v
{
namespace core
{
FetchMailContentJob::FetchMailContentJob(const QString &mailFolder, qint64 id, EmailClientWrapper &emailClientWrapper, QObject *parent):
    AbstractJob(parent),
    m_mailFolder(mailFolder),
    m_id(id),
    m_emailClientWrapper(emailClientWrapper)
{
    m_timeout.setSingleShot(true);
    m_timeout.setInterval(timeoutDelay);
    connect(&m_timeout, &QTimer::timeout, this, &FetchMailContentJob::onTimeout);
}

void FetchMailContentJob::run()
{
    connect(&m_emailClientWrapper, &EmailClientWrapper::contentReceived, this, &FetchMailContentJob::onFetchContentSucceeded);
    connect(&m_emailClientWrapper, &EmailClientWrapper::fetchContentFailed, this, &FetchMailContentJob::onFetchContentFailed);
    m_emailClientWrapper.fetchContent(m_mailFolder, m_id);
    m_timeout.start();
}

void FetchMailContentJob::stop()
{
    m_timeout.stop();
    disconnect(&m_emailClientWrapper, &EmailClientWrapper::contentReceived, this, &FetchMailContentJob::onFetchContentSucceeded);
    disconnect(&m_emailClientWrapper, &EmailClientWrapper::fetchContentFailed, this, &FetchMailContentJob::onFetchContentFailed);
}

void FetchMailContentJob::onFetchContentSucceeded(qint64 emailId, const QString& mailbox, const QString &content)
{
    if(emailId != m_id)
    {
        qDebug() << __FUNCTION__ << "Received unexpected email (expected"  << m_id << "but received" << emailId << ").";
        return;
    }

    stop();
    emit jobFinished(content);
    emit finished();
}

void FetchMailContentJob::onFetchContentFailed(qint64 emailId, const QString &mailbox)
{
    if(emailId != m_id || m_mailFolder != mailbox)
    {
        qDebug() << __FUNCTION__ << "Received unexpected email (expected"  << m_id << "but received" << emailId << ").";
        return;
    }

    qDebug() << __FUNCTION__ << "Failed to fetch email content (" << m_id << ", " << m_mailFolder << ").";

    stop();

    Error commandFailure(ErrorType::ImapCommandFailed,
                         QString("Failed to fetch content for email %1, %2").arg(m_id).arg(m_mailFolder));
    emit error(commandFailure);
}


void FetchMailContentJob::onTimeout()
{
    stop();

    Error TimeoutError(ErrorType::Timeout,
                       QString("Failed to fetch email content (%1, %2)").arg(m_id).arg(m_mailFolder));
    emit error(TimeoutError);
}

}
}

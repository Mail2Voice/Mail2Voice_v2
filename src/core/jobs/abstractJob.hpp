#pragma once

#include <QObject>
#include "../error/error.hpp"

namespace m2v
{
namespace core
{
class AbstractJob : public QObject
{
    Q_OBJECT

public:
    AbstractJob(QObject *parent = nullptr):
        QObject(parent)
    {
    }
    ~AbstractJob() override = default;

    virtual void run() = 0;
    virtual void stop() {}

signals:
    void error(const Error error);
    void finished();
};

}
}

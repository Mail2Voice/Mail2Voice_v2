#pragma once

namespace m2v
{
namespace core
{

enum class ErrorType {
  Timeout,
  ImapCommandFailed
};

}  // namespace core
}  // namespace m2v

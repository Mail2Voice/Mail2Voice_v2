#pragma once

#include <QString>

#include "errorType.hpp"

namespace m2v
{
namespace core
{

class Error
{
public:
    Error(const ErrorType type, const QString& reason) :
        m_type {type},
        m_reason {reason} {}

    ErrorType type() const { return m_type; }
    QString reason() const { return m_reason; }

private:
    ErrorType m_type;
    QString m_reason;
};

}  // namespace core
}  // namespace m2v

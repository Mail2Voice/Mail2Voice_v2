#pragma once

#include "../account.hpp"
#include "../database/accountDatabaseConnector.hpp"


namespace m2v {

namespace core {

class Accounts final
{
public:
    Accounts() = delete;
    ~Accounts() = delete;

    static bool loadDefaultAccount();

    static const Account& currentAccount() { return m_currentAccount; }
    static bool saveCurrentAccount(const Account& account);

private:
    static Account m_currentAccount;
    static AccountDatabaseConnector m_accountDatabaseConnector;
};

}  // namespace core

}  // namespace m2v

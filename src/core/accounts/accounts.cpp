#include "accounts.hpp"

namespace m2v {

namespace core {

Account Accounts::m_currentAccount{};
AccountDatabaseConnector Accounts::m_accountDatabaseConnector{};

bool Accounts::loadDefaultAccount()
{
    m_accountDatabaseConnector.setupDatabase("mail2voice");

    QList<Account> accounts = Accounts::m_accountDatabaseConnector.listAccounts();

    if(!accounts.isEmpty())
    {
        Accounts::m_currentAccount = accounts.first();
        return true;
    }

    return false;
}

bool Accounts::saveCurrentAccount(const Account &account)
{
    qint64 currentAccountId = Accounts::m_currentAccount.id();
    Accounts::m_currentAccount = account;
    Accounts::m_currentAccount.setId(currentAccountId);

    return m_accountDatabaseConnector.saveAccount(Accounts::m_currentAccount);
}

}  // namespace core
}  // namespace m2v

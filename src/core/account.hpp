#pragma once

#include <QString>

#include "serverSettings.hpp"

namespace m2v
{
namespace core
{
class Account final
{
public:
    Account() = default;

    Account(const QString& username, const QString& password, const QString& displayName,
            const QString& email, const ServerSettings& storeSettings, const ServerSettings& transportSettings) :
        m_username{username},
        m_password{password},
        m_displayName{displayName},
        m_email{email},
        m_storeSettings{storeSettings},
        m_transportSettings{transportSettings}
    {
    }

    Account(const Account& other) :
        m_id{other.m_id},
        m_username{other.m_username},
        m_password{other.m_password},
        m_displayName{other.m_displayName},
        m_email{other.m_email},
        m_storeSettings{other.m_storeSettings},
        m_transportSettings{other.m_transportSettings}
    {
    }

    Account& operator=(Account other)
    {
        std::swap(m_id, other.m_id);
        std::swap(m_username, other.m_username);
        std::swap(m_password, other.m_password);
        std::swap(m_displayName, other.m_displayName);
        std::swap(m_email, other.m_email);
        std::swap(m_storeSettings, other.m_storeSettings);
        std::swap(m_transportSettings, other.m_transportSettings);

        return *this;
    }

    bool operator==(const Account& other)
    {
        return m_username == other.m_username &&
               m_displayName == other.m_displayName &&
               m_password == other.m_password &&
               m_displayName == other.m_displayName &&
               m_email == other.m_email &&
               m_storeSettings.type() == other.m_storeSettings.type() &&
               m_storeSettings.address() == other.m_storeSettings.address() &&
               m_storeSettings.port() == other.m_storeSettings.port() &&
               m_storeSettings.ssl() == other.m_storeSettings.ssl() &&
               m_transportSettings.address() == other.m_transportSettings.address() &&
               m_transportSettings.port() == other.m_transportSettings.port() &&
               m_transportSettings.ssl() == other.m_transportSettings.ssl();
    }

    bool operator!=(const Account& other)
    {
        return !(*this == other);
    }

    bool isValid() const
    {
        return m_id != -1 &&
               !m_username.isEmpty() &&
               !m_password.isEmpty() &&
               !m_storeSettings.address().isEmpty() &&
               m_storeSettings.port() != 0;
    }

    qint64 id()                    const { return m_id; }
    const QString& username()      const { return m_username; }
    const QString& password()      const { return m_password; }
    const QString& displayName()   const { return m_displayName; }
    const QString& email()         const { return m_email; }
    const ServerSettings& storeSettings()   const { return m_storeSettings; }
    const ServerSettings& transportSettings()   const { return m_transportSettings; }

    void setId(const qint64 id)                     { m_id = id;}
    void setUsername(const QString& username)       { m_username = username; }
    void setPassword(const QString& password)       { m_password = password; }
    void setDisplayName(const QString& displayName) { m_displayName = displayName; }
    void setEmail(const QString& email)             { m_email = email; }
    void setStoreSettings(const ServerSettings& storeSettings)          { m_storeSettings = storeSettings; }
    void setTransportSettings(const ServerSettings& transportSettings)  { m_transportSettings = transportSettings; }

private:
    qint64  m_id{-1};
    QString m_username;
    QString m_password;
    QString m_displayName;
    QString m_email;
    ServerSettings m_storeSettings;
    ServerSettings m_transportSettings;
};
}
}

#include "contactDatabaseConnector.hpp"

#include <QDebug>
#include <QRegularExpression>
#include <QSqlError>
#include <QSqlQuery>

/**
 * @brief ContactDatabaseConnector::ContactDatabaseConnector
 * @param dbConnectionName
 */
ContactDatabaseConnector::ContactDatabaseConnector(const QString& dbConnectionName)
       : m_database(QSqlDatabase::database(dbConnectionName))
{
}

/**
 * @brief ContactDatabaseConnector::~ContactDatabaseConnector
 */
ContactDatabaseConnector::~ContactDatabaseConnector()
{
}

/**
 * @brief ContactDatabaseConnector::addContact
 * @param contact
 */
void ContactDatabaseConnector::addContact(m2v::core::Contact& contact)
{
    if(contact.email().isEmpty())
    {
        throw std::runtime_error("Can't add contact in database, email is empty.");
    }

    QSqlQuery queryAdd(m_database);

    //add in db
    queryAdd.prepare("INSERT INTO contact (name, email, portrait, groups)"
                     "VALUES (:name, :email, :portrait, :groups)");

    queryAdd.bindValue(":name", contact.name());
    queryAdd.bindValue(":email", contact.email());
    queryAdd.bindValue(":portrait", contact.portrait());

    QString contactlist = contact.groups().join(",");
    queryAdd.bindValue(":groups", QString("{%1}").arg(contactlist));

    if(!queryAdd.exec())
    {
        throw std::runtime_error("Failed to add contact in database");
    }

    contact.setId(queryAdd.lastInsertId().toInt());
}

/**
 * @brief ContactDatabaseConnector::deleteContact
 * @param contact
 */
void ContactDatabaseConnector::deleteContact(const m2v::core::Contact& contact)
{
    if(!contactExist(contact))
    {
        throw std::runtime_error("Can't delete contact, doesn't exist in database.");
    }

    QSqlQuery queryDelete(m_database);
    queryDelete.prepare("DELETE FROM contact WHERE contact_id = :id");
    queryDelete.bindValue(":id", contact.id());

    if(!queryDelete.exec())
    {
        throw std::runtime_error("Failed to delete contact in database.");
    }
}

/**
 * @brief ContactDatabaseConnector::getContactList
 * @return the list of contacts
 */
QList<m2v::core::Contact> ContactDatabaseConnector::getContactList() const
{
    QList<m2v::core::Contact> contacts;

    QSqlQuery query(m_database);
    query.exec("SELECT contact_id, name, email, portrait, groups FROM contact");

    while(query.next())
    {
        m2v::core::Contact contact;

        contact.setId(query.value("contact_id").toLongLong());
        contact.setName(query.value("name").toString());
        contact.setEmail(query.value("email").toString());
        contact.setPortrait(query.value("portrait").toString());

        QRegularExpression pattern("[{,}]");
        QString serializedGroups = query.value("groups").toString();
        QStringList groups = serializedGroups.split(pattern, QString::SkipEmptyParts);
        contact.setGroups(groups);

        contacts.push_back(contact);
    }

    return contacts;
}

/************LOCAL FUNCTIONS: *************/
/**
 * @brief contactExist
 * @param inEntry
 * @return true if the contact exists in database
 */
bool ContactDatabaseConnector::contactExist(const m2v::core::Contact& contact) const
{
    QSqlQuery checkQuery(m_database);
    checkQuery.prepare("SELECT contact_id FROM contact WHERE contact_id = :id");
    checkQuery.bindValue(":id", contact.id());

    return checkQuery.exec() && checkQuery.first();
}

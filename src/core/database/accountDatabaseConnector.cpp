#include "accountDatabaseConnector.hpp"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

/**
 * @brief AccountDatabaseConnector::AccountDatabaseConnector
 * @param dbConnectionName
 */
AccountDatabaseConnector::AccountDatabaseConnector(const QString& dbConnectionName)
   : m_database(QSqlDatabase::database(dbConnectionName))
{
}

/**
 * @brief AccountDatabaseConnector::~AccountDatabaseConnector
 */
AccountDatabaseConnector::~AccountDatabaseConnector()
{
}

void AccountDatabaseConnector::setupDatabase(const QString &dbConnectionName)
{
    m_database = QSqlDatabase::database(dbConnectionName);
}

/**
 * @brief AccountDatabaseConnector::save
 * @param account
 */
bool AccountDatabaseConnector::saveAccount(m2v::core::Account &account)
{
    bool mustSetId {false};

    QSqlQuery querySave(m_database);

    if(isAccountStored(account))
    {
        querySave.prepare(QStringLiteral("UPDATE account SET "
                          "username=:username, "
                          "password=:password, "
                          "displayName=:displayName, "
                          "email=:email, "
                          "incomingProtocol=:incomingProtocol, "
                          "incomingAddress=:incomingAddress, "
                          "incomingPort=:incomingPort, "
                          "incomingSsl=:incomingSsl, "
                          "outgoingAddress=:outgoingAddress, "
                          "outgoingPort=:outgoingPort, "
                          "outgoingSsl=:outgoingSsl "
                          "WHERE account_id=:id"));

        querySave.bindValue(":id", account.id());
    }
    else
    {
        querySave.prepare(QStringLiteral("INSERT INTO account (username, password, displayName, email, "
                          "incomingProtocol, incomingAddress, incomingPort, incomingSsl, "
                          "outgoingAddress, outgoingPort, outgoingSsl) "
                          "VALUES (:username, :password, :displayName, :email, "
                          ":incomingProtocol, :incomingAddress, :incomingPort, :incomingSsl, "
                          ":outgoingAddress, :outgoingPort, :outgoingSsl)"));

        mustSetId = true;
    }

    querySave.bindValue(QStringLiteral(":username"), account.username());
    querySave.bindValue(QStringLiteral(":password"), account.password());
    querySave.bindValue(QStringLiteral(":displayName"), account.displayName());
    querySave.bindValue(QStringLiteral(":email"), account.email());
    querySave.bindValue(QStringLiteral(":incomingProtocol"), typeToString(account.storeSettings().type()));
    querySave.bindValue(QStringLiteral(":incomingAddress"), account.storeSettings().address());
    querySave.bindValue(QStringLiteral(":incomingPort"), account.storeSettings().port());
    querySave.bindValue(QStringLiteral(":incomingSsl"), account.storeSettings().ssl());
    querySave.bindValue(QStringLiteral(":outgoingAddress"), account.transportSettings().address());
    querySave.bindValue(QStringLiteral(":outgoingPort"), account.transportSettings().port());
    querySave.bindValue(QStringLiteral(":outgoingSsl"), account.transportSettings().ssl());

    if(!querySave.exec())
    {
        qCritical() << "Failed to save account in database: " << querySave.lastError().text() << querySave.executedQuery();
        return false;
    }

    if (mustSetId)
    {
        account.setId(querySave.lastInsertId().toInt());
    }

    return true;
}

/**
 * @brief AccountDatabaseConnector::remove
 * @param account
 */
void AccountDatabaseConnector::removeAccount(const m2v::core::Account& account)
{
   if(!isAccountStored(account))
   {
       throw std::runtime_error("Can't delete account, doesn't exist in database.");
   }

   QSqlQuery queryDelete(m_database);
   queryDelete.prepare("DELETE FROM account WHERE account_id = :id");
   queryDelete.bindValue(":id", account.id());

   if(!queryDelete.exec())
   {
       throw std::runtime_error("Failed to delete account.");
   }
}

/**
 * @brief AccountDatabaseConnector::list
 * @return
 */
QList<m2v::core::Account> AccountDatabaseConnector::listAccounts() const
{
    QList<m2v::core::Account> accounts;

    QSqlQuery query(m_database);
    query.exec("SELECT * FROM account");

    while(query.next())
    {
        m2v::core::Account account;

        account.setId(query.value("account_id").toLongLong());
        account.setUsername(query.value("username").toString());
        account.setPassword(query.value("password").toString());
        account.setDisplayName(query.value("displayName").toString());
        account.setEmail(query.value("email").toString());

        {
            m2v::core::ServerSettings storeSettings;
            storeSettings.setAddress(query.value("incomingAddress").toString());
            storeSettings.setType(stringToType(query.value("incomingProtocol").toString()));
            storeSettings.setPort(query.value("incomingPort").toInt());
            storeSettings.setSsl(query.value("incomingSsl").toBool());
            account.setStoreSettings(storeSettings);
        }
        {
            m2v::core::ServerSettings transportSettings;
            transportSettings.setType(m2v::core::ServerType::Smtp);
            transportSettings.setAddress(query.value("outgoingAddress").toString());
            transportSettings.setPort(query.value("outgoingPort").toInt());
            transportSettings.setSsl(query.value("outgoingSsl").toBool());
            account.setTransportSettings(transportSettings);
        }

        accounts.push_back(account);
    }

    return accounts;
}

/************LOCAL FUNCTIONS: *************/
/**
 * @brief isExist
 * @return true if the account exists in database
 */
bool AccountDatabaseConnector::isAccountStored(const m2v::core::Account& account) const
{
    QSqlQuery checkQuery(m_database);
    checkQuery.prepare("SELECT account_id FROM account WHERE account_id = :id");
    checkQuery.bindValue(":id", account.id());

    return checkQuery.exec() && checkQuery.next();
}

QString AccountDatabaseConnector::typeToString(m2v::core::ServerType type)
{
    switch(type) {
        case m2v::core::ServerType::Imap:
            return QStringLiteral("IMAP");
        case m2v::core::ServerType::Pop3:
            return QStringLiteral("POP3");
        case m2v::core::ServerType::Smtp:
            return QStringLiteral("SMTP");
    }
}

m2v::core::ServerType AccountDatabaseConnector::stringToType(const QString &type)
{
    if(type == "IMAP")
    {
        return m2v::core::ServerType::Imap;
    }
    else if(type == "POP3")
    {
        return m2v::core::ServerType::Pop3;
    }
    else if(type == "SMTP")
    {
        return m2v::core::ServerType::Smtp;
    }

    return m2v::core::ServerType::Imap;
}

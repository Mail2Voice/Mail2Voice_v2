CREATE TABLE account (
    account_id INTEGER PRIMARY KEY,
    username TEXT,
    password TEXT,
    displayname TEXT,
    email TEXT,
    incomingProtocol TEXT,
    incomingAddress TEXT,
    incomingPort INTEGER,
    incomingSsl INTEGER,
    outgoingAddress TEXT,
    outgoingPort INTEGER,
    outgoingSsl INTEGER
);

CREATE TABLE contact (
    contact_id INTEGER PRIMARY KEY,
    name TEXT,
    email TEXT,
    portrait TEXT,
    groups TEXT
);

CREATE TABLE email (
    email_id INTEGER PRIMARY KEY,
    account_id INTEGER,
    message_id TEXT,
    subject TEXT,
    sender TEXT,
    recipients TEXT,
    carbonCopy TEXT,
    blindCarbonCopy TEXT,
    date INTEGER,
    content TEXT,
    attachments TEXT,
    extraHeaders TEXT,
    read INTEGER,
    folder TEXT,
    FOREIGN KEY(account_id) REFERENCES account(account_id)
);

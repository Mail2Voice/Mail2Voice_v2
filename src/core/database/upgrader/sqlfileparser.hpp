#pragma once

#include <QString>

class SqlFileParser
{
    public:
        SqlFileParser() = default;
        ~SqlFileParser() = default;

        QStringList parse(const QString &filename, bool *parseSuccess);

    private:
        QString m_filePath;
};

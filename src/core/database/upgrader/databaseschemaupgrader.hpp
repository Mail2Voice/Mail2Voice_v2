#pragma once

#include <QSqlDatabase>

#include <QString>

#include <limits>

/**
 * @brief This class intends to list all embedded SQL files in application and apply ones that are
 * greater than the current version.
 */
class DatabaseSchemaUpgrader
{
public:
    DatabaseSchemaUpgrader();

    bool upgrade();

private:
    static constexpr quint64 invalidSchemaVersion {std::numeric_limits<quint64>::max()};

private:
    static quint64 getVersionFromFilename(const QString &filename);
};

#include "databaseschemaupgrader.hpp"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QString>

#include "sqlfileparser.hpp"

DatabaseSchemaUpgrader::DatabaseSchemaUpgrader()
{
}

bool DatabaseSchemaUpgrader::upgrade()
{
    QSqlDatabase db = QSqlDatabase::database("mail2voice");

    if(!db.isValid() || !db.isOpen())
    {
        qCritical() << "Can't upgrade database because it is not opened.";
        return false;
    }

    db.exec(QStringLiteral("CREATE TABLE IF NOT EXISTS "
                           "version_history ("
                               "version INTEGER UNIQUE,"
                               "upgrade_timestamp INTEGER"
                           ");"));

    quint64 databaseSchemaVersion {0};
    QSqlQuery versionQuery = db.exec(QStringLiteral("SELECT version FROM "
                                                    "version_history "
                                                    "ORDER BY upgrade_timestamp DESC"));

    if(versionQuery.next())
    {
        databaseSchemaVersion = versionQuery.value("version").value<quint64>();
    }
    else
    {
        qCritical() << versionQuery.lastError().text();
    }

    // List all SQL files
    const QString rootPath{":/database"};
    QDir rootDir{rootPath};

    if(!db.transaction())
    {
        qCritical() << "Failed to start transaction.";
        return false;
    }

    QFileInfoList infoList = rootDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot, QDir::Name);
    quint64 lastFileVersion {0};
    bool mustUpgrade {false};

    try
    {
        for(const QFileInfo &fileInfo : infoList)
        {
            {
                quint64 fileVersion = getVersionFromFilename(fileInfo.fileName());

                if(fileVersion == invalidSchemaVersion)
                {
                    throw std::runtime_error("Could not get version for file "
                                             + fileInfo.fileName().toStdString());
                }

                if(fileVersion <= databaseSchemaVersion)
                {
                    continue;
                }

                lastFileVersion = fileVersion;
            }
            {
                mustUpgrade = true;
                SqlFileParser parser;
                bool parseSuccess {false};
                const QStringList sqlStatements = parser.parse(fileInfo.absoluteFilePath(),
                                                               &parseSuccess);

                if(!parseSuccess || sqlStatements.empty())
                {
                    throw std::runtime_error("Could not parse any SQL statement for file: "
                                             + fileInfo.fileName().toStdString());
                }

                for(const QString &sqlStatement : sqlStatements)
                {
                    QSqlQuery query = db.exec(sqlStatement);

                    if(query.lastError().type() != QSqlError::NoError) {
                        throw std::runtime_error("Database upgrade error with statement: " +
                                                 sqlStatement.toStdString() + " ->" +
                                                 query.lastError().text().toStdString());
                    }
                }
            }
        }
    }
    catch(const std::runtime_error &exception)
    {
        qCritical() << exception.what();
        db.rollback();
        return false;
    }

    if (mustUpgrade)
    {
        // Insert newest version number in version history.
        QString query = QString(R"(INSERT INTO version_history VALUES (%1, %2))")
                        .arg(QString::number(lastFileVersion))
                        .arg(QString::number(QDateTime::currentDateTimeUtc().toMSecsSinceEpoch()));

        QSqlQuery updateVersionQuery = db.exec(query);
    }

    db.commit();

    return true;
}

quint64 DatabaseSchemaUpgrader::getVersionFromFilename(const QString &filename)
{
    quint64 version {invalidSchemaVersion};

    QString versionAsString = filename.left(filename.indexOf('.'));

    bool conversionSuccess {false};
    version = versionAsString.toULongLong(&conversionSuccess, 10);

    if(!conversionSuccess)
    {
        return invalidSchemaVersion;
    }

    return version;
}

#include "sqlfileparser.hpp"

#include <QDebug>
#include <QFile>
#include <QRegularExpression>
#include <QTextStream>

QStringList SqlFileParser::parse(const QString& filename, bool* parseSuccess)
{
    QStringList parsedSqlRequests;

    QFile file(filename);
    QTextStream fileStream(&file);

    if(!file.open(QIODevice::ReadOnly))
    {
        qCritical() << "Could not open file" << filename;
        return {};
    }

    QString fileContent = file.readAll();
    file.close();

    fileContent.remove(QRegularExpression("--.*[\\n\\r]"));

     QStringList statements = fileContent.split(";", QString::SkipEmptyParts);

     if (statements.size() == 1) {
         *parseSuccess = false;
         return {};
     }

     for(QString &statement : statements) {
        statement.replace(QRegularExpression("(\\n\\r|\\n)\\s*"), " ");
        QString cleanedStatement = statement.trimmed();

        if(cleanedStatement.isEmpty())
        {
            continue;
        }

        parsedSqlRequests.append(cleanedStatement + ';');
     }

    *parseSuccess = true;
    return parsedSqlRequests;
}

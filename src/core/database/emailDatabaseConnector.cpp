#include "emailDatabaseConnector.hpp"

#include <QByteArray>
#include <QDebug>
#include <QRegularExpression>
#include <QSqlError>
#include <QSqlQuery>

/**
 * @brief EmailDatabaseConnector::EmailDatabaseConnector
 * @param dbConnectionName
 */
EmailDatabaseConnector::EmailDatabaseConnector(const QString& dbConnectionName)
   : m_database(QSqlDatabase::database(dbConnectionName))
{
}

/**
 * @brief EmailDatabaseConnector::~EmailDatabaseConnector
 */
EmailDatabaseConnector::~EmailDatabaseConnector()
{
}

/**
 * @brief EmailDatabaseConnector::addEmail
 * @param email
 */
void EmailDatabaseConnector::addEmail(m2v::core::Email& email)
{
    QSqlQuery queryAdd(m_database);

    //add in db
    queryAdd.prepare("INSERT INTO email (account_id, "
                                        "message_id , "
                                        "subject, "
                                        "sender, "
                                        "recipients, "
                                        "carbonCopy, "
                                        "blindCarbonCopy, "
                                        "date, "
                                        "content, "
                                        "attachments, "
                                        "extraHeaders, "
                                        "read, "
                                        "folder) "
                        " VALUES (:account_id, :message_id, :subject, :sender, :recipients, "
                        " :carbon_copy, :blind_carbon_copy, :date, :content, :attachments, :extraHeaders, :read, :folder )");

    queryAdd.bindValue(":account_id", email.accountId());
    queryAdd.bindValue(":message_id", email.messageId());
    queryAdd.bindValue(":subject", email.subject());

    QString serializedSenders = email.senders().join(",");
    queryAdd.bindValue(":senders", QString("{%1}").arg(serializedSenders));

    QString serializedRecipients = email.recipients().join(",");
    queryAdd.bindValue(":recipients", QString("{%1}").arg(serializedRecipients));

    QString serializedCarbonCopies = email.carbonCopy().join(",");
    queryAdd.bindValue(":carbon_copy", QString("{%1}").arg(serializedCarbonCopies));

    QString serializedBlindCopies = email.blindCarbonCopy().join(",");
    queryAdd.bindValue(":blind_carbon_copy", QString("{%1}").arg(serializedBlindCopies));

    queryAdd.bindValue(":date", email.date().toMSecsSinceEpoch());
    queryAdd.bindValue(":content", email.content());

    //convert AttachmentList -> QString
    QString serializedAttachments;

    for(auto& attachment : email.attachments())
    {
        if(!serializedAttachments.isEmpty())
        {
            serializedAttachments += "|";
        }
        serializedAttachments += attachment->name() + "|";
        serializedAttachments += attachment->mimetype() + "|";
        serializedAttachments += QString(attachment->content());
    }

    queryAdd.bindValue(":attachments", serializedAttachments);

    QString extralist = email.extraHeaders().join(",");
    queryAdd.bindValue(":extraHeaders", QString("{%1}").arg(extralist));

    queryAdd.bindValue(":read", email.read());
    queryAdd.bindValue(":folder", email.folder());

    if(!queryAdd.exec())
    {
        throw std::runtime_error("Failed to add email in database");
    }

    email.setId(queryAdd.lastInsertId().toInt());
}

/**
 * @brief EmailDatabaseConnector::deleteEmail
 * @param email
 */
void EmailDatabaseConnector::deleteEmail(const m2v::core::Email& email)
{
    if(!emailExist(email))
    {
        throw std::runtime_error("Can't delete email, doesn't exist in database.");
    }

    QSqlQuery queryDelete(m_database);
    queryDelete.prepare("DELETE FROM email WHERE email_id = (:email_id)");
    queryDelete.bindValue(":email_id", email.id());

    if(!queryDelete.exec())
    {
        throw std::runtime_error("Failed to delete email in database.");
    }
}

/**
 * @brief EmailDatabaseConnector::getEmailList
 * @return
 */
QList<m2v::core::Email> EmailDatabaseConnector::getEmailList() const
{
    QList<m2v::core::Email> emails;

    QSqlQuery query(m_database);
    query.exec("SELECT email_id,"
                     " account_id, "
                     " message_id , "
                     " subject, "
                     " sender, "
                     " recipients, "
                     " carbonCopy, "
                     " blindCarbonCopy, "
                     " date, "
                     " content, "
                     " attachments, "
                     " extraHeaders, "
                     " read, "
                     " folder FROM email");

    static const QRegularExpression separator("[{,}]");
    static const QRegularExpression attachSeparator("[{|}]");

    while(query.next())
    {
        m2v::core::Email email;
        email.setId(query.value("email_id").toLongLong());
        email.setAccountId(query.value("account_id").toInt());
        email.setMessageId(query.value("message_id").toString());
        email.setSubject(query.value("subject").toString());

        QString senders = query.value("senders").toString();
        QStringList listSenders = senders.split(separator, QString::SkipEmptyParts);
        email.setSenders(listSenders);

        QString recipients = query.value("recipients").toString();
        QStringList listRecipients = recipients.split(separator, QString::SkipEmptyParts);
        email.setRecipients(listRecipients);

        QString carbonCopy = query.value("carbonCopy").toString();
        QStringList carbonCopyList = carbonCopy.split(separator, QString::SkipEmptyParts);
        email.setCarbonCopy(carbonCopyList);

        QString blindCarbonCopy = query.value("blindCarbonCopy").toString();
        QStringList blindCarbonCopyList = blindCarbonCopy.split(separator, QString::SkipEmptyParts);
        email.setBlindCopy(blindCarbonCopyList);

        email.setDate(QDateTime::fromMSecsSinceEpoch(query.value("date").toLongLong()));
        email.setContent(query.value("content").toString());

        QString serializedAttachments = query.value("attachments").toString();
        QStringList serializedAttachmentsParts = serializedAttachments.split(attachSeparator, QString::SkipEmptyParts);
        m2v::core::AttachmentList attachments;

        for(int i = 0; i < serializedAttachmentsParts.size(); i += 3)
        {
            std::shared_ptr<m2v::core::Attachment> attachment = std::make_shared<m2v::core::Attachment>();
            attachment->setName(serializedAttachmentsParts[i]);
            attachment->setMimetype(serializedAttachmentsParts[i+1]);
            attachment->setContent(serializedAttachmentsParts[i+2].toUtf8());
            attachments.push_back(attachment);
        }

        email.setAttachments(attachments);

        QString extraHeaders = query.value("extraHeaders").toString();
        QStringList extraHeadersList = extraHeaders.split(separator, QString::SkipEmptyParts);
        email.setExtraHeader(extraHeadersList);

        email.setRead(query.value("read").toBool());
        email.setFolder(query.value("folder").toString());

        emails.push_back(email);
    }

    return emails;
}

/************LOCAL FUNCTIONS: *************/
/**
 * @brief emailExist
 * @return true if the email exists in database
 */
bool EmailDatabaseConnector::emailExist(const m2v::core::Email& email) const
{
    QSqlQuery checkQuery(m_database);
    checkQuery.prepare("SELECT email_id FROM email WHERE email_id = (:id)");
    checkQuery.bindValue(":id", email.id());

    return checkQuery.exec() && checkQuery.first();
}

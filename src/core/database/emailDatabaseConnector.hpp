#pragma once

#include <QList>
#include <QSqlDatabase>
#include <QString>

#include "../email.hpp"

class EmailDatabaseConnector final
{
public:

    EmailDatabaseConnector(const QString& dbConnectionName);
    ~EmailDatabaseConnector();

    void addEmail(m2v::core::Email& email);
    void deleteEmail(const m2v::core::Email& email);
    QList<m2v::core::Email> getEmailList() const;
    bool emailExist(const m2v::core::Email& email) const;

private:
    QSqlDatabase m_database;
};

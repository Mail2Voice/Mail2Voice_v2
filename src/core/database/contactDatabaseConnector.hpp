#pragma once

#include <QList>
#include <QSqlDatabase>
#include <QString>

#include "../contact.hpp"

class ContactDatabaseConnector final
{
  public:
    ContactDatabaseConnector(const QString& dbConnectionName);
    ~ContactDatabaseConnector();

    void addContact(m2v::core::Contact& contact);
    void deleteContact(const m2v::core::Contact& contact);
    QList<m2v::core::Contact> getContactList() const;
    bool contactExist(const m2v::core::Contact& contact) const;

  private:
    QSqlDatabase m_database;
};

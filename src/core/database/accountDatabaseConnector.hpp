#pragma once

#include <QList>
#include <QSqlDatabase>
#include <QString>

#include "../account.hpp"

class AccountDatabaseConnector final
{
public:
    AccountDatabaseConnector() = default;
    AccountDatabaseConnector(const QString& dbConnectionName);
    ~AccountDatabaseConnector();

    void setupDatabase(const QString& dbConnectionName);

    bool saveAccount(m2v::core::Account& account);
    void removeAccount(const m2v::core::Account& account);
    QList<m2v::core::Account> listAccounts() const;
    bool isAccountStored(const m2v::core::Account& account) const;

private:
    static QString typeToString(m2v::core::ServerType type);
    static m2v::core::ServerType stringToType(const QString& type);

private:
    QSqlDatabase m_database;
};

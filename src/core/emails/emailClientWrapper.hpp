#pragma once

#include <QObject>
#include <QHash>

#include <memory>
#include <functional>

#include "account.hpp"

class QTcpSocket;
class QJsonObject;

namespace m2v
{
namespace core
{
class Email;

class EmailClientWrapper : public QObject
{
    Q_OBJECT

public:
    EmailClientWrapper(quint16 nodeJSClientPort, QObject *object = nullptr);

    // Messages to imap layer
    bool login(const Account &account);
    void refresh();
    void fetchContent(const QString &mailFolder, qint64 id);

signals:
    void connected();
    void loginSuccess();
    void loginFailed();
    void mailboxListReceived(const QStringList &mailboxList);
    void emailReceived(std::shared_ptr<core::Email> email);
    void errorOccurred(const QString &error);
    void contentReceived(qint64 emailId, const QString& mailbox, const QString &content);
    void fetchContentFailed(qint64 emailId, const QString& mailbox);

private:
    void sendMessage(const QString &type, const QJsonObject &parameters);
    void sendMessage(const QString &type);
    void onMessageReceived(const QString &type, const QJsonValue &parameters);

private:
    QTcpSocket *m_socket{nullptr};
    quint32 m_expectedDataSize{};
    QHash<QString, std::function<void(const QJsonValue &)>> m_messageDispatcher;
    // TODO: This is a temporary storage for emails, we will have to use the database instead.
    QHash<qint64, std::shared_ptr<core::Email>> localEmailStorage;

private:
    static const int ImapModuleConnectionTimerMs {3000};
    static const int ImapModuleConnectionTimeoutMs {10000};
};

}
}

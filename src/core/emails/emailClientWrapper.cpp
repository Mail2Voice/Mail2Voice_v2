#include "emailClientWrapper.hpp"

#include "../email.hpp"

#include <QHostAddress>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QTcpSocket>
#include <QTimer>
#include <QDataStream>

namespace m2v
{
namespace core
{
QStringList toStringList(const QJsonArray &jsonArray)
{
    QStringList result;

    for(const QJsonValue &value: jsonArray)
    {
        result.append(value.toObject().value("address").toString());
    }

    return result;
}

EmailClientWrapper::EmailClientWrapper(quint16 nodeJSClientPort, QObject *object) :
    QObject(object),
    m_socket(new QTcpSocket(this))
{
    connect(m_socket, &QTcpSocket::connected, [this]
    {
        qWarning("connected to the Nodejs server");
        emit connected();
    });

    connect(m_socket, &QTcpSocket::readyRead, [this]
    {
        forever
        {
            if(m_expectedDataSize == 0)
            {
                if(m_socket->bytesAvailable() < static_cast<int>(sizeof(m_expectedDataSize)))
                {
                    // We cannot read the header, skip

                    return;
                }

                qint64 readSize = m_socket->read(reinterpret_cast<char *>(&m_expectedDataSize), sizeof(m_expectedDataSize));
                Q_ASSERT(readSize == sizeof(m_expectedDataSize));
            }

            if(m_socket->bytesAvailable() < m_expectedDataSize)
            {
                // Not enough data has arrived, wait for more to come

                return;
            }

            QByteArray data = m_socket->read(m_expectedDataSize);
            m_expectedDataSize = 0;

            QJsonParseError error;
            QJsonDocument document = QJsonDocument::fromJson(data, &error);
            if(error.error != QJsonParseError::NoError)
            {
                qWarning() << "Received an invalid message from the server:" << error.errorString() << "data:" << QString::fromUtf8(data);

                return;
            }

            QJsonObject documentObject = document.object();

            onMessageReceived(documentObject.value("type").toString(), documentObject.value("value"));
        }
    });

    m_messageDispatcher =
    {
        {
            "loginSuccess",
            [this](const QJsonValue &parameters)
            {
                Q_UNUSED(parameters);

                emit loginSuccess();
            }
        },
        {
            "loginFailed",
            [this](const QJsonValue &parameters)
            {
                Q_UNUSED(parameters);

                emit loginFailed();
            }
        },
        {
            "mailboxList",
            [this](const QJsonValue &parameters)
            {
                QStringList mailboxList;

                for(QJsonValueRef mailbox: parameters.toArray())
                {
                    mailboxList << mailbox.toString();
                }

                emit mailboxListReceived(mailboxList);
            }
        },
        {
            "newMessage",
            [this](const QJsonValue &parameters)
            {
                QJsonObject parametersObject = parameters.toObject();
                QJsonObject envelope = parametersObject.value("envelope").toObject();

                auto email = std::make_shared<core::Email>();
                email->setId(parametersObject.value("uid").toInt());
                email->setFolder(parametersObject.value("mailbox").toString());
                email->setSubject(envelope.value("subject").toString());
                email->setSenders(toStringList(envelope.value("sender").toArray()));
                email->setRecipients(toStringList(envelope.value("to").toArray()));
                email->setBodystructure(QVariant{parametersObject.value("bodystructure")}.toMap());

                localEmailStorage.insert(email->id(), email);// Temporary email storage

                QStringList recipients;
                for(QJsonValueRef to: envelope.value("to").toArray())
                {
                    recipients.append(to.toObject().value("address").toString());
                }
                email->setRecipients(recipients);

                emit this->emailReceived(email);
            }
        },
        {
            "content",
            [this](const QJsonValue &parameters)
            {
                QJsonObject parametersObject = parameters.toArray()[0].toObject();

                // Get the e-mail data from our local storage
                qint64 emailId = parametersObject.value("uid").toInt();
                QString mailbox = parametersObject.value("mailbox").toString();
                std::shared_ptr<core::Email> email = localEmailStorage.value(emailId);
                Q_ASSERT(email);

                QString content = parametersObject.value("content").toObject().value("html").toString();
                if(content.isEmpty())
                {
                    content = parametersObject.value("content").toObject().value("textAsHtml").toString();
                }

                emit contentReceived(emailId, mailbox, content);
            }
        },
        {
            "fetchContentFailed",
            [this](const QJsonValue &parameters)
            {
                QJsonObject parametersObject = parameters.toObject();
                qint64 emailId = parametersObject.value("uid").toInt();
                QString mailbox = parametersObject.value("mailbox").toString();

                emit fetchContentFailed(emailId, mailbox);
            }
        },
        {
            "error",
            [this](const QJsonValue &parameters)
            {
                // Received a fatal IMAP server error
                m_socket->disconnectFromHost();

                emit errorOccurred(parameters.toString());
            }
        }
    };

    QTimer::singleShot(ImapModuleConnectionTimerMs, [this, nodeJSClientPort]()
    {
        m_socket->connectToHost(QHostAddress{QHostAddress::LocalHost}, nodeJSClientPort);
        if(!m_socket->waitForConnected(ImapModuleConnectionTimeoutMs))
        {
            qWarning("Failed connecting to the Nodejs server!");
        }
    });
}

bool EmailClientWrapper::login(const core::Account& account)
{
    if(!account.isValid())
    {
        return false;
    }

    sendMessage("login", QJsonObject::fromVariantHash(
    {
        {"username", account.username()},
        {"password", account.password()},
        {"host", account.storeSettings().address()},
        {"port", account.storeSettings().port()}
    }));

    return true;
}

void EmailClientWrapper::refresh()
{
    Q_ASSERT(m_socket->isOpen());

    sendMessage("refresh");
}

void EmailClientWrapper::fetchContent(const QString &mailFolder, qint64 id)
{
    Q_ASSERT(m_socket->isOpen());

    sendMessage("fetchContent", QJsonObject::fromVariantHash(
    {
        {"mailFolder", mailFolder},
        {"id", QString::number(id)},
    }));
}

void EmailClientWrapper::sendMessage(const QString &type, const QJsonObject &parameters)
{
    QJsonObject object;

    if(!parameters.isEmpty())
    {
        Q_ASSERT(!parameters.contains("type"));

        object = parameters;
    }

    object.insert("type", type);

    QJsonDocument document{object};

#ifndef NDEBUG
    QByteArray data{document.toJson(QJsonDocument::Indented)};
#else
    QByteArray data{document.toJson(QJsonDocument::Compact)};
#endif

    Q_ASSERT(static_cast<unsigned int>(data.size()) < std::numeric_limits<quint32>::max());

    quint32 dataSize = static_cast<quint32>(data.size());
    data.prepend(reinterpret_cast<const char *>(&dataSize), sizeof(dataSize));

    m_socket->write(data);
}

void EmailClientWrapper::sendMessage(const QString &type)
{
    sendMessage(type, {});
}

void EmailClientWrapper::onMessageReceived(const QString &type, const QJsonValue &parameters)
{
    auto dispatcherEntry = m_messageDispatcher.find(type);

    if(dispatcherEntry == m_messageDispatcher.end())
    {
        qWarning() << "Received an invalid message from the server, type is" << type;

        return;
    }

    dispatcherEntry.value()(parameters);
}

}
}

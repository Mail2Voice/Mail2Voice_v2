import qbs

DynamicLibrary
{
    name: "mail2voice-core"
    files:
    [
        "account.hpp",
        "attachment.hpp",
        "contact.hpp",
        "database/accountDatabaseConnector.cpp",
        "database/accountDatabaseConnector.hpp",
        "database/contactDatabaseConnector.cpp",
        "database/contactDatabaseConnector.hpp",
        "database/emailDatabaseConnector.cpp",
        "database/emailDatabaseConnector.hpp",
        "database/sql/database.qrc",
        "database/upgrader/databaseschemaupgrader.cpp",
        "database/upgrader/databaseschemaupgrader.hpp",
        "database/upgrader/sqlfileparser.cpp",
        "database/upgrader/sqlfileparser.hpp",
        "email.cpp",
        "email.hpp",
        "emailServiceWrapper.hpp",
        "jobs/abstractJob.hpp",
        "jobs/abstractJobDefinition.hpp",
        "jobs/fetchMailContentJob.cpp",
        "jobs/fetchMailContentJob.hpp",
        "jobs/jobRunner.cpp",
        "jobs/jobRunner.hpp",
        "jobs/jobs.cpp",
        "jobs/jobs.hpp",
        "jobs/testJob.cpp",
        "jobs/testJob.hpp",
        "jobs/testJobDefinition.cpp",
        "jobs/testJobDefinition.hpp",
        "serverSettings.hpp",
        "emails/emailClientWrapper.hpp",
        "emails/emailClientWrapper.cpp",
    ]

    cpp.cxxLanguageVersion: "c++14"

    Depends
    {
        name: "Qt";
        submodules:
        [
            "core",
            "sql",
            "network",
        ]
    }

    Group
    {
        fileTagsFilter: product.type
        qbs.install: true
    }
}

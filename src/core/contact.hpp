#pragma once

#include <memory>
#include <QMetaType>
#include <QString>
#include <QStringList>

namespace m2v
{
namespace core
{
class Contact final
{
public:
    Contact() = default;

    Contact(const QString& email, const QString& name = QString(),
            const QString& portrait = QString(), const QStringList& groups = QStringList()) :
        m_email{email},
        m_name{name},
        m_portrait{portrait},
        m_groups{groups}
    {
    }

    Contact(const Contact& other) :
        m_id{other.m_id},
        m_email{other.m_email},
        m_name{other.m_name},
        m_portrait{other.m_portrait},
        m_groups{other.m_groups}
    {
    }

    Contact &operator=(Contact other)
    {
        std::swap(m_id, other.m_id);
        std::swap(m_email, other.m_email);
        std::swap(m_name, other.m_name);
        std::swap(m_portrait, other.m_portrait);
        std::swap(m_groups, other.m_groups);

        return *this;
    }

    qint64 id()                    const { return m_id; }
    const QString& email()         const { return m_email; }
    const QString& name()          const { return m_name; }
    const QString& portrait()      const { return m_portrait; }
    const QStringList& groups()    const { return m_groups; }

    void setId(qint64 id)                       { m_id = id;}
    void setEmail(const QString& email)         { m_email = email; }
    void setName(const QString& name)           { m_name = name; }
    void setPortrait(const QString& portrait)   { m_portrait = portrait; }
    void setGroups(const QStringList& groups)   { m_groups = groups; }


private:
    qint64 m_id{-1};
    QString m_email;
    QString m_name;
    QString m_portrait;
    QStringList m_groups;
};
}
}

Q_DECLARE_METATYPE(m2v::core::Contact)
Q_DECLARE_METATYPE(std::shared_ptr<m2v::core::Contact>)

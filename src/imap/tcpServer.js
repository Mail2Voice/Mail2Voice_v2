'use strict';

// Load the TCP Library
const net = require('net');
// importing Client class
const TcpClient = require('./tcpClient');

const EventEmitter = require('events');

class TcpServer extends EventEmitter
{
    constructor (port, address)
    {
        super();
        this.port = port || 0;
        this.address = address || '127.0.0.1';

        // Array to hold our currently connected clients
        this.clients = [];
    }

    /*
     * Broadcasts messages to the network
     * The clientSender doesn't receive it's own message
    */
    broadcast (message)
    {
        this.clients.forEach((client) =>
        {
            client.receiveMessage(message);
        });
    }

    /*
     * Starting the server
     * The callback is executed when the server finally inits
    */
    start (callback)
    {
        let server = this;

        this.connection = net.createServer((socket) =>
        {
            let client = new TcpClient(socket);

            // Validation, if the client is valid
            if (!server._validateClient(client))
            {
                client.socket.destroy();
                return;
            }

            console.log(`${client.name} connected.\n`);

            server.emit('newClient', client);

            if(server.clients.length == 0)
            {
                //setInterval(server.emitFakeMessage, 5000, server);
            }

            // Storing client for later usage
            server.clients.push(client);

            // Triggered on message received by this client
            socket.on('data', (data) =>
            {
                //Create a chunk prop if it does not exist
                if(!socket.chunk)
                {
                      socket.chunk =
                      {
                          messageSize : 0,
                          buffer: new Buffer(0),
                          bufferStack: new Buffer(0)
                      };
                  }

                  //store the incoming data
                  socket.chunk.bufferStack = Buffer.concat([socket.chunk.bufferStack, data]);
                  //this is to check if you have a second message incoming in the tail of the first
                  var reCheck = false;
                  do
                  {
                      reCheck = false;
                      //if message size == 0 you got a new message so read the message size (first 4 bytes)
                      if (socket.chunk.messageSize == 0 && socket.chunk.bufferStack.length >= 4)
                      {
                          socket.chunk.messageSize = socket.chunk.bufferStack.readInt32LE(0);
                      }

                      //After read the message size (!= 0) and the bufferstack is completed and/or the incoming data contains more data (the next message)
                      if (socket.chunk.messageSize != 0 && socket.chunk.bufferStack.length >= socket.chunk.messageSize + 4)
                      {
                          var buffer = socket.chunk.bufferStack.slice(4, socket.chunk.messageSize + 4);
                          socket.chunk.messageSize = 0;
                          socket.chunk.bufferStack = socket.chunk.bufferStack.slice(buffer.length + 4);
                          server.emit('data', JSON.parse(buffer));
                          //if the stack contains more data after read the entire message, maybe you got a new message, so it will verify the next 4 bytes and so on...
                          reCheck = socket.chunk.bufferStack.length > 0;
                      }
                  }
                  while (reCheck);
            });

            // Triggered when this client disconnects
            socket.on('end', () =>
            {
                // Removing the client from the list
                server.clients.splice(server.clients.indexOf(client), 1);
                // Broadcasting that this player left
                console.log(`${client.name} disconnected.\n`);
            });

        });

        // starting the server
        let listener = this.connection.listen(this.port, this.address, function()
        {
            this.port = listener.address().port;

            // setuping the callback of the start function
            if (callback != undefined)
            {
                 callback(this.port);
            }
        });
    }

    emitFakeMessage (server)
    {
        server.broadcast(`
        {
            "newMessage":
            {
                "#": 123,
                "uid": 456,
                "flags": ["\\Seen", "$MyFlag"],
                "envelope":
                {
                    "date": "Fri, 13 Sep 2013 15:01:00 +0300",
                    "subject": "hello ${(Math.random() * 100)}",
                    "from": [{"name": "sender name", "address": "sender@example.com"}],
                    "to": [{"name": "Receiver name", "address": "receiver@example.com"}],
                    "message-id": "<abcde>"
                }
            }
        }`);
    }

    /*
     * An example function: Validating the client
     */
    _validateClient (client)
    {
        return client.isLocalHost();
    }
}

module.exports = TcpServer;

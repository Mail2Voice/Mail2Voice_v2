'use strict';

class TcpClient
{
    constructor (socket)
    {
        this.address = socket.remoteAddress;
        this.port 	 = socket.remotePort;
        this.name    = `${this.address}:${this.port}`;
        this.socket  = socket;
    }

    receiveMessage (message)
    {
        this.socket.write(message);
    }

    isLocalHost()
    {
        return this.address === 'localhost' || this.address === '127.0.0.1';
    }
}

module.exports = TcpClient;

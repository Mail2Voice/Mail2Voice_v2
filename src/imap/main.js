const ImapClient = require('emailjs-imap-client');
const simpleParser = require('mailparser').simpleParser;
const Utf8 = require('utf8');

const TcpServer = require('./tcpServer');

const fs = require('fs');
let imapClient;
let tcpServer = new TcpServer();

/*
const Config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));


let imapClient = new ImapClient.default(Config.host, Config.port,
{
    auth:
    {
        user: Config.username,
        pass: Config.password
    }
});

*/

// Starting our server
tcpServer.start(port =>
{
    process.stdout.write(`${port}\n`);
});

tcpServer.on('newClient', (newClient) =>
{
    console.log(`New client: ${newClient}`);
    sendMessage('connected');
});

tcpServer.on('data', (data) =>
{
    if(data.type === 'login')
    {
        login(data)
    }
    if(data.type === 'refresh')
    {
        console.log('handle refresh command');
        onClientConnected();
    }
    else if(data.type === 'fetchContent')
    {
        console.log('handle fetchContent command')
        fetchMessageBody(data.mailFolder, data.id);
    }
});

function login(data)
{
    console.log('login', data)
    imapClient = new ImapClient.default(data.host, data.port,
    {
        auth:
        {
            user: data.username,
            pass: data.password
        }
    })

    imapClient.onupdate = async function(path, type, value)
    {
        console.log('update received: ' + path + ' ' + type + ' ' + value);

        if(type == 'exists')
        {
            messages = await imapClient.listMessages(path, value, ['uid', 'flags', 'envelope']);

            messages.forEach((message) =>
            {
                sendMessage("newMessage", message);
            })
        }
    }

    imapClient.onerror = function(error)
    {
        sendMessage("error", error);
    }

    imapClient.connect().then(() =>
    {
        console.log("login successful !")
        sendMessage("loginSuccess");
    }).catch((error) => 
    {
        console.log(error);
        sendMessage("loginFailed");
    });
}

async function fetchMessageBody(mailbox, uid)
{
    imapClient.listMessages(mailbox, uid, ['uid', 'body[]'], {'byUid': true}).then(async (messages) => {
        let messagesArray = new Array();

        for(let messageIndex in messages)
        {
            let message = messages[messageIndex];
            try {
                let parsed = await simpleParser(Utf8.decode(message['body[]']));
                console.log(`Parsed email: ${uid}`);

                messagesArray.push({"uid": message['uid'], "mailbox": mailbox, "content": parsed});
            }
            catch(error)
            {
                sendMessage("fetchContentFailed", {"uid": message['uid'], "mailbox": mailbox});
            }
        }

        if(messagesArray.length > 0) {
            sendMessage("content", messagesArray);
        }

        imapClient.selectMailbox("INBOX");
    }).catch((error) => {
        sendMessage("fetchContentFailed", {"uid": message['uid'], "mailbox": mailbox});
    })
}

function sendMessage(type, message)
{
    let messageString = JSON.stringify({"type": type, "value": message});
    let messageLength = Buffer.byteLength(messageString);
    let buffer = Buffer.allocUnsafe(4 + messageLength);

    buffer.writeUInt32LE(messageLength);
    buffer.write(messageString, 4);

    tcpServer.broadcast(buffer);
}

function listMailboxes(currentMailbox, mailboxesArray)
{
    if(!currentMailbox.root)
    {
        mailboxesArray.push(currentMailbox.path);
    }

    for(let childMailbox of currentMailbox.children)
    {
        listMailboxes(childMailbox, mailboxesArray);
    }
}

async function listMessages(mailboxesArray)
{
    let messagesArray = new Array();

    // WARNING: This area is a work in progress, don't pay attention to fresh paint.

    //for(let mailboxIndex in mailboxesArray)
    {
        let mailbox = 'INBOX';  //mailboxesArray[mailboxIndex];

        //let mailboxInfo = await imapClient.selectMailbox(mailbox);

        //let messageCount = mailboxInfo.exists;

        //if(messageCount <= 0)
        //    continue;

        // let messageToFetchCount = messageCount - 10 + 1;//TMP
        // if(messageToFetchCount < 0)
        // messageToFetchCount = messageCount;
        let messages = await imapClient.listMessages(mailbox, '1:*', ['uid', 'flags', 'envelope', 'bodystructure']);
        // let messages = await imapClient.listMessages(mailbox, messageToFetchCount + ':' + messageCount, ['uid', 'flags', 'envelope']);

        for(let messageIndex in messages)
        {
            let message = messages[messageIndex];

            message.mailbox = mailbox;
            messagesArray.push(message);
        }
    }

    imapClient.selectMailbox("INBOX");

    return messagesArray;
}

async function onClientConnected()
{
    let mailboxes = await imapClient.listMailboxes();

    let mailboxesArray = new Array();

    listMailboxes(mailboxes, mailboxesArray);

    sendMessage("mailboxList", mailboxesArray);

    let messages = await listMessages(mailboxesArray);

    messages.forEach((message) =>
    {
        sendMessage("newMessage", message);
    })
}


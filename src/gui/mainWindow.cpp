#include "mainWindow.hpp"
#include "ui_mainWindow.h"

#include "core/accounts/accounts.hpp"
#include "core/emails/emailClientWrapper.hpp"
#include "core/jobs/fetchMailContentJob.hpp"
#include "core/jobs/jobRunner.hpp"
#include "gui/common/widgets/pageWidget.hpp"
#include "gui/system/updateChecker.hpp"
#include "gui/system/versionNumber.hpp"

#include <QDebug>
#include <QFile>
#include <QSettings>
#include <QProcess>

namespace m2v
{
namespace gui
{
MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    m_ui(std::make_unique<Ui::MainWindow>()),
    m_process(new QProcess(this))
{
    m_ui->setupUi(this);
    core::JobRunner::init(this);
    core::Accounts::loadDefaultAccount();

    connect(m_ui->pageSettings, &pages::AccountSettingsPageWidget::accountChanged, this, &MainWindow::onAccountChanged);

    connect(m_ui->widgetMenu, &MenuWidget::changeViewRequest,
            this, &MainWindow::onChangeViewRequest);

    connect(m_ui->widgetMenu, &MenuWidget::exitApplicationRequest,
            this, &MainWindow::onExitApplicationRequest);

    connect(m_ui->stackedWidgetViews, &QStackedWidget::currentChanged, [this](int current)
    {
        Q_UNUSED(current);

        auto &currentPageWidget = *static_cast<pages::PageWidget*>(m_ui->stackedWidgetViews->currentWidget());

        currentPageWidget.pageEntered();

        if(m_currentPageWidget)
        {
            m_currentPageWidget->pageExited();
        }

        m_currentPageWidget = &currentPageWidget;
    });

    {
        // Read and apply the global style sheet
        QFile stylesheetFile(":/resources/stylesheets/main.css");
        bool stylesheetOpen = stylesheetFile.open(QFile::ReadOnly);

        Q_ASSERT(stylesheetOpen);

        m_ui->containerTopLevel->setStyleSheet(stylesheetFile.readAll());
    }

    // Test version checking
    auto *updateChecker = new UpdateChecker(this);
    connect(updateChecker, &UpdateChecker::checkingFinished, [](const QVersionNumber &version)
    {
        qDebug() << "Version check finished:" << version;
    });
    updateChecker->check();

    // Select the starting stack
    m_ui->stackedWidgetViews->setCurrentIndex(0);

    // Load settings
    {
        QSettings settings;

        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("windowState").toByteArray());
    }

    m_process->setWorkingDirectory(RUNTIME_IMAP_CLIENT_FOLDER);
    m_process->setProcessChannelMode(QProcess::SeparateChannels);
    m_process->start("node", {"main.js"});
    if(!m_process->waitForStarted(ImapModuleStartTimeoutMs))
    {
        qWarning("Failed starting the Nodejs IMAP client!");
    }

    if(!m_process->waitForReadyRead(ImapModuleReadyTimeoutMs))
    {
        qWarning("Failed waiting for the Nodejs IMAP client to start!");
    }

    bool ok{false};
    QString line = m_process->readLine();
    quint16 port = line.toUShort(&ok);

    if(!ok)
    {
        qWarning() << line;
    }

    Q_ASSERT(ok);
    Q_ASSERT(port != 0);

#ifndef NDEBUG
    // Redirect the processe's output to stdout
    connect(m_process, &QProcess::readyRead, [this]()
    {
        static QTextStream stdoutTextStream(stdout);

        stdoutTextStream << m_process->readAll();
        stdoutTextStream.flush();
    });
#endif

    m_emailClientWrapper = new core::EmailClientWrapper(port, this);

    connect(m_emailClientWrapper, &core::EmailClientWrapper::connected, [this]() {
        if(core::Accounts::currentAccount().id() != -1)
        {
            m_emailClientWrapper->login(core::Accounts::currentAccount());
        }
        else
        {
            m_ui->stackedWidgetViews->setCurrentWidget(m_ui->pageSettings);
        }
    });

    connect(m_emailClientWrapper, &core::EmailClientWrapper::loginSuccess, [this]() {
        m_emailClientWrapper->refresh();

    });

    connect(m_emailClientWrapper, &core::EmailClientWrapper::emailReceived, m_ui->pageEmailsView, &pages::EmailsPageWidget::onEmailReceived);
    connect(m_ui->widgetMenu, &MenuWidget::refreshEMailList, m_emailClientWrapper, &core::EmailClientWrapper::refresh);
    connect(m_ui->pageEmailsView, &pages::EmailsPageWidget::emailSelected, this, [this](qint64 emailId)
    {
        auto job = std::make_unique<core::FetchMailContentJob>("INBOX", emailId, *m_emailClientWrapper, this);
        connect(job.get(), &core::FetchMailContentJob::jobFinished, m_ui->pageEmailsView, &pages::EmailsPageWidget::onEmailContentReceived);
        connect(job.get(), &core::FetchMailContentJob::error, m_ui->pageEmailsView, &pages::EmailsPageWidget::onEmailContentFailed);

        //TODO: Send the current mailbox
        core::JobRunner::getInstance()->addJob(std::move(job));
    });
}

MainWindow::~MainWindow()
{
    m_process->terminate();
    m_process->waitForFinished();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);

    auto &currentPageWidget = *static_cast<pages::PageWidget*>(m_ui->stackedWidgetViews->currentWidget());
    currentPageWidget.pageExited();

    // Save settings
    {
        QSettings settings;

        settings.setValue("geometry", saveGeometry());
        settings.setValue("windowState", saveState());
    }
}

void MainWindow::onChangeViewRequest(View view)
{
    switch(view)
    {
        case View::Inbox:
            m_ui->stackedWidgetViews->setCurrentWidget(m_ui->pageEmailsView);
        break;
        case View::Contacts:
            m_ui->stackedWidgetViews->setCurrentWidget(m_ui->pageContacts);
        break;
        case View::Settings:
            m_ui->stackedWidgetViews->setCurrentWidget(m_ui->pageSettings);
        break;
        default:
        break;
    }
}

void MainWindow::onAccountChanged()
{
    if (core::Accounts::currentAccount().isValid())
    {
        m_emailClientWrapper->login(core::Accounts::currentAccount());
    }
}

void MainWindow::onExitApplicationRequest()
{
    closeEvent(nullptr);
    qApp->exit(0);
}

}
}

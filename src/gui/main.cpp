#include "mainWindow.hpp"

#include <QApplication>
#include <QSqlDatabase>

#include "core/database/upgrader/databaseschemaupgrader.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setOrganizationName("Mail2Voice Team");
    app.setOrganizationDomain("mail2voice.org");
    app.setApplicationName("Mail2Voice");
    app.setApplicationVersion(PROJECT_VERSION);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "mail2voice");
    db.setDatabaseName("mail2voice.db");
    db.open();

    DatabaseSchemaUpgrader upgrader;
    upgrader.upgrade();

    m2v::gui::MainWindow mainWindow;
    mainWindow.show();

    return app.exec();
}

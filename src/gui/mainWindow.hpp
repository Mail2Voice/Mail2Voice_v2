#pragma once

#include <QMainWindow>

#include <memory>

#include "gui/common/widgets/menuWidget.hpp"

class QProcess;

namespace Ui
{
    class MainWindow;
}

namespace m2v
{

namespace core
{
    class EmailClientWrapper;
}

namespace gui
{
namespace pages
{
class PageWidget;
}

/**
 * @brief Main window of the application.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit MainWindow(QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~MainWindow();

protected:
    /**
     * @brief Catch the close event to do some cleanup before exiting.
     * @param event The close event.
     */
    void closeEvent(QCloseEvent *event) override;

private slots:
    /**
     * @brief Slot called when the user clicks on a view button in main menu.
     * @param view The requested view.
     */
    void onChangeViewRequest(View view);

    void onAccountChanged();

    /**
     * @brief Slot called when the user clicks on the exit button.
     */
    void onExitApplicationRequest();

private:
    std::unique_ptr<Ui::MainWindow> m_ui;
    pages::PageWidget *m_currentPageWidget{nullptr};
    QProcess *m_process {nullptr};
    core::EmailClientWrapper *m_emailClientWrapper{nullptr};


private:
    static const int ImapModuleStartTimeoutMs {5000};
    static const int ImapModuleReadyTimeoutMs {5000};
};
}
}

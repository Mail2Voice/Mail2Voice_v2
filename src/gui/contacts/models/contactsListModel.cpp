#include "contactsListModel.hpp"

#include "core/contact.hpp"

namespace m2v
{
namespace gui
{

ContactsListModel::ContactsListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void ContactsListModel::loadContacts(const QVector<std::shared_ptr<core::Contact>> &contactsToLoad)
{
    beginInsertRows(QModelIndex(), 0, contactsToLoad.size() - 1);
    m_contacts = contactsToLoad;
    endInsertRows();
}

QVariant ContactsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    if(role == Qt::DisplayRole)
    {
        return QString(tr("Contacts"));
    }

    return {};
}

int ContactsListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if(parent.isValid())
    {
        return 0;
    }

    return m_contacts.size();
}

QVariant ContactsListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return {};
    }

    if(role == Qt::DisplayRole)
    {
        if(index.row() >= 0 && (index.row() < m_contacts.size()))
        {
            return QVariant::fromValue(m_contacts.at(index.row()));
        }
    }

    return {};
}

}
}

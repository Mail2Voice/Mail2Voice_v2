#pragma once

#include <QAbstractListModel>

#include <memory>


namespace m2v
{

namespace core
{
    class Contact;
}


namespace gui
{

/**
 * @brief Model for all contacts.
 */
class ContactsListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent object.
     */
    explicit ContactsListModel(QObject *parent = nullptr);

    /**
     * @brief Loads a list of contacts in the model. Previous data is lost.
     * @param contactsToLoad The list of contacts to load.
     */
    void loadContacts(const QVector<std::shared_ptr<core::Contact>> &contactsToLoad);

    /**
     * @brief Returns data for given header location.
     * @param section Header section.
     * @param orientation Header orientation.
     * @param role Header data role requested.
     * @return Data for given header.
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    /**
     * @brief Returns the number of items in the model.
     * @param parent Parent index.
     * @return The number of items in the model.
     */
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    /**
     * @brief Fetch the data for given index and role.
     * @param index The model index of the requested data.
     * @param role The model role of the requested data.
     * @return The fetched data.
     */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    QVector<std::shared_ptr<m2v::core::Contact>> m_contacts;
};

}
}

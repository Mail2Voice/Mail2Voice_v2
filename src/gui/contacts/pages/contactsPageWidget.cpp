#include "contactsPageWidget.hpp"
#include "ui_contactsPageWidget.h"

#include "gui/common/containers/pixmapContainer.hpp"
#include "gui/common/guiUtils.hpp"
#include "gui/common/layouts/flowLayout.hpp"
#include "gui/contacts/data/avatarGenerator.hpp"
#include "gui/contacts/models/contactsListModel.hpp"
#include "gui/contacts/widgets/contactWidget.hpp"
#include "gui/common/factories/listWidgetFactory.h"

#include <QPainter>
#include <QScrollArea>

namespace m2v
{
namespace gui
{
namespace pages
{
ContactsPageWidget::ContactsPageWidget(QWidget *parent):
    PageWidget(parent),
    m_ui(std::make_unique<Ui::ContactsPageWidget>()),
    m_contactPixmapContainer(std::make_unique<PixmapContainer>("contacts", QPixmap{":/resources/icons/user.png"}))
{
    m_ui->setupUi(this);

    m_ui->containerContactPanels->setProperty("class", utils::css::container2);
    m_ui->widgetContactsList->setOrientation(Qt::Vertical, true);

    std::shared_ptr<m2v::gui::ContactsListModel> contactsListModel = std::make_shared<m2v::gui::ContactsListModel>();
    m_ui->widgetContactsList->setModel(contactsListModel);

    std::shared_ptr<m2v::gui::AbstractWidgetFactory> listWidgetFactory =
            std::make_shared<m2v::gui::ListWidgetFactory<gui::ContactWidget, core::Contact>>();
    m_ui->widgetContactsList->setListWidgetFactory(listWidgetFactory);

    connect(m_ui->widgetContactsList, &ListWidget::dataSelected,
            this, &ContactsPageWidget::onContactSelected);

    m_ui->stackedWidgetContactPanels->hide();

    contactsListModel->loadContacts(generateFakeContacts());
}

ContactsPageWidget::~ContactsPageWidget()
{
}

void ContactsPageWidget::onContactSelected(SelectableWidget *selectedWidget, const QVariant &contact)
{
    Q_UNUSED(selectedWidget)

    std::shared_ptr<m2v::core::Contact> selectedContact = contact.value<std::shared_ptr<m2v::core::Contact>>();

    if(selectedContact)
    {
        m_ui->stackedWidgetContactPanels->show();
        m_ui->stackedWidgetContactPanels->setCurrentWidget(m_ui->pageContactViewerPanel);
        m_ui->pageContactViewerPanel->setContact(selectedContact);
    }
}

QVector<std::shared_ptr<core::Contact>> ContactsPageWidget::generateFakeContacts()
{
    QVector<std::shared_ptr<core::Contact>> contacts;

    // Create some test contacts
    for(int contactIndex = 0; contactIndex < 15; ++contactIndex)
    {
        std::shared_ptr<core::Contact> contact = std::make_shared<core::Contact>(
                    QString("test%1@mail.com").arg(contactIndex),
                              QString("Jane Doe%1").arg(contactIndex),
                              ":/resources/icons/user.png");

        contacts.append(contact);
    }

    /*AvatarGenerator avatarGenerator;

    for(auto &contact: m_contacts)
    {
        contact.portraitPixmap = QPixmap::fromImage(avatarGenerator.generate(contact.contact.email(), {100, 100}));

        auto contactWidget = new ContactWidget(contact);

        // Update the contact widgets when the user selects a contact
        connect(contactWidget, &ContactWidget::selected, [this, &contact]()
        {
            m_ui->containerContactInformation->show();
            m_ui->labelContactName->setText(contact.contact.name());
            m_ui->labelContactEmailAddress->setText(contact.contact.email());
            m_ui->labelContactPortrait->setPixmap(contact.portraitPixmap);
        });

        layout->addWidget(contactWidget);
    }*/

    return contacts;
}
}
}
}

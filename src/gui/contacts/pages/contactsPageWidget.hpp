#pragma once

#include "core/contact.hpp"
#include "gui/common/widgets/pageWidget.hpp"


#include <memory>

namespace Ui
{
class ContactsPageWidget;
}

namespace m2v
{
namespace gui
{

class PixmapContainer;
class SelectableWidget;

namespace pages
{

/**
 * @brief Page to manage contacts.
 */
class ContactsPageWidget : public PageWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit ContactsPageWidget(QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~ContactsPageWidget();

private slots:
    /**
     * @brief Slot called when a contact has been selected.
     * @param selectedWidget The corresponding selected widget.
     * @param contact The selected contact data.
     */
    void onContactSelected(m2v::gui::SelectableWidget *selectedWidget, const QVariant& contact);

private:
    QVector<std::shared_ptr<core::Contact>> generateFakeContacts();

private:
    std::unique_ptr<Ui::ContactsPageWidget> m_ui;
    std::unique_ptr<PixmapContainer> m_contactPixmapContainer;

};
}
}
}

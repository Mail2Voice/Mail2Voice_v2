#include "contactWidget.hpp"
#include "ui_contactWidget.h"

#include "gui/common/guiUtils.hpp"

namespace m2v
{
namespace gui
{
ContactWidget::ContactWidget(std::shared_ptr<core::Contact> contact, QWidget *parent):
    SelectableWidget(utils::css::contactWidget, parent),
    m_contact(contact),
    m_ui(std::make_unique<Ui::ContactWidget>())
{
    m_ui->setupUi(this);

    m_ui->labelContactName->setText(contact->name());
    m_ui->labelContactPortrait->setPixmap(QPixmap(contact->portrait()));
}

ContactWidget::~ContactWidget()
{
}

}
}

#pragma once

#include "gui/common/widgets/selectableWidget.hpp"
#include "core/contact.hpp"

#include <memory>

namespace Ui
{
class ContactWidget;
}

namespace m2v
{
namespace gui
{

/**
 * @brief Contact widget appearing in contacts list.
 */
class ContactWidget : public SelectableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param contact The contact data to display.
     * @param parent Optional parent widget.
     */
    explicit ContactWidget(std::shared_ptr<core::Contact> contact, QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~ContactWidget();

private:
    std::shared_ptr<core::Contact> m_contact;
    std::unique_ptr<::Ui::ContactWidget> m_ui;
};
}
}

#include "contactViewerPanel.hpp"
#include "ui_contactViewerPanel.h"

#include "gui/common/guiUtils.hpp"
#include "core/contact.hpp"

namespace m2v
{
namespace gui
{

ContactViewerPanel::ContactViewerPanel(QWidget *parent) :
    StylableWidget(utils::css::container2, parent),
    m_ui(std::make_unique<Ui::ContactViewerPanel>())
{
    m_ui->setupUi(this);

    utils::setButtonStyle(m_ui->buttonAddContact, utils::css::normalButton, ":/resources/icons/user-plus.svg");
    utils::setButtonStyle(m_ui->buttonEditContact, utils::css::normalButton, ":/resources/icons/pencil-square-o.svg");
    utils::setButtonStyle(m_ui->buttonSendEmail, utils::css::normalButton, ":/resources/icons/microphone.svg");
    utils::setButtonStyle(m_ui->buttonDeleteContact, utils::css::warningButton, ":/resources/icons/user-times.svg");

    connect(m_ui->buttonAddContact, &QPushButton::clicked,
            this, &ContactViewerPanel::onButtonAddContactClicked);

    connect(m_ui->buttonDeleteContact, &QPushButton::clicked,
            this, &ContactViewerPanel::onButtonDeleteContactClicked);

    connect(m_ui->buttonEditContact, &QPushButton::clicked,
            this, &ContactViewerPanel::onButtonEditContactClicked);

    connect(m_ui->buttonSendEmail, &QPushButton::clicked,
            this, &ContactViewerPanel::onButtonSendEmailClicked);
}

ContactViewerPanel::~ContactViewerPanel()
{
}

void ContactViewerPanel::setContact(std::shared_ptr<const core::Contact> contact)
{
    m_ui->labelContactName->setText(contact->name());
    m_ui->labelContactEmailAddress->setText(contact->email());
    m_ui->labelContactPortrait->setPixmap(QPixmap(contact->portrait()));
}

void ContactViewerPanel::onButtonAddContactClicked()
{
    // TODO
}

void ContactViewerPanel::onButtonDeleteContactClicked()
{
    // TODO
}

void ContactViewerPanel::onButtonEditContactClicked()
{
    // TODO
}

void ContactViewerPanel::onButtonSendEmailClicked()
{
    // TODO
}

}
}

#pragma once

#include "gui/common/widgets/stylableWidget.hpp"

#include <memory>


namespace Ui
{
    class ContactViewerPanel;
}

namespace m2v
{

namespace core
{
    class Contact;
}

namespace gui
{

/**
 * @brief View contact details.
 */
class ContactViewerPanel : public StylableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit ContactViewerPanel(QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~ContactViewerPanel();

    /** Fills all fields of the view with given contact */
    void setContact(std::shared_ptr<const m2v::core::Contact> contact);

private:
    /**
     * @brief Slot called when user clicks on the add button/
     */
    void onButtonAddContactClicked();

    /**
     * @brief Slot called when user clicks on the delete button/
     */
    void onButtonDeleteContactClicked();

    /**
     * @brief Slot called when user clicks on the edit button/
     */
    void onButtonEditContactClicked();

    /**
     * @brief Slot called when user clicks on the send email button/
     */
    void onButtonSendEmailClicked();

private:
    std::unique_ptr<Ui::ContactViewerPanel> m_ui;
};

}
}

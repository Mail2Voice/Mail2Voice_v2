#pragma once

#include "core/contact.hpp"

#include <QPixmap>

namespace m2v
{
namespace gui
{
// Represents a contact within the GUI
struct GuiContact
{
    explicit GuiContact(const core::Contact &contact):
        contact(contact)
    {
    }

    core::Contact contact;
    QPixmap portraitPixmap;
};
}
}

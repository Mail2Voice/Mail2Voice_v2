#pragma once

#include <QImage>

#include <memory>

class QSize;
class QString;

namespace m2v
{
namespace gui
{
struct AvatarGeneratorPrivate;

///
/// \brief This class uses a set of SVG files to generate avatars using a string as input
///
class AvatarGenerator final
{
public:
    AvatarGenerator();
    ~AvatarGenerator();

    AvatarGenerator(const AvatarGenerator &) = delete;
    AvatarGenerator &operator=(const AvatarGenerator &) = delete;

    ///
    /// \brief Generates an avatar image using an input string and an image size
    /// \param input The input string
    /// \param imageSize The image size to output
    /// \return The avatar image
    ///
    QImage generate(const QString &input, const QSize &imageSize) const;

private:
    std::unique_ptr<AvatarGeneratorPrivate> m_private;
};
}
}

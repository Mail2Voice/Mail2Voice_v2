#include "avatarGenerator.hpp"
#include "avatarGenerator_private.hpp"

#include <QByteArray>
#include <QDir>
#include <QDomDocument>
#include <QFile>
#include <QPainter>
#include <QSvgRenderer>

#include <random>

namespace m2v
{
namespace gui
{
void createRenderersFromContentsDirectory(RendererList &list, const QString &directoryPath)
{
    QDir directory{directoryPath};

    for(const QString &entry: directory.entryList())
    {
        list.emplace_back(std::make_unique<QSvgRenderer>(directory.filePath(entry)));
    }
}

std::default_random_engine createRandomGeneratorFromString(const QString &input)
{
    QByteArray data = input.toUtf8();
    int checksum = qChecksum(data.data(), static_cast<uint>(data.size()));

    return std::default_random_engine(checksum);
}

int randomNumber(std::default_random_engine &generator, int min, int max)
{
    std::uniform_int_distribution<int> distribution(min, max);

    return distribution(generator);
}

template<class T>
T &randomListElement(std::default_random_engine &generator, const std::vector<std::unique_ptr<T>> &list)
{
    Q_ASSERT(!list.empty());

    return *list[static_cast<std::size_t>(randomNumber(generator, 0, static_cast<int>(list.size()) - 1))];
}

void setFaceBackgroundColor(QDomDocument &svgDocument, const QColor &color)
{
    QDomNodeList circles = svgDocument.elementsByTagName(QStringLiteral("circle"));

    for(int i = 0; i < circles.length(); ++i)
    {
        QDomElement circle = circles.at(i).toElement();

        if(circle.attribute("id") == "circle6096")
        {
            QString styleString = circle.attribute("style");
            QStringList styleArray = styleString.split(";");

            for(int j = 0; j < styleArray.size(); ++j)
            {
                if(styleArray[j].startsWith("fill:"))
                {
                    styleArray[j] = "fill:" + color.name(QColor::HexRgb).toLower();

                    return;
                }
            }

            circle.setAttribute("style", styleArray.join(";"));
        }
    }

    qWarning("AvatarGenerator: failed to change face background color");
}

QRectF percentToRect(const QSize &imageSize, const QPointF &position, const QSizeF &size)
{
    return
    {
        position.x() * imageSize.width(), position.y() * imageSize.height(),
        size.width() * imageSize.width(), size.height() * imageSize.height()
    };
}

AvatarGenerator::AvatarGenerator():
    m_private(std::make_unique<AvatarGeneratorPrivate>())
{
    // Load the face SVG file
    {
        QFile svgFile(QStringLiteral(":/resources/avatars/faces/face.svg"));

        {
            bool isOpen = svgFile.open(QIODevice::ReadOnly);

            Q_ASSERT(isOpen);
        }

        m_private->faceDocument.setContent(svgFile.readAll());
    }

    createRenderersFromContentsDirectory(m_private->bowtieRenderers, QStringLiteral(":/resources/avatars/bowties"));
    createRenderersFromContentsDirectory(m_private->glassesRenderers, QStringLiteral(":/resources/avatars/glasses"));
    createRenderersFromContentsDirectory(m_private->hatRenderers, QStringLiteral(":/resources/avatars/hats"));
}

AvatarGenerator::~AvatarGenerator()
{
}

QImage AvatarGenerator::generate(const QString &input, const QSize &imageSize) const
{
    // Create a random number generator that uses a string as a seed
    std::default_random_engine generator = createRandomGeneratorFromString(input);

    // Generate a random background
    int level = randomNumber(generator, m_private->faceBackgroundMinimumGreyscaleLevel, m_private->faceBackgroundMaximumGreyscaleLevel);
    setFaceBackgroundColor(m_private->faceDocument, {level, level, level});

    // Create our output image we will draw on
    QImage image{imageSize, QImage::Format_ARGB32};
    image.fill(Qt::transparent);

    {
        QPainter painter(&image);

        {
            // Render the face
            QSvgRenderer renderer(m_private->faceDocument.toByteArray());
            renderer.render(&painter, percentToRect(imageSize, m_private->facePosition, m_private->faceSize));
        }

        if(randomNumber(generator, 0, 99) > m_private->bowtiePercent)
        {
            // Render a bowtie
            QSvgRenderer &renderer = randomListElement(generator, m_private->bowtieRenderers);
            renderer.render(&painter, percentToRect(imageSize, m_private->bowtiePosition, m_private->bowtieSize));
        }

        if(randomNumber(generator, 0, 99) > m_private->glassesPercent)
        {
            // Render glasses
            QSvgRenderer &renderer = randomListElement(generator, m_private->glassesRenderers);
            renderer.render(&painter, percentToRect(imageSize, m_private->glassesPosition, m_private->glassesSize));
        }

        if(randomNumber(generator, 0, 99) > m_private->hatPercent)
        {
            // Render a hat
            QSvgRenderer &renderer = randomListElement(generator, m_private->hatRenderers);
            renderer.render(&painter, percentToRect(imageSize, m_private->hatPosition, m_private->hatSize));
        }
    }

    return image;
}
}
}

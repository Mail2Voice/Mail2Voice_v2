#pragma once

#include <QDomDocument>
#include <QSvgRenderer>

#include <memory>
#include <vector>

namespace m2v
{
namespace gui
{
using RendererList = std::vector<std::unique_ptr<QSvgRenderer>>;

struct AvatarGeneratorPrivate
{
    static constexpr int faceBackgroundMinimumGreyscaleLevel{180};
    static constexpr int faceBackgroundMaximumGreyscaleLevel{255};
    static constexpr int bowtiePercent{60};
    static constexpr int glassesPercent{40};
    static constexpr int hatPercent{30};
    const QPointF facePosition{0.22, 0.39};
    const QSizeF faceSize{0.56, 0.56};
    const QPointF bowtiePosition{0.3, 0.86};
    const QSizeF bowtieSize{0.4, 0.14};
    const QPointF glassesPosition{0.3, 0.5};
    const QSizeF glassesSize{0.4, 0.16};
    const QPointF hatPosition{0.22, 0.09};
    const QSizeF hatSize{0.56, 0.47};

    QDomDocument faceDocument;
    RendererList bowtieRenderers;
    RendererList glassesRenderers;
    RendererList hatRenderers;
};
}
}

// Returns true if actualVersion is lower than expectedVersion
function isVersionLowerThan(actualVersion, expectedVersion)
{
    var actualVersionParts = actualVersion.split('.').map(function(item)
    {
        return parseInt(item, 10);
    });

    var expectedVersionParts = expectedVersion.split('.').map(function(item)
    {
        return parseInt(item, 10);
    });

    for (var i = 0; i < expectedVersionParts.length; ++i)
    {
        if (actualVersionParts[i] > expectedVersionParts[i])
            return false;

        if (actualVersionParts[i] < expectedVersionParts[i])
            return true;
    }

    return !(i === expectedVersionParts.length || expectedVersionParts[i] === 0);
}

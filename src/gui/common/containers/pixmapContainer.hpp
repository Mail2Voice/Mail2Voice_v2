#pragma once

#include "resourceContainer.hpp"

#include <QPixmap>

namespace m2v
{
namespace gui
{
/**
 * @brief The PixmapContainer class A resource container for QPixmaps
 */
class PixmapContainer final : public ResourceContainer<QPixmap>
{
public:
    /**
     * @brief PixmapContainer Constructor
     * @param resourceName The name of the pixmap type, e.g. "contact pictures"
     * @param defaultResource A default pixmap to be returned when the requested pixmap does not exist
     */
    PixmapContainer(const QString &resourceName, const QPixmap &defaultResource);
    ~PixmapContainer();

    /**
     * @brief store Store a pixmap
     * @param resource The pixmap to store
     * @return A resource id that can be used to retrieve the pixmap later
     */
    ResourceId store(const QPixmap &resource);

    /**
     * @brief retrieve Retrieve a stored pixmap
     * @param id The pixmap's resource id
     * @return The requested pixmap, or the default pixmap if the requested pixmap is not stored
     */
    QPixmap retrieve(ResourceId id) const;
};
}
}

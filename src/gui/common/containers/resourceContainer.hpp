#pragma once

#include <QString>

#include "baseContainer.hpp"

namespace m2v
{
namespace gui
{
/**
 * @brief The ResourceContainer class Templated class used to represent a resource container
 * @tparam T Resource type
 */
template<typename T>
class ResourceContainer : public BaseContainer
{
public:
    /**
     * @brief ResourceContainer Constructor
     * @param resourceName The name of the resource type, e.g. "contact pictures"
     * @param defaultResource A default resource to be returned when the requested resource does not exist
     */
    ResourceContainer(const QString &resourceName, const T &defaultResource):
        BaseContainer(resourceName),
        m_defaultResource(defaultResource)
    {
    }

    virtual ~ResourceContainer() = default;

    ResourceContainer(const ResourceContainer &) = delete;
    ResourceContainer &operator=(const ResourceContainer &) = delete;

protected:
    const T &defaultResource() const { return m_defaultResource; }

private:
    T m_defaultResource;
};
}
}

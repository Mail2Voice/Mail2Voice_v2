#pragma once

#include <QString>

class QByteArray;
class QDir;

namespace m2v
{
namespace gui
{
/**
 * @brief The BaseContainer class Abstract non-templated class used to represent a resource container
 */
class BaseContainer
{
public:
    /**
     * The resource id type ; an unsigned 64 bit integer should be enough since we never reset it
     */
    using ResourceId = quint64;

    /**
     * An invalid resource id, used to represent a non-existing resource
     */
    static const ResourceId InvalidResourceId{0};

    /**
     * The first, default resource id
     */
    static const ResourceId DefaultResourceId{InvalidResourceId + 1};

    /**
     * @brief BaseContainer Constructor
     * @param resourceName The name of the resource type, e.g. "contact pictures"
     */
    explicit BaseContainer(const QString &resourceName);

    /**
     * @brief ~BaseContainer Destructor
     */
    virtual ~BaseContainer();

    BaseContainer(const BaseContainer &) = delete;
    BaseContainer &operator=(const BaseContainer &) = delete;

    /**
     * @brief hasResource Returns true if a resource is stored
     * @param id The resource id
     * @return True if the resource is stored
     */
    bool hasResource(ResourceId id) const;

protected:
    ResourceId store(const QByteArray &resource);
    QByteArray retrieve(ResourceId id) const;

private:
    QDir getDataDirectory() const;

    QString m_resourceName;
    ResourceId m_lastAssignedResourceId{DefaultResourceId};
};
}
}

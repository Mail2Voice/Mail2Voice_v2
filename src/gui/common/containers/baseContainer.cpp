#include "baseContainer.hpp"

#include <QByteArray>
#include <QDir>
#include <QFile>
#include <QSettings>
#include <QStandardPaths>

namespace m2v
{
namespace gui
{
BaseContainer::BaseContainer(const QString &resourceName):
    m_resourceName(resourceName)
{
    QSettings settings;
    bool ok = true;

    // Fetch the last assigned resource id
    m_lastAssignedResourceId = settings.value("resources/" + m_resourceName + "/lastId", DefaultResourceId).toULongLong(&ok);

    if(!ok)
    {
        m_lastAssignedResourceId = DefaultResourceId;
    }
}

BaseContainer::~BaseContainer()
{
    QSettings settings;

    // Store the last assigned resource id
    settings.setValue("resources/" + m_resourceName + "/lastId", m_lastAssignedResourceId);
}

bool BaseContainer::hasResource(ResourceId id) const
{
    // Check if the resource file exists
    auto dataDirectory = getDataDirectory();
    auto filepath = dataDirectory.filePath(QString::number(id));

    QFile file(filepath);

    return file.exists();
}

BaseContainer::ResourceId BaseContainer::store(const QByteArray &resource)
{
    // Get a new id
    auto id = m_lastAssignedResourceId++;
    auto dataDirectory = getDataDirectory();

    // Create the storage path if needed
    if(!dataDirectory.exists())
    {
        if(!dataDirectory.mkpath(dataDirectory.path()))
        {
            return InvalidResourceId;
        }
    }

    // Create the resource file and store the resource data
    auto filepath = dataDirectory.filePath(QString::number(id));

    QFile file(filepath);

    if(!file.open(QIODevice::WriteOnly))
    {
        return InvalidResourceId;
    }

    if(file.write(resource) == -1)
    {
        file.remove();

        return InvalidResourceId;
    }

    return id;
}

QByteArray BaseContainer::retrieve(ResourceId id) const
{
    // Fetch the resource file data if it exists, otherwise return an invalid (default) QByteArray
    auto dataDirectory = getDataDirectory();
    auto filepath = dataDirectory.filePath(QString::number(id));

    QFile file(filepath);

    if(!file.exists())
    {
        return {};
    }

    if(!file.open(QIODevice::ReadOnly))
    {
        return {};
    }

    return file.readAll();
}

QDir BaseContainer::getDataDirectory() const
{
    return {QStandardPaths::writableLocation(QStandardPaths::StandardLocation::AppDataLocation)
            + QDir::separator()
            + m_resourceName};
}
}
}

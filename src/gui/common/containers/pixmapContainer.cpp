#include "pixmapContainer.hpp"

#include <QBuffer>

namespace m2v
{
namespace gui
{
PixmapContainer::PixmapContainer(const QString &resourceName, const QPixmap &defaultResource):
    ResourceContainer<QPixmap>(resourceName, defaultResource)
{
}

PixmapContainer::~PixmapContainer()
{
}

BaseContainer::ResourceId PixmapContainer::store(const QPixmap &resource)
{
    QByteArray byteArray;
    QBuffer buffer{&byteArray};

    if(!buffer.open(QIODevice::WriteOnly))
    {
        return InvalidResourceId;
    }

    if(!resource.save(&buffer, "PNG"))
    {
        return InvalidResourceId;
    }

    return BaseContainer::store(byteArray);
}

QPixmap PixmapContainer::retrieve(BaseContainer::ResourceId id) const
{
    auto byteArray = BaseContainer::retrieve(id);

    if(byteArray.isNull())
    {
        return defaultResource();
    }

    QPixmap pixmap;

    if(!pixmap.loadFromData(byteArray))
    {
        return defaultResource();
    }

    return pixmap;
}
}
}

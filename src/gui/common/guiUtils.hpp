#pragma once

#include <QColor>
#include <QPixmap>

class QPushButton;

namespace m2v
{
namespace gui
{

namespace utils
{
    /**
     * Namespace containing all CSS class names
     */
    namespace css
    {
        const QString container1 {"container1"};
        const QString container2 {"container2"};
        const QString normalButton {"normalButton"};
        const QString warningButton {"warningButton"};
        const QString emailWidget {"emailWidget"};
        const QString contactWidget {"contactWidget"};
        const QString attachmentWidget {"attachmentWidget"};
    }

    /**
     * @brief Colorize given pixmap with given color (assuming pixmap icon is flat and black).
     * @param pixmap The pixmap to colorize.
     * @param color Color to use.
     * @return The colorized pixmap.
     */
    QPixmap &colorize(QPixmap &pixmap, const QColor &color);

    /**
     * @brief Set CSS class and icon for given button.
     * @param button The button to customize.
     * @param cssClass CSS class name.
     * @param iconPath Icon path.
     * @param iconColor Color to use for the icon, white by default.
     */
    void setButtonStyle(QPushButton *button,
                        const QString &cssClass,
                        const QString &iconPath,
                        const QColor &iconColor = Qt::white);
}

}
}

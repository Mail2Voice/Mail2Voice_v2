#pragma once

#include "gui/common/factories/abstractWidgetFactory.h"

#include <memory>
#include <QVariant>
#include <QWidget>

namespace m2v
{
namespace gui
{

/**
 * @brief Tamplate widget factory that can produce SelectableWidget of type WidgetType from data of type DatumType.
 */
template <class WidgetType, class DatumType>
class ListWidgetFactory : public AbstractWidgetFactory
{
public:
    ListWidgetFactory() = default;

    /**
     * @copydoc AbstractWidgetFactory::createWidget
     */
    SelectableWidget *createWidget(const QVariant &data);
};

template<class WidgetType, class DatumType>
SelectableWidget *ListWidgetFactory<WidgetType, DatumType>::createWidget(const QVariant &data)
{
    std::shared_ptr<DatumType> convertedDatum = data.value<std::shared_ptr<DatumType>>();
    if(convertedDatum)
    {
        return new WidgetType(convertedDatum);
    }

    return nullptr;
}

}
}

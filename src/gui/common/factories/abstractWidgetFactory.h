#pragma once

#include <QWidget>

#include "gui/common/widgets/selectableWidget.hpp"

namespace m2v
{
namespace gui
{

/**
 * @brief Abstract factory designed to produce selectable widgets.
 */
class AbstractWidgetFactory
{
public:
    AbstractWidgetFactory() = default;

    virtual ~AbstractWidgetFactory() = default;

    /**
     * @brief Creates a selectable widget from given data.
     * @warning The factory doesn't hold any pointer to the created widget.
     * @param data Contains information to fill the created widget.
     * @return The created SelectableWidget or nullptr if creation failed.
     */
    virtual SelectableWidget *createWidget(const QVariant &data) = 0;
};

}
}

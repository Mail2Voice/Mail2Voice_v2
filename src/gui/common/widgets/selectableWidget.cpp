#include "selectableWidget.hpp"

#include <QLabel>
#include <QStyle>
#include <QFocusEvent>
#include <QDebug>

namespace m2v
{
namespace gui
{

SelectableWidget::SelectableWidget(const QString &cssClass, QWidget *parent):
    StylableWidget(cssClass, parent)
{
}

SelectableWidget::~SelectableWidget()
{
}

void SelectableWidget::setSelected(bool selected)
{
    m_selected = selected;

    // Trick to get css style applied
    style()->unpolish(this);
    style()->polish(this);

    for(QObject *child : children())
    {
        QWidget *childWidget = qobject_cast<QWidget *>(child);

        if(childWidget)
        {
            childWidget->style()->unpolish(childWidget);
            childWidget->style()->polish(childWidget);
        }
    }
}

void SelectableWidget::focusInEvent(QFocusEvent *event)
{
    if(event->reason() == Qt::TabFocusReason)
    {
        // TODO: workaround to avoid widget selection on false focus event raised
        return;
    }

    QWidget::focusInEvent(event);
    emit selected();
}

void SelectableWidget::focusOutEvent(QFocusEvent *event)
{
    QWidget::focusOutEvent(event);
}

}
}

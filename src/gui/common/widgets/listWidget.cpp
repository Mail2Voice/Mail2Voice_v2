#include "listWidget.hpp"

#include <QDebug>
#include <QHBoxLayout>
#include <QScrollBar>
#include <QVBoxLayout>

#include "gui/common/factories/abstractWidgetFactory.h"
#include "gui/common/guiUtils.hpp"
#include "gui/common/layouts/flowLayout.hpp"

namespace m2v
{
namespace gui
{

ListWidget::ListWidget(QWidget *parent) :
    StylableWidget("listWidget", parent),
    m_buttonPrevious(std::make_unique<QPushButton>()),
    m_buttonNext(std::make_unique<QPushButton>()),
    m_scrollArea(std::make_unique<QScrollArea>())
{
    m_buttonPrevious->setAccessibleName(tr("Previous"));
    m_buttonPrevious->setEnabled(false);

    m_buttonNext->setAccessibleName(tr("Next"));
    m_buttonNext->setEnabled(false);


    connect(m_buttonPrevious.get(), &QPushButton::clicked,
            this, &ListWidget::onPreviousButtonClicked);

    connect(m_buttonNext.get(), &QPushButton::clicked,
            this, &ListWidget::onNextButtonClicked);

    m_scrollArea->setWidgetResizable(true);
    m_scrollArea->setStyleSheet("QScrollBar:vertical { min-width: 20px; }");
    m_scrollArea->setFrameStyle(QFrame::NoFrame);

    setOrientation(Qt::Vertical);
}

void ListWidget::setOrientation(Qt::Orientation orientation, bool useFlowLayout)
{
    if(useFlowLayout && orientation == Qt::Horizontal)
    {
        qCritical() << "The use of flow layout with an horizontal orientation is not supported";
        return;
    }

    // First, clear current layout if any
    delete layout();

    if(m_scrollbarToListen)
    {
        disconnect(m_scrollbarToListen, &QScrollBar::rangeChanged,
                   this, &ListWidget::onScrollBarRangeChanged);

        disconnect(m_scrollbarToListen, &QScrollBar::valueChanged,
                  this, &ListWidget::onScrollBarValueChanged);
    }

    //Then create new layout
    QBoxLayout *customLayout;
    QLayout* listLayout;
    QBoxLayout *buttonPreviousLayout;
    QBoxLayout *buttonNextLayout;
    QSpacerItem *spacer;

    if(orientation == Qt::Vertical)
    {
        customLayout = new QVBoxLayout();
        customLayout->setContentsMargins(2, 2, 2, 2);
        customLayout->setSpacing(0);

        if(useFlowLayout)
        {
            listLayout = new FlowLayout();
        }
        else
        {
            listLayout = new QVBoxLayout();
        }

        buttonPreviousLayout = new QHBoxLayout();
        buttonNextLayout = new QHBoxLayout();

        utils::setButtonStyle(m_buttonPrevious.get(), utils::css::normalButton, ":/resources/icons/arrow-up.svg");
        m_buttonPrevious->setMaximumHeight(ButtonThicknessMax);
        m_buttonPrevious->setMaximumWidth(ButtonLengthMax);
        m_buttonPrevious->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        utils::setButtonStyle(m_buttonNext.get(), utils::css::normalButton, ":/resources/icons/arrow-down.svg");
        m_buttonNext->setMaximumHeight(ButtonThicknessMax);
        m_buttonNext->setMaximumWidth(ButtonLengthMax);
        m_buttonNext->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        spacer = new QSpacerItem(0, 10, QSizePolicy::Expanding, QSizePolicy::Expanding);

        m_scrollbarToListen = m_scrollArea->verticalScrollBar();
    }
    else
    {
        customLayout = new QHBoxLayout();
        customLayout->setContentsMargins(2, 2, 2, 2);
        customLayout->setSpacing(4);

        listLayout = new QHBoxLayout();
        buttonPreviousLayout = new QVBoxLayout();
        buttonNextLayout = new QVBoxLayout();

        utils::setButtonStyle(m_buttonPrevious.get(), utils::css::normalButton, ":/resources/icons/arrow-left.svg");
        m_buttonPrevious->setMaximumHeight(ButtonLengthMax);
        m_buttonPrevious->setMaximumWidth(ButtonThicknessMax);
        m_buttonPrevious->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        utils::setButtonStyle(m_buttonNext.get(), utils::css::normalButton, ":/resources/icons/arrow-right.svg");
        m_buttonNext->setMaximumHeight(ButtonLengthMax);
        m_buttonNext->setMaximumWidth(ButtonThicknessMax);
        m_buttonNext->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        spacer = new QSpacerItem(10, 0, QSizePolicy::Expanding, QSizePolicy::Expanding);

        m_scrollbarToListen = m_scrollArea->horizontalScrollBar();
    }

    StylableWidget *listWidget = new StylableWidget(utils::css::container1);
    listLayout->setSpacing(8);
    listLayout->setContentsMargins(4, 4, 4, 4);

    if(!useFlowLayout)
    {
        static_cast<QBoxLayout *>(listLayout)->insertSpacerItem(0, spacer);
    }

    listWidget->setLayout(listLayout);

    m_scrollArea->setWidget(listWidget);

    setLayout(customLayout);

    buttonPreviousLayout->addWidget(m_buttonPrevious.get());
    buttonNextLayout->addWidget(m_buttonNext.get());

    customLayout->addLayout(buttonPreviousLayout);
    customLayout->addWidget(m_scrollArea.get());
    customLayout->addLayout(buttonNextLayout);

    connect(m_scrollbarToListen, &QScrollBar::rangeChanged,
            this, &ListWidget::onScrollBarRangeChanged);

    connect(m_scrollbarToListen, &QScrollBar::valueChanged,
            this, &ListWidget::onScrollBarValueChanged);
}

void ListWidget::setModel(std::shared_ptr<QAbstractListModel> model)
{
    if(!model)
    {
        qCritical() << "Failed to set model because model is null";
        return;
    }

    m_model = model;

    connect(model.get(), &QAbstractListModel::dataChanged,
            this, &ListWidget::onDataChanged);

    connect(model.get(), &QAbstractListModel::layoutAboutToBeChanged,
            this, &ListWidget::onLayoutAboutToBeChanged);

    connect(model.get(), &QAbstractListModel::layoutChanged,
            this, &ListWidget::onLayoutChanged);

    connect(model.get(), &QAbstractListModel::modelAboutToBeReset,
            this, &ListWidget::onModelAboutToBeReset);

    connect(model.get(), &QAbstractListModel::modelReset,
            this, &ListWidget::onModelReset);

    connect(model.get(), &QAbstractListModel::rowsAboutToBeInserted,
            this, &ListWidget::onRowsAboutToBeInserted);

    connect(model.get(), &QAbstractListModel::rowsAboutToBeMoved,
            this, &ListWidget::onRowsAboutToBeMoved);

    connect(model.get(), &QAbstractListModel::rowsAboutToBeRemoved,
            this, &ListWidget::onRowsAboutToBeRemoved);

    connect(model.get(), &QAbstractListModel::rowsInserted,
            this, &ListWidget::onRowsInserted);

    connect(model.get(), &QAbstractListModel::rowsMoved,
            this, &ListWidget::onRowsMoved);

    connect(model.get(), &QAbstractListModel::rowsRemoved,
            this, &ListWidget::onRowsRemoved);
}

void ListWidget::setListWidgetFactory(const std::shared_ptr<m2v::gui::AbstractWidgetFactory> &listWidgetFactory)
{
    m_listWidgetFactory = listWidgetFactory;
}

void ListWidget::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    // TODO
}

void ListWidget::onLayoutAboutToBeChanged(const QList<QPersistentModelIndex> &parents, QAbstractItemModel::LayoutChangeHint hint)
{
    // TODO
}

void ListWidget::onLayoutChanged(const QList<QPersistentModelIndex> &parents, QAbstractItemModel::LayoutChangeHint hint)
{
    // TODO
}

void ListWidget::onModelAboutToBeReset()
{
    // TODO
}

void ListWidget::onModelReset()
{
    // TODO
}

void ListWidget::onRowsAboutToBeInserted(const QModelIndex &parent, int first, int last)
{
    // TODO
}

void ListWidget::onRowsAboutToBeMoved(const QModelIndex &sourceParent, int sourceStart, int sourceEnd, const QModelIndex &destinationParent, int destinationRow)
{
    // TODO
}

void ListWidget::onRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    // TODO
}

void ListWidget::onRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent)

    QBoxLayout *layout = qobject_cast<QBoxLayout *>(m_scrollArea->widget()->layout());
    FlowLayout *flowLayout {nullptr};

    if(!layout)
    {
        flowLayout = static_cast<FlowLayout *>(m_scrollArea->widget()->layout());
    }

    for(int i = first; i <= last; ++i)
    {
        QVariant data = m_model->data(m_model->index(i, 0));
        SelectableWidget *widgetToInsert = m_listWidgetFactory->createWidget(data);

        if(widgetToInsert)
        {
            if(layout)
            {
                layout->insertWidget(i, widgetToInsert);
                connect(widgetToInsert, &SelectableWidget::selected,
                        this, &ListWidget::onItemSelected);
            }
            else
            {
                flowLayout->addWidget(widgetToInsert);
                connect(widgetToInsert, &SelectableWidget::selected,
                        this, &ListWidget::onItemSelected);
            }
        }
    }
}

void ListWidget::onRowsMoved(const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row)
{
    // TODO
}

void ListWidget::onRowsRemoved(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent)

    for(int i = first; i <= last; ++i)
    {
        QLayoutItem *child = m_scrollArea->widget()->layout()->itemAt(first);
        m_scrollArea->widget()->layout()->removeItem(child);
        delete child->widget();
        delete child;
    }
}

void ListWidget::onScrollBarRangeChanged(int min, int max)
{
    if(m_previousRangeMin == min && m_previousRangeMax == max)
    {
        return;
    }

    m_previousRangeMin = min;
    m_previousRangeMax = max;

    const int value = m_scrollbarToListen->value();

    onScrollBarValueChanged(value);
}

void ListWidget::onScrollBarValueChanged(int value)
{
    m_buttonPrevious->setEnabled(value != m_previousRangeMin);
    m_buttonNext->setEnabled(value != m_previousRangeMax);
}

void ListWidget::onPreviousButtonClicked()
{
    // TODO: Find a smarter way of defining the scroll delta
    int newValue = m_scrollbarToListen->value() - 20;
    m_scrollbarToListen->setValue(newValue);
}

void ListWidget::onNextButtonClicked()
{
    // TODO: Workaround to avoid email unselection when used next button up to the end of the list (only with horizontal list)
    int newValue = m_scrollbarToListen->value() + 20;
    m_scrollbarToListen->setValue(newValue);
}

void ListWidget::onItemSelected()
{
    m2v::gui::SelectableWidget *selectedWidget = qobject_cast<m2v::gui::SelectableWidget *>(sender());

    if(selectedWidget)
    {
        if(m_lastSelectedWidget)
        {
            m_lastSelectedWidget->setSelected(false);
        }

        m_lastSelectedWidget = selectedWidget;
        m_lastSelectedWidget->setSelected(true);

        int widgetIndex = m_scrollArea->widget()->layout()->indexOf(selectedWidget);
        emit dataSelected(selectedWidget, m_model->data(m_model->index(widgetIndex)));
    }
}

}
}

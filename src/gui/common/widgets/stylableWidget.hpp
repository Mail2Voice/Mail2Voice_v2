#pragma once

#include <QWidget>

#include <QObject>

namespace m2v
{
namespace gui
{

/** @brief Widget that applies CSS stylesheet properties. */
class StylableWidget : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString class READ getCssClass WRITE setCssClass)

public:

    explicit StylableWidget(QWidget *parent = nullptr);

    /**
     * @brief Constructor.
     * @param parent The parent widget.
     */
    explicit StylableWidget(const QString& cssClass, QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    virtual ~StylableWidget();

    /**
     * @brief Returns the CSS class name of the widget.
     */
    QString getCssClass() const { return m_cssClass; }

    /**
     * @brief Sets the CSS class name of the widget.
     */
    void setCssClass(const QString& cssClass) { m_cssClass = cssClass; }

protected:
    /**
     * @brief Overrides paintEvent method to apply CSS properties.
     * @param event The raised paint event.
     */
    virtual void paintEvent(QPaintEvent *event) override;

protected:
    QString m_cssClass;
};

}
}

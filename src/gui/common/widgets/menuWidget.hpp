#pragma once

#include "gui/common/widgets/stylableWidget.hpp"

#include <memory>

namespace Ui
{
class MenuWidget;
}

namespace m2v
{
namespace gui
{

enum class View
{
    Write,
    Drafts,
    Inbox,
    Sent,
    Trash,
    Junk,
    Contacts,
    Settings
};

/**
 * @brief Signals the application for view change requests issued by user.
 */
class MenuWidget : public StylableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit MenuWidget(QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~MenuWidget();

signals:
    /**
     * @brief Signal emitted when the user clicked on a view button.
     * @param view The requested view to display.
     */
    void changeViewRequest(View view);

    /**
     * @brief Signal emitted when the user clicked on the exit button.
     */
    void exitApplicationRequest();

    void refreshEMailList();

private:
    std::unique_ptr<Ui::MenuWidget> m_ui;
};

}
}

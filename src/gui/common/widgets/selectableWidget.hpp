#pragma once

#include "gui/common/widgets/stylableWidget.hpp"

#include <QWidget>

class QLabel;

namespace m2v
{
namespace gui
{

/**
 * @brief A SelectableWidget can be highlighted by user selection.
 */
class SelectableWidget : public StylableWidget
{
    Q_OBJECT

    Q_PROPERTY(bool selected READ isSelected WRITE setSelected)

public:
    /**
     * @brief Constructor.
     * @param parent The parent widget.
     */
    explicit SelectableWidget(const QString &cssClass, QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    virtual ~SelectableWidget();

    /**
     * @brief Returns true if the widget is selected, false otherwise.
     */
    bool isSelected() const { return m_selected; }

    /**
     * @brief Sets whether or not the widget is selected.
     * @param selected Toggle to switch betwwen selected and unselected state.
     */
    void setSelected(bool selected);

signals:
    /**
     * @brief Signals emitted when the widget is selected.
     */
    void selected();

protected:
    /**
     * @copydoc QWidget::focusInEvent
     */
    void focusInEvent(QFocusEvent *event) override;

    /**
     * @copydoc QWidget::focusOutEvent
     */
    void focusOutEvent(QFocusEvent *event) override;

private:
    bool m_selected{false};
};
}
}

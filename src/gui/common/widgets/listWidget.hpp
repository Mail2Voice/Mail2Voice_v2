#pragma once

#include "gui/common/widgets/stylableWidget.hpp"

#include <memory>
#include <QAbstractListModel>
#include <QPushButton>
#include <QScrollArea>
#include <QSpacerItem>

#include "gui/common/widgets/selectableWidget.hpp"

class QAbstractListModel;

namespace m2v
{
namespace gui
{

class AbstractWidgetFactory;

/**
 * @brief This class manages a list of selectable widgets in a list view. It can arrange
 *        the items vertically, horizontally or in a "flow" layout.
 *        In addition to the scrollbar, two buttons (next and previous) are added automatically
 *        and provide user two controls to navigate through the list.
 */
class ListWidget : public StylableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor
     * @param parent Optional parent object.
     */
    explicit ListWidget(QWidget *parent = nullptr);

    ListWidget(ListWidget *listWidget) = delete;

    ~ListWidget() = default;

    /**
     * @brief Sets the layout orientation.
     * @param orientation Horizontal or vertical
     * @param useFlowLayout If set to true, the layout will be a flow layout (works only for vertical orientation)
     */
    void setOrientation(Qt::Orientation orientation, bool useFlowLayout = false);

    /**
     * @brief Sets the internal data model.
     * @param model The list model.
     */
    void setModel(std::shared_ptr<QAbstractListModel> model);

    /**
     * @brief Sets the widget factory to use for widgets creation.
     * @param listWidgetFactory The widget factory.
     */
    void setListWidgetFactory(const std::shared_ptr<m2v::gui::AbstractWidgetFactory> &listWidgetFactory);

signals:
    /**
     * @brief Signal emitted when the user selects an item.
     * @param widget The selected widget.
     * @param data The corresponding selected data.
     */
    void dataSelected(m2v::gui::SelectableWidget *widget, const QVariant &data);

private slots:
    /**
     * @see QAbstractListModel::dataChanged
     */
    void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = {});

    /**
     * @see QAbstractListModel::layoutAboutToBeChanged
     */
    void onLayoutAboutToBeChanged(const QList<QPersistentModelIndex> &parents = {},
                                  QAbstractItemModel::LayoutChangeHint hint = QAbstractItemModel::NoLayoutChangeHint);

    /**
     * @see QAbstractListModel::layoutChanged
     */
    void onLayoutChanged(const QList<QPersistentModelIndex> &parents = {},
                         QAbstractItemModel::LayoutChangeHint hint = QAbstractItemModel::NoLayoutChangeHint);

    /**
     * @see QAbstractListModel::modelAboutToBeReset
     */
    void onModelAboutToBeReset();

    /**
     * @see QAbstractListModel::modelReset
     */
    void onModelReset();

    /**
     * @see QAbstractListModel::rowsAboutToBeInserted
     */
    void onRowsAboutToBeInserted(const QModelIndex &parent, int first, int last);

    /**
     * @see QAbstractListModel::rowsAboutToBeMoved
     */
    void onRowsAboutToBeMoved(const QModelIndex &sourceParent, int sourceStart, int sourceEnd, const QModelIndex &destinationParent, int destinationRow);

    /**
     * @see QAbstractListModel::rowsAboutToBeRemoved
     */
    void onRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);

    /**
     * @see QAbstractListModel::rowsInserted
     */
    void onRowsInserted(const QModelIndex &parent, int first, int last);

    /**
     * @see QAbstractListModel::rowsMoved
     */
    void onRowsMoved(const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    /**
     * @see QAbstractListModel::rowsRemoved
     */
    void onRowsRemoved(const QModelIndex &parent, int first, int last);

    /**
     * @see QScrollBar::rangeChanged
     */
    void onScrollBarRangeChanged(int min, int max);

    /**
     * @see QScrollBar::valueChanged
     */
    void onScrollBarValueChanged(int value);

    /**
     * @brief Slot called when the user clicks on the previous button.
     */
    void onPreviousButtonClicked();

    /**
     * @brief Slot called when the user clicks on the next button.
     */
    void onNextButtonClicked();

    /**
     * @brief Slot called when the user select an item.
     */
    void onItemSelected();

private:

    static constexpr int ButtonThicknessMax {30};
    static constexpr int ButtonLengthMax {200};

private:
    std::unique_ptr<QPushButton> m_buttonPrevious;
    std::unique_ptr<QPushButton> m_buttonNext;
    std::unique_ptr<QScrollArea> m_scrollArea;
    std::shared_ptr<QAbstractListModel> m_model;
    std::shared_ptr<AbstractWidgetFactory> m_listWidgetFactory;
    QScrollBar *m_scrollbarToListen{nullptr};
    m2v::gui::SelectableWidget *m_lastSelectedWidget{nullptr};

    int m_previousRangeMin {0};
    int m_previousRangeMax {0};
};

}
}

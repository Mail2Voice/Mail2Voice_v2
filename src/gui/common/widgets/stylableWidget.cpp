#include "stylableWidget.hpp"

#include <QPainter>
#include <QStyleOption>

namespace m2v
{
namespace gui
{

StylableWidget::StylableWidget(QWidget *parent) :
    QWidget(parent)
{
}

StylableWidget::StylableWidget(const QString &cssClass, QWidget *parent) :
    QWidget(parent),
    m_cssClass(cssClass)
{
}

StylableWidget::~StylableWidget()
{
}

void StylableWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    // Apply CSS style
    QStyleOption option;
    option.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &option, &painter, this);
}

}
}

#pragma once

#include <QWidget>

namespace m2v
{
namespace gui
{
namespace pages
{

/**
 * @brief Represents a view in the application.
 */
class PageWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit PageWidget(QWidget *parent = nullptr):
        QWidget(parent)
    {
    }

    /**
     * @brief Executes operations when the page is entered.
     */
    void pageEntered() { onPageEntered(); }

    /**
     * @brief Executes operations when the page is exited.
     */
    void pageExited() { onPageExited(); }

protected:
    /**
     * @brief Executes operations when the page is entered.
     */
    virtual void onPageEntered() {}

    /**
     * @brief Executes operations when the page is exited.
     */
    virtual void onPageExited() {}
};
}
}
}

#include "menuWidget.hpp"
#include "ui_menuWidget.h"

#include "gui/common/guiUtils.hpp"

namespace m2v
{
namespace gui
{

MenuWidget::MenuWidget(QWidget *parent) :
    StylableWidget("menuWidget", parent),
    m_ui(std::make_unique<Ui::MenuWidget>())
{
    m_ui->setupUi(this);

    utils::setButtonStyle(m_ui->buttonSend, utils::css::normalButton, ":/resources/icons/microphone.svg");
    utils::setButtonStyle(m_ui->buttonDrafts, utils::css::normalButton, ":/resources/icons/pencil-square-o.svg");
    utils::setButtonStyle(m_ui->buttonInbox, utils::css::normalButton, ":/resources/icons/inbox.svg");
    utils::setButtonStyle(m_ui->buttonSent, utils::css::normalButton, ":/resources/icons/share.svg");
    utils::setButtonStyle(m_ui->buttonTrash, utils::css::normalButton, ":/resources/icons/trash.svg");
    utils::setButtonStyle(m_ui->buttonAllContacts, utils::css::normalButton, ":/resources/icons/users.svg");
    utils::setButtonStyle(m_ui->buttonSettings, utils::css::normalButton, ":/resources/icons/cogs.svg");
    utils::setButtonStyle(m_ui->buttonQuit, utils::css::warningButton, ":/resources/icons/sign-out.svg");

    connect(m_ui->buttonInbox, &QPushButton::clicked,
            [this](){ emit changeViewRequest(View::Inbox); });

    connect(m_ui->buttonAllContacts, &QPushButton::clicked,
            [this](){ emit changeViewRequest(View::Contacts); });

    connect(m_ui->buttonSettings, &QPushButton::clicked,
            [this](){ emit changeViewRequest(View::Settings); });

    connect(m_ui->buttonQuit, &QPushButton::clicked,
            this, &MenuWidget::exitApplicationRequest);

    connect(m_ui->buttonRefresh, &QPushButton::clicked,
            this, &MenuWidget::refreshEMailList);
}

MenuWidget::~MenuWidget()
{
}

}
}

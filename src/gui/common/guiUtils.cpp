#include "guiUtils.hpp"

#include <QBitmap>
#include <QPushButton>
#include <QString>
#include <QVariant>

namespace m2v
{
namespace gui
{

QPixmap &utils::colorize(QPixmap &pixmap, const QColor &color)
{
    QBitmap mask = pixmap.createMaskFromColor(QColor("black"), Qt::MaskOutColor);
    pixmap.fill(color);
    pixmap.setMask(mask);
    return pixmap;
}

void utils::setButtonStyle(QPushButton *button,
                           const QString &cssClass,
                           const QString &iconPath,
                           const QColor &iconColor)
{
    button->setProperty("class", cssClass);
    QPixmap iconPixmap(iconPath);
    button->setIcon(utils::colorize(iconPixmap,iconColor));
}

}
}

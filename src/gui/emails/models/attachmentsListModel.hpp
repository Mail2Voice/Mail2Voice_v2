#pragma once

#include <QAbstractListModel>

#include <memory>


namespace m2v
{

namespace core
{
    class Attachment;
}


namespace gui
{

/**
 * @brief Model for attachments of an email.
 */
class AttachmentsListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent object.
     */
    explicit AttachmentsListModel(QObject *parent = nullptr);

    /**
     * @brief Loads a list of attachments in the model. Previous data is lost.
     * @param attachmentsToLoad The list of attachments to load.
     */
    void loadAttachments(const QList<std::shared_ptr<core::Attachment>> &attachmentsToLoad);

    /**
     * @brief Clear internal attachments list.
     */
    void clear();

    /**
     * @brief Returns data for given header location.
     * @param section Header section.
     * @param orientation Header orientation.
     * @param role Header data role requested.
     * @return Data for given header.
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    /**
     * @brief Returns the number of items in the model.
     * @param parent Parent index.
     * @return The number of items in the model.
     */
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    /**
     * @brief Fetch the data for given index and role.
     * @param index The model index of the requested data.
     * @param role The model role of the requested data.
     * @return The fetched data.
     */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    QList<std::shared_ptr<m2v::core::Attachment>> m_attachments;
};

}
}

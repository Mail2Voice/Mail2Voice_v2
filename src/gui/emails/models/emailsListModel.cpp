#include "emailsListModel.hpp"

#include "core/email.hpp"

namespace m2v
{
namespace gui
{

EmailsListModel::EmailsListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void EmailsListModel::appendEmails(const QVector<std::shared_ptr<core::Email>> &emails)
{
    beginInsertRows(QModelIndex(), m_emails.size(), m_emails.size() + emails.size() - 1);
    m_emails.append(emails);
    endInsertRows();
}

QVariant EmailsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    if(role == Qt::DisplayRole)
    {
        return QString(tr("Emails"));
    }

    return {};
}

int EmailsListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if(parent.isValid())
    {
        return 0;
    }

    return m_emails.size();
}

QVariant EmailsListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return {};
    }

    if(role == Qt::DisplayRole)
    {
        if(index.row() >= 0 && (index.row() < m_emails.size()))
        {
            return QVariant::fromValue(m_emails.at(index.row()));
        }
    }

    return {};
}

}
}

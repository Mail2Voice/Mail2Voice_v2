#include "attachmentsListModel.hpp"

#include "core/email.hpp"

namespace m2v
{
namespace gui
{

AttachmentsListModel::AttachmentsListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void AttachmentsListModel::loadAttachments(const QList<std::shared_ptr<core::Attachment>> &attachmentsToLoad)
{
    beginInsertRows(QModelIndex(), 0, attachmentsToLoad.size() - 1);
    m_attachments = attachmentsToLoad;
    endInsertRows();
}

void AttachmentsListModel::clear()
{
    if(!m_attachments.isEmpty())
    {
        beginRemoveRows(QModelIndex(), 0, m_attachments.size() - 1);
        m_attachments.clear();
        endRemoveRows();
    }
}

QVariant AttachmentsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    if(role == Qt::DisplayRole)
    {
        return QString(tr("Attachments"));
    }

    return {};
}

int AttachmentsListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if(parent.isValid())
    {
        return 0;
    }

    return m_attachments.size();
}

QVariant AttachmentsListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return {};
    }

    if(role == Qt::DisplayRole)
    {
        if(index.row() >= 0 && (index.row() < m_attachments.size()))
        {
            return QVariant::fromValue(m_attachments.at(index.row()));
        }
    }

    return {};
}

}
}

#pragma once

#include "gui/common/widgets/pageWidget.hpp"

#include "core/email.hpp"
#include "core/error/error.hpp"

#include <memory>
#include <QList>

namespace Ui
{
class EmailsPageWidget;
}

namespace m2v
{
namespace gui
{

class SelectableWidget;
class EmailsListModel;

namespace pages
{
class EmailsPageWidget : public PageWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit EmailsPageWidget(QWidget *parent = nullptr);

     /**
      * @brief Destructor.
      */
    ~EmailsPageWidget();

    void setEmailContent(const QString &content);

signals:
    void emailSelected(qint64 emailId);

public slots:
    void onEmailReceived(std::shared_ptr<core::Email> email);
    void onEmailContentReceived(const QString &emailContent);
    void onEmailContentFailed(const m2v::core::Error &error);


private slots:
    /**
     * @brief Slot called when an email has been selected.
     * @param selectedWidget The corresponding selected widget.
     * @param email The selected email data.
     */
    void onEmailSelected(m2v::gui::SelectableWidget *selectedWidget, const QVariant& email);

private:
    QVector<std::shared_ptr<core::Email>> generateFakeEmails();

private:
    std::unique_ptr<Ui::EmailsPageWidget> m_ui;
    std::shared_ptr<m2v::gui::EmailsListModel> m_model;
};
}
}
}

#include "emailsPageWidget.hpp"
#include "ui_emailsPageWidget.h"

#include "gui/common/factories/listWidgetFactory.h"
#include "gui/emails/models/emailsListModel.hpp"
#include "gui/emails/widgets/emailWidget.hpp"

#include <QDateTime>
#include <QScrollArea>

namespace m2v
{
namespace gui
{
namespace pages
{

EmailsPageWidget::EmailsPageWidget(QWidget *parent):
    PageWidget(parent),
    m_ui(std::make_unique<Ui::EmailsPageWidget>()),
    m_model(std::make_shared<EmailsListModel>())
{
    m_ui->setupUi(this);

    m_ui->widgetEmailsList->layout()->setContentsMargins(0, 0, 0, 0);
    m_ui->widgetEmailsList->setModel(m_model);

    std::shared_ptr<m2v::gui::AbstractWidgetFactory> listWidgetFactory =
            std::make_shared<m2v::gui::ListWidgetFactory<gui::EmailWidget, core::Email>>();
    m_ui->widgetEmailsList->setListWidgetFactory(listWidgetFactory);

    connect(m_ui->widgetEmailsList, &ListWidget::dataSelected,
            this, &EmailsPageWidget::onEmailSelected);

}

EmailsPageWidget::~EmailsPageWidget()
{
}

void EmailsPageWidget::setEmailContent(const QString &content)
{
    m_ui->widgetEmailViewerPanel->setContent(content);
}

void EmailsPageWidget::onEmailSelected(m2v::gui::SelectableWidget *selectedWidget, const QVariant &email)
{
    std::shared_ptr<m2v::core::Email> guiEmail = email.value<std::shared_ptr<m2v::core::Email>>();

    if(guiEmail)
    {
        m_ui->widgetEmailViewerPanel->setEmail(guiEmail);

        EmailWidget *emailWidget = qobject_cast<EmailWidget *>(selectedWidget);

        emit emailSelected(guiEmail->id());

        if(emailWidget)
        {
            emailWidget->onStatusChanged(true);
        }
    }
}

void EmailsPageWidget::onEmailReceived(std::shared_ptr<core::Email> email)
{
    m_model->appendEmails({email});
}

void EmailsPageWidget::onEmailContentReceived(const QString &emailContent)
{
    setEmailContent(emailContent);
}

void EmailsPageWidget::onEmailContentFailed(const m2v::core::Error &error)
{
    Q_UNUSED(error)
    setEmailContent(tr("Failed to load email content."));
}

QVector<std::shared_ptr<core::Email>> EmailsPageWidget::generateFakeEmails()
{
    QVector<std::shared_ptr<core::Email>> emails;

    QDateTime currentDateTime = QDateTime::currentDateTime();

    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(0);
        dateTime.setTime(QTime(9, 35, 37));

        email->setSubject(QString{"Déjeuner samedi midi ?"});
        email->setSenders({"Stéphanie R."});
        email->setFrom({"Stéphanie R."});
        email->setRecipients({"David Leseul", "Tris"});
        email->setDate(dateTime);
        email->setContent(QString{"Bonjour tout le monde,\n"
                                  "Seriez-vous disponible pour venir déjeuner à la maison samedi ?\n\n"
                                  "Stéphanie R."});
        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-1);
        dateTime.setTime(QTime(18, 17, 52));

        email->setSubject(QString{"Photos de la rando"});
        email->setSenders({"Gregoire Prisol"});
        email->setFrom({"Gregoire Prisol"});
        email->setRecipients({});
        email->setDate(dateTime);
        email->setContent(QString{"Coucou,\n\n"
                                  "Je t'envoie quelques photos de notre dernière rando.\n\n"
                                  "De quoi se remémorer tous nos efforts et la super récompense au bout !\n"
                                  "A bientôt !"});
        m2v::core::AttachmentList attachments;
        QStringList attachmentNames{"photo1.jpg",
                                    "photo2.jpg",
                                    "photo3.jpg",
                                    "photo4.jpg",
                                    "photo5.jpg",
                                    "photo6.jpg"};

        for(const QString &name : attachmentNames)
        {
            std::shared_ptr<m2v::core::Attachment> attachment = std::make_shared<m2v::core::Attachment>();
            attachment->setName(name);
            attachment->setMimetype("jpg");
            attachments << attachment;
        }
        email->setAttachments(attachments);
        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-4);
        dateTime.setTime(QTime(11, 5, 12));

        email->setSubject(QString{"FNUC - Commande n°73821"});
        email->setSenders({"service-client@fnuc.fr"});
        email->setFrom({"service-client@fnuc.fr"});
        //email->setRecipients();
        email->setDate(dateTime);
        email->setContent(QString{"Bonjour M. Dupont,\n"
                                  "Nous avons la plaisir de confirmer votre commande n°73821.\n\n"
                                  "Vous trouverez votre facture en pièce jointe.\n\n"
                                  "L'expédition se fera dans un délai maximum de 48h.\n\n"
                                  "La FNUC vous remercier pour votre confiance."});

        m2v::core::AttachmentList attachments;
        QStringList attachmentNames{"FACTURE_73821.pdf"};
        for(const QString &name : attachmentNames)
        {
            std::shared_ptr<m2v::core::Attachment> attachment = std::make_shared<m2v::core::Attachment>();
            attachment->setName(name);
            attachment->setMimetype("pdf");
            attachments << attachment;
        }
        email->setAttachments(attachments);

        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-4);
        dateTime.setTime(QTime(8, 46, 27));

        email->setSubject(QString{"Tournoi SIA Bridge 2018 - 17 - 18 mars"});
        email->setSenders({"Antoine Legrulain"});
        email->setFrom({"Antoine Legrulain"});
        email->setRecipients({"club-sia-bridge@wanadoo.fr"});
        email->setDate(dateTime);
        email->setContent(QString{"Bonjour à toutes et à tous,\n"
                                  "J'ai l'immense plaisir de vous annoncer la date du prochain tournoi.\n\n"
                                  "Il aura donc lieu les 17 et 18 mars 2018 à Brisson.\n"
                                  "Les inscriptions seront ouvertes dès le 1er décembre.\n"
                                  "Je vous invite à vous manifester au plus tôt afin de faciliter l'organisation !\n\n"
                                  "Très cordialement,\n"
                                  "Antoine"});
        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-5);
        dateTime.setTime(QTime(18, 55, 57));

        email->setSubject(QString{"rdv médecin"});
        email->setSenders({"Maman"});
        email->setFrom({"Maman"});
        //email->setRecipients();
        email->setDate(dateTime);
        email->setContent(QString{"Coucou,\n"
                                  "Je t'ai pris un rendez-vous chez le médecin jeudi prochain à 14h30.\n"
                                  "Bisous\n\n"
                                  "Maman"});
        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-5);
        dateTime.setTime(QTime(15, 2, 32));

        email->setSubject(QString{"Juste pour rire"});
        email->setSenders({"Stéphanie R."});
        email->setFrom({"Stéphanie R."});
        email->setRecipients({"David Leseul", "Tris"});
        email->setDate(dateTime);
        email->setContent(QString{"J'ai trouvé une petite vidéo rigolote : https://lolcat.org/hNzo8jfze223"});
        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-7);
        dateTime.setTime(QTime(13, 21, 43));

        email->setSubject(QString{"Sans objet"});
        email->setSenders({"David Leseul"});
        email->setFrom({"David Leseul"});
        //email->setRecipients();
        email->setDate(dateTime);
        email->setContent(QString{});

        m2v::core::AttachmentList attachments;
        QStringList attachmentNames{"PL0034.jpg"};
        for(const QString &name : attachmentNames)
        {
            std::shared_ptr<m2v::core::Attachment> attachment = std::make_shared<m2v::core::Attachment>();
            attachment->setName(name);
            attachment->setMimetype("jpg");
            attachments << attachment;
        }
        email->setAttachments(attachments);

        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-10);
        dateTime.setTime(QTime(15, 0, 2));

        email->setSubject(QString{"Chanson Francis Cabroul"});
        email->setSenders({"Tris"});
        email->setFrom({"Tris"});
        //email->setRecipients();
        email->setDate(dateTime);
        email->setContent(QString{"Salut,\n"
                                  "Tu connais la chanson de Francis Cabroul sur la cabane ? Je pensais l'utiliser pour l'anniversaie de Stéphanie. Tu en penses quoi ?"});
        emails << email;
    }
    {
        std::shared_ptr<core::Email> email = std::make_shared<m2v::core::Email>();
        QDateTime dateTime = currentDateTime.addDays(-10);
        dateTime.setTime(QTime(10, 54, 20));

        email->setSubject(QString{"Renard futé n°372"});
        email->setSenders({"Renard futé"});
        email->setFrom({"Renard futé"});
        //email->setRecipients();
        email->setDate(dateTime);
        email->setContent(QString{"Bonjour,\n"
                                  "Le nouveau numéro de Renard futé est disponible en kiosque. Bonne lecture !"});
        emails << email;
    }

    return emails;
}

}
}
}

#include "attachmentWidget.hpp"
#include "ui_attachmentWidget.h"

#include "core/attachment.hpp"
#include "gui/common/guiUtils.hpp"

namespace m2v
{
namespace gui
{

AttachmentWidget::AttachmentWidget(const std::shared_ptr<const core::Attachment> &attachment, QWidget *parent) :
    SelectableWidget(utils::css::attachmentWidget, parent),
    m_ui(std::make_unique<Ui::AttachmentWidget>()),
    m_attachment(attachment)
{
    m_ui->setupUi(this);

    m_ui->labelName->setText(m_attachment->name());
    QPixmap attachmentIcon(":/resources/icons/picture.svg");
    m_ui->labelcon->setPixmap(utils::colorize(attachmentIcon, QColor("white")).scaled(60, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

AttachmentWidget::~AttachmentWidget()
{
}

}
}

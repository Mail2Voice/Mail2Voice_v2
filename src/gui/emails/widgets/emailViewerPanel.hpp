#pragma once

#include "gui/common/widgets/stylableWidget.hpp"

#include <memory>

namespace Ui
{
    class EmailViewerPanel;
}

namespace m2v
{

namespace core
{
    class Email;
}

namespace gui
{

class AttachmentsListModel;


class EmailViewerPanel : public StylableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit EmailViewerPanel(QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~EmailViewerPanel();

    /**
     * @brief Sets the email to display.
     * @param email The email to display.
     */
    void setEmail(std::shared_ptr<const core::Email> email);

    /**
     * @brief Sets the content to display.
     * @param content The content to display.
     */
    void setContent(const QString &content);

    /**
     * @brief Clear all fields of the panel.
     */
    void clear();

private slots:
    /**
     * @brief Slot called when user clicks on the reply button/
     */
    void onButtonReplyClicked();

    /**
     * @brief Slot called when user clicks on the reply all button/
     */
    void onButtonReplyAllClicked();

    /**
     * @brief Slot called when user clicks on the forward button/
     */
    void onButtonForwardEmailClicked();

    /**
     * @brief Slot called when user clicks on the read button/
     */
    void onButtonReadEmailClicked();

    /**
     * @brief Slot called when user clicks on the delete button/
     */
    void onButtonDeleteEmailClicked();

private:
    std::unique_ptr<Ui::EmailViewerPanel> m_ui;
    std::shared_ptr<m2v::gui::AttachmentsListModel> m_attachmentsListModel;
};

}
}

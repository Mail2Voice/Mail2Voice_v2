#include "emailWidget.hpp"
#include "ui_emailWidget.h"

#include "gui/common/guiUtils.hpp"

namespace m2v
{
namespace gui
{
EmailWidget::EmailWidget(const std::shared_ptr<const core::Email> &email, QWidget *parent):
    SelectableWidget(utils::css::emailWidget, parent),
    m_email(email),
    m_ui(std::make_unique<Ui::EmailWidget>())
{
    m_ui->setupUi(this);

    m_ui->labelSenderName->setText(email->senders().join(", "));
    m_ui->labelSubject->setText(email->subject());
    m_ui->labelDateTime->setText(email->date().toString(Qt::DateFormat::DefaultLocaleShortDate));

    m_ui->labelAttachmentIcon->setVisible(!email->attachments().isEmpty());
    onStatusChanged(email->read());
}

EmailWidget::~EmailWidget()
{
}

void EmailWidget::onStatusChanged(bool read)
{
    if(read)
    {
        m_ui->labelReadStatusIcon->setPixmap(QPixmap(":resources/icons/mail-status-read.svg"));
    }
    else
    {
        m_ui->labelReadStatusIcon->setPixmap(QPixmap(":resources/icons/mail-status-unread.svg"));
    }

}

}
}

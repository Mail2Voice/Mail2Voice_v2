#include "emailViewerPanel.hpp"
#include "ui_emailViewerPanel.h"

#include <memory>

#include "core/email.hpp"
#include "core/jobs/testJob.hpp"
#include "core/jobs/jobRunner.hpp"
#include "gui/common/factories/listWidgetFactory.h"
#include "gui/common/guiUtils.hpp"
#include "gui/emails/models/attachmentsListModel.hpp"
#include "gui/emails/widgets/attachmentWidget.hpp"

namespace m2v
{
namespace gui
{

EmailViewerPanel::EmailViewerPanel(QWidget *parent) :
    StylableWidget(utils::css::container2, parent),
    m_ui(std::make_unique<Ui::EmailViewerPanel>())
{
    m_ui->setupUi(this);

    utils::setButtonStyle(m_ui->buttonDeleteEmail, utils::css::warningButton, ":/resources/icons/trash.svg");
    utils::setButtonStyle(m_ui->buttonForwardEmail, utils::css::normalButton, ":/resources/icons/share.svg");
    utils::setButtonStyle(m_ui->buttonReadEmail, utils::css::normalButton, ":/resources/icons/play.svg");
    utils::setButtonStyle(m_ui->buttonReplyAllEmail, utils::css::normalButton, ":/resources/icons/reply-all.svg");
    utils::setButtonStyle(m_ui->buttonReplyEmail, utils::css::normalButton, ":/resources/icons/reply.svg");

    m_ui->widgetAttachments->hide();

    m_ui->widgetAttachments->setOrientation(Qt::Horizontal);
    m_ui->widgetAttachments->layout()->setContentsMargins(0, 0, 0, 0);

    m_attachmentsListModel = std::make_shared<m2v::gui::AttachmentsListModel>();
    m_ui->widgetAttachments->setModel(m_attachmentsListModel);

    std::shared_ptr<m2v::gui::AbstractWidgetFactory> widgetFactory =
            std::make_shared<m2v::gui::ListWidgetFactory<gui::AttachmentWidget, core::Attachment>>();
    m_ui->widgetAttachments->setListWidgetFactory(widgetFactory);

    connect(m_ui->buttonReplyEmail, &QPushButton::clicked,
            this, &EmailViewerPanel::onButtonReplyClicked);

    connect(m_ui->buttonReplyAllEmail, &QPushButton::clicked,
            this, &EmailViewerPanel::onButtonReplyAllClicked);

    connect(m_ui->buttonForwardEmail, &QPushButton::clicked,
            this, &EmailViewerPanel::onButtonForwardEmailClicked);

    connect(m_ui->buttonDeleteEmail, &QPushButton::clicked,
            this, &EmailViewerPanel::onButtonDeleteEmailClicked);

    connect(m_ui->buttonReadEmail, &QPushButton::clicked,
            this, &EmailViewerPanel::onButtonReadEmailClicked);

    m_ui->containerEmailDetails->hide();
}

EmailViewerPanel::~EmailViewerPanel()
{
}

void EmailViewerPanel::setEmail(std::shared_ptr<const m2v::core::Email> email)
{
    clear();

    m_ui->lineEditEmailSubject->setText(email->subject());
    m_ui->labelEmailSenderName->setText(email->senders().join(", "));
    m_ui->labelEmailDate->setText(email->date().date().toString(Qt::DateFormat::DefaultLocaleLongDate));
    m_ui->labelEmailTime->setText(email->date().time().toString(Qt::DateFormat::DefaultLocaleShortDate));
    m_ui->labelEmailRecipients->setText(email->recipients().join(", "));
    m_ui->textEditEmailContent->setHtml(email->content());

    if(!email->attachments().isEmpty())
    {
        m_ui->widgetAttachments->show();
        m_attachmentsListModel->loadAttachments(email->attachments());
    }
    else
    {
        m_ui->widgetAttachments->hide();
    }

    m_ui->containerEmailDetails->show();
}

void EmailViewerPanel::setContent(const QString &content)
{
    m_ui->textEditEmailContent->setHtml(content);
}

void EmailViewerPanel::clear()
{
    m_ui->lineEditEmailSubject->setText("");
    m_ui->labelEmailSenderName->setText("");
    m_ui->labelEmailDate->setText("");
    m_ui->labelEmailTime->setText("");
    m_ui->labelEmailRecipients->setText("");
    m_ui->textEditEmailContent->setHtml("");

    m_attachmentsListModel->clear();
}

void EmailViewerPanel::onButtonReplyClicked()
{

}

void EmailViewerPanel::onButtonReplyAllClicked()
{

}

void EmailViewerPanel::onButtonForwardEmailClicked()
{

}

void EmailViewerPanel::onButtonReadEmailClicked()
{
    // TMP
    std::unique_ptr<m2v::core::TestJob> testJob1 = std::make_unique<m2v::core::TestJob>("yo");
    std::unique_ptr<m2v::core::TestJob> testJob2 = std::make_unique<m2v::core::TestJob>("YOLO");

    core::JobRunner *jobRunner = core::JobRunner::getInstance();

    connect(testJob1.get(), &m2v::core::TestJob::finished, this, []()
    {
        qDebug("test1 finished");
    });

    connect(testJob2.get(), &m2v::core::TestJob::finished, this, []()
    {
        qDebug("test2 finished");
    });

    jobRunner->addJob(std::move(testJob1));
    jobRunner->addJob(std::move(testJob2));
}

void EmailViewerPanel::onButtonDeleteEmailClicked()
{

}

}
}

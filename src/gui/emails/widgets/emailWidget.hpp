#pragma once

#include "gui/common/widgets/selectableWidget.hpp"
#include "core/email.hpp"

#include <memory>

namespace Ui
{
    class EmailWidget;
}

namespace m2v
{
namespace gui
{

/**
 * @brief The EmailWidget class represents an email object in a list.
 */
class EmailWidget : public SelectableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param email Holds email data.
     * @param parent The parent widget.
     */
    explicit EmailWidget(const std::shared_ptr<const core::Email> &email, QWidget *parent = nullptr);

    /**
     * @brief Destructor
     */
    virtual ~EmailWidget();

public slots:

    /**
     * @brief Slot called when the email status has changed .
     * @param read If the message is now read or not.
     */
    void onStatusChanged(bool read);

private:
    std::shared_ptr<const core::Email> m_email;
    std::unique_ptr<::Ui::EmailWidget> m_ui;
};
}
}

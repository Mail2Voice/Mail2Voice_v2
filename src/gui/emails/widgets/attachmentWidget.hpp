#pragma once

#include "gui/common/widgets/selectableWidget.hpp"

#include <memory>

namespace Ui
{
    class AttachmentWidget;
}

namespace m2v
{

namespace core
{
    class Attachment;
}

namespace gui
{


class AttachmentWidget : public SelectableWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param attachment The attachment data to display.
     * @param parent Optional parent widget.
     */
    explicit AttachmentWidget(const std::shared_ptr<const core::Attachment> &attachment, QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~AttachmentWidget();

private:
    std::unique_ptr<Ui::AttachmentWidget> m_ui;
    std::shared_ptr<const core::Attachment> m_attachment;
};

}
}

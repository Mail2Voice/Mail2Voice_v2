#pragma once

#include "core/email.hpp"

#include <memory>

namespace m2v
{
namespace gui
{
// Represents an e-mail within the GUI
class GuiEmail
{
public:

    explicit GuiEmail(const std::shared_ptr<core::Email> &email):
        m_email(email)
    {
    }

    std::shared_ptr<const core::Email> getEmail() const { return m_email; }

private:
    std::shared_ptr<core::Email> m_email;
};
}
}

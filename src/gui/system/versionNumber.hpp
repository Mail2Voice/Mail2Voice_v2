#pragma once

#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(5, 6, 0)
#include "gui/3rdparty/qversionnumber_internal.h"

using QVersionNumber = QVersionNumber_Internal;
#else
#include <QVersionNumber>
#endif

#pragma once

#include <QObject>

#include "versionNumber_forward.hpp"

class QNetworkAccessManager;
class QNetworkReply;

namespace m2v
{
namespace gui
{

/**
 * @brief The UpdateChecker class checks for a new version of the application.
 */
class UpdateChecker final : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent object.
     */
    explicit UpdateChecker(QObject *parent = nullptr);

    UpdateChecker(const UpdateChecker &) = delete;
    UpdateChecker &operator=(const UpdateChecker &) = delete;

    /**
     * @brief Emits a request on remote server to ask for updates
     */
    void check();

    /**
     * @brief Checks whether or not the update checking is finished or not.
     */
    bool isFinished() const { return !m_networkReply; }

signals:
    /**
     * @brief Signal emitted when the update checking is finished.
     * @param version The latest version number.
     */
    void checkingFinished(const QVersionNumber &version);

private slots:
    /**
     * @brief Slot called when the update checking request received an answer.
     */
    void finished();

private:
    QNetworkAccessManager *m_networkAccessManager{nullptr};
    QNetworkReply *m_networkReply{nullptr};
};
}
}

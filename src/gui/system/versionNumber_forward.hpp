#pragma once

#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(5, 6, 0)
class QVersionNumber_Internal;

using QVersionNumber = QVersionNumber_Internal;
#else
class QVersionNumber;
#endif

#include "updateChecker.hpp"
#include "versionNumber.hpp"

#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QtDebug>

namespace m2v
{
namespace gui
{
UpdateChecker::UpdateChecker(QObject *parent):
    QObject(parent),
    m_networkAccessManager(new QNetworkAccessManager(this))
{
    connect(m_networkAccessManager, &QNetworkAccessManager::finished, this, &UpdateChecker::finished);
}

void UpdateChecker::check()
{
    // Ensure that there is no request being processed
    Q_ASSERT(isFinished());

    m_networkReply = m_networkAccessManager->get(QNetworkRequest{QUrl{"http://updates.mail2voice.org"}});
}

void UpdateChecker::finished()
{
    // We make a copy here so that we can set the member variable to nullptr and still continue to use the reply
    // Doing this allows us to return from this function at any time without having to remember to set the variable to nullptr before returning
    QNetworkReply *reply = m_networkReply;
    m_networkReply = nullptr;

    // Never delete the object that triggered the signal, always use deleteLater()
    reply->deleteLater();

    if(reply->error() != QNetworkReply::NoError)
    {
        //TODO: error management here

        qWarning() << "Network error:" << reply->errorString();

        return;
    }

    QJsonParseError jsonParseError;
    QJsonDocument versionDocument = QJsonDocument::fromJson(reply->readAll(), &jsonParseError);

    if(jsonParseError.error != QJsonParseError::NoError)
    {
        //TODO: error management here

        return;
    }

    auto versionObject = versionDocument.object().value("version").toObject();

    if(!versionDocument.isObject() || versionObject.isEmpty())
    {
        //TODO: error management here

        qWarning() << "JSon parsing error: invalid version";

        return;
    }

    emit checkingFinished({versionObject.value("major").toInt(),
                          versionObject.value("minor").toInt(),
                          versionObject.value("patch").toInt()});
}
}
}

#include "accountSettingsPageWidget.hpp"
#include "ui_accountSettingsPageWidget.h"

#include "core/accounts/accounts.hpp"

namespace m2v
{
namespace gui
{
namespace pages
{
AccountSettingsPageWidget::AccountSettingsPageWidget(QWidget *parent):
    PageWidget(parent),
    m_ui(std::make_unique<Ui::AccountSettingsPageWidget>())
{
    m_ui->setupUi(this);
}

AccountSettingsPageWidget::~AccountSettingsPageWidget()
{
}

void AccountSettingsPageWidget::onPageEntered()
{
    fillForm();
}

void AccountSettingsPageWidget::onPageExited()
{
    core::Account account;

    account.setDisplayName(m_ui->lineEditUserDisplayName->text());
    account.setUsername(m_ui->lineEditUserLogin->text());
    account.setEmail(m_ui->lineEditUserEmailAddress->text());
    account.setPassword(m_ui->lineEditUserPassword->text());

    {
        core::ServerSettings storeSettings;
        storeSettings.setType(core::ServerType::Imap);
        storeSettings.setAddress(m_ui->lineEditIncomingServerAddress->text());
        storeSettings.setPort(m_ui->spinBoxIncomingServerPort->value());
        storeSettings.setSsl(m_ui->checkBoxIncomingServerSslTls->isChecked());
        account.setStoreSettings(storeSettings);
    }
    {
        core::ServerSettings transportSettings;
        transportSettings.setType(core::ServerType::Smtp);
        transportSettings.setAddress(m_ui->lineEditOutgoingServerAddress->text());
        transportSettings.setPort(m_ui->spinBoxOutgoingServerPort->value());
        transportSettings.setSsl(m_ui->checkBoxOutgoingServerSslTls->isChecked());
        account.setTransportSettings(transportSettings);
    }

    if (account != core::Accounts::currentAccount()) {
        if(core::Accounts::saveCurrentAccount(account))
        {
            emit accountChanged();
        }
        else
        {
            fillForm();
        }
    }
}

void AccountSettingsPageWidget::fillForm()
{
    const core::Account& currentAccount = core::Accounts::currentAccount();

    if (currentAccount.id() != -1)
    {
        m_ui->lineEditUserDisplayName->setText(currentAccount.displayName());
        m_ui->lineEditUserLogin->setText(currentAccount.username());
        m_ui->lineEditUserEmailAddress->setText(currentAccount.email());
        m_ui->lineEditUserPassword->setText(currentAccount.password());
        m_ui->lineEditIncomingServerAddress->setText(currentAccount.storeSettings().address());
        m_ui->spinBoxIncomingServerPort->setValue(currentAccount.storeSettings().port() != 0 ?
                                                  currentAccount.storeSettings().port() :
                                                  core::ServerSettings::defaultImapPort);
        m_ui->checkBoxIncomingServerSslTls->setChecked(currentAccount.storeSettings().ssl());
        m_ui->lineEditOutgoingServerAddress->setText(currentAccount.transportSettings().address());
        m_ui->spinBoxOutgoingServerPort->setValue(currentAccount.transportSettings().port() != 0 ?
                                                  currentAccount.transportSettings().port() :
                                                  core::ServerSettings::defaultSmtpPort);
        m_ui->checkBoxOutgoingServerSslTls->setChecked(currentAccount.transportSettings().ssl());
    }
}
}
}
}

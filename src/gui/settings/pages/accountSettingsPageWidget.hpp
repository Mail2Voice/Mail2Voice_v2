#pragma once

#include "gui/common/widgets/pageWidget.hpp"

#include <memory>

namespace Ui
{
class AccountSettingsPageWidget;
}

namespace m2v
{
namespace gui
{
namespace pages
{

/**
 * @brief Page to manage settings.
 */
class AccountSettingsPageWidget : public PageWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor.
     * @param parent Optional parent widget.
     */
    explicit AccountSettingsPageWidget(QWidget *parent = nullptr);

    /**
     * @brief Destructor.
     */
    ~AccountSettingsPageWidget();

signals:
    void accountChanged();

protected:
    /** @copydoc PageWidget::onPageEntered */
    void onPageEntered() override;

    /** @copydoc PageWidget::onPageExited */
    void onPageExited() override;

private:
    void fillForm();

private:
    std::unique_ptr<Ui::AccountSettingsPageWidget> m_ui;
};
}
}
}

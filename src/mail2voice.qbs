import qbs
import "gui/functions.js" as QbsFunctions

Project
{
    property string version: "1.0.0"

    CppApplication
    {
        name: "mail2voice"
        files:
        [
            "gui/common/containers/baseContainer.cpp",
            "gui/common/containers/baseContainer.hpp",
            "gui/common/containers/pixmapContainer.cpp",
            "gui/common/containers/pixmapContainer.hpp",
            "gui/common/containers/resourceContainer.hpp",
            "gui/common/factories/abstractWidgetFactory.h",
            "gui/common/factories/listWidgetFactory.h",
            "gui/common/guiUtils.cpp",
            "gui/common/guiUtils.hpp",
            "gui/common/layouts/flowLayout.cpp",
            "gui/common/layouts/flowLayout.hpp",
            "gui/common/widgets/listWidget.cpp",
            "gui/common/widgets/listWidget.hpp",
            "gui/common/widgets/menuWidget.cpp",
            "gui/common/widgets/menuWidget.hpp",
            "gui/common/widgets/menuWidget.ui",
            "gui/common/widgets/pageWidget.hpp",
            "gui/common/widgets/selectableWidget.cpp",
            "gui/common/widgets/selectableWidget.hpp",
            "gui/common/widgets/stylableWidget.cpp",
            "gui/common/widgets/stylableWidget.hpp",
            "gui/contacts/data/avatarGenerator.cpp",
            "gui/contacts/data/avatarGenerator.hpp",
            "gui/contacts/data/avatarGenerator_private.hpp",
            "gui/contacts/data/guiContact.hpp",
            "gui/contacts/models/contactsListModel.cpp",
            "gui/contacts/models/contactsListModel.hpp",
            "gui/contacts/pages/contactsPageWidget.cpp",
            "gui/contacts/pages/contactsPageWidget.hpp",
            "gui/contacts/pages/contactsPageWidget.ui",
            "gui/contacts/widgets/contactViewerPanel.cpp",
            "gui/contacts/widgets/contactViewerPanel.hpp",
            "gui/contacts/widgets/contactViewerPanel.ui",
            "gui/contacts/widgets/contactWidget.cpp",
            "gui/contacts/widgets/contactWidget.hpp",
            "gui/contacts/widgets/contactWidget.ui",
            "gui/emails/data/guiEmail.hpp",
            "gui/emails/models/attachmentsListModel.cpp",
            "gui/emails/models/attachmentsListModel.hpp",
            "gui/emails/models/emailsListModel.cpp",
            "gui/emails/models/emailsListModel.hpp",
            "gui/emails/pages/emailsPageWidget.cpp",
            "gui/emails/pages/emailsPageWidget.hpp",
            "gui/emails/pages/emailsPageWidget.ui",
            "gui/emails/widgets/attachmentWidget.cpp",
            "gui/emails/widgets/attachmentWidget.hpp",
            "gui/emails/widgets/attachmentWidget.ui",
            "gui/emails/widgets/emailViewerPanel.cpp",
            "gui/emails/widgets/emailViewerPanel.hpp",
            "gui/emails/widgets/emailViewerPanel.ui",
            "gui/emails/widgets/emailWidget.cpp",
            "gui/emails/widgets/emailWidget.hpp",
            "gui/emails/widgets/emailWidget.ui",
            "gui/main.cpp",
            "gui/mainWindow.cpp",
            "gui/mainWindow.hpp",
            "gui/mainWindow.qrc",
            "gui/mainWindow.ui",
            "gui/settings/pages/accountSettingsPageWidget.cpp",
            "gui/settings/pages/accountSettingsPageWidget.hpp",
            "gui/settings/pages/accountSettingsPageWidget.ui",
            "gui/system/updateChecker.cpp",
            "gui/system/updateChecker.hpp",
            "gui/system/versionNumber_forward.hpp",
            "gui/system/versionNumber.hpp",
        ]

        cpp.defines: ["PROJECT_VERSION=\"" + project.version + "\""]
        cpp.cxxLanguageVersion: "c++14"
        cpp.includePaths: "."

        Group
        {
            // QVersionNumber was introduced in Qt 5.6.0, so we provide an embedded copy that can be used with older versions
            condition: QbsFunctions.isVersionLowerThan(Qt.core.version, "5.6.0")
            files:
            [
                "gui/3rdparty/qversionnumber_internal.cpp",
                "gui/3rdparty/qversionnumber_internal.h",
                "gui/3rdparty/strtoull.cpp",
            ]
         }

        Depends
        {
            name: "Qt";
            submodules:
            [
                "core",
                "gui",
                "widgets",
                "network",
                "sql",
                "svg",
                "xml"
            ]
        }

        Depends
        {
            name: "mail2voice-core"
        }

        Group
        {
            fileTagsFilter: product.type
            qbs.install: true
        }
    }

    references: ["core/mail2voice-core.qbs", "unittests/unittests.qbs", "unittests-gui/unittests-gui.qbs"]

}

Mail2Voice_v2
=============

E-Mail client software dedicated to illiterate people or with cognitive or motor impairment,
or for the elderly persons and young children.



Accessibility Concepts
======================

  * Mail2Voice proposes a simplified graphical interface usable with a touch screen.
  * Voice recording is used to send messages (audio file attached).
  * Received messages can be read with speech synthesizer.
  * Addressbook displays portraits to make contacts selection easier.
  

Why V2?
=================

This project is a complete rewriting of the original project by the same team. There are multiple objectives:
  * make a reliable email client based on VMime,
  * improve user interface and accessibility support,
  * write clean and maintenable code.

  
More infos
==========

Users can visit the website : http://www.mail2voice.org
Developpers can visit the wiki : http://wiki.mail2voice.org